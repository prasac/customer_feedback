<?php
include('admin/session.php');
if(isset($_SESSION['login_user'])){
  if(isset($_SESSION['login_id'])==1):
    header("location:http://feedback.local/auth/index.php");
  else:
    header("location:http://feedback.local/auth/index.php");
  endif;
}else{
  include('admin/login.php'); // Includes Login Script
  include('header-login.php');
?>
<style>
body {
    background: -webkit-linear-gradient(rgba(243, 242, 242, 0.7), rgba(241, 240, 240, 0.7)), url('http://feedback.local/images/2ndbackground.jpg') no-repeat center;
    background-size: cover;
    width: 100%;
    height: 100vh;
    background-attachment: fixed;
}

/* ANIMATIONS */

/* Simple CSS3 Fade-in-down Animation */
.fadeInDown {
  -webkit-animation-name: fadeInDown;
  animation-name: fadeInDown;
  -webkit-animation-duration: 2s;
  animation-duration: 2s;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
}

@-webkit-keyframes fadeInDown {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
  100% {
    opacity: 1;
    -webkit-transform: none;
    transform: none;
  }
}

@keyframes fadeInDown {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
  100% {
    opacity: 1;
    -webkit-transform: none;
    transform: none;
  }
}

/* Simple CSS3 Fade-in Animation */
@-webkit-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@-moz-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@keyframes fadeIn { from { opacity:0; } to { opacity:1; } }

.fadeIn {
  opacity:0;
  -webkit-animation:fadeIn ease-in 2;
  -moz-animation:fadeIn ease-in 2;
  animation:fadeIn ease-in 2;

  -webkit-animation-fill-mode:forwards;
  -moz-animation-fill-mode:forwards;
  animation-fill-mode:forwards;

  -webkit-animation-duration:2s;
  -moz-animation-duration:2s;
  animation-duration:2s;
}
</style>
  <main>
  <center>
    <div class="container-fluid row">
      <div class="section"></div>
      <div class="z-depth-1 lighten-4 row form-wrapper" style="display: inline-block; padding:2em;border: 1px solid #4db848;margin-top:10%;">
        <h1 class="indigo-text">ប្រព័ន្ធគ្រប់គ្រងព័ត៌មានត្រលប់ពីអតិថិជន</h1>
        <hr>
        <div class="col s4">
          <img class="responsive-img main-logo fadeInDown" src="http://feedback.local/images/Animation-Logo-Prasac.gif"/>
        </div>
        <div class="col s8">
          <form action="" method="POST" class="login-form">
            <div class='row'>
              <div class='input-field col s12'>
                <input type='text' name='username' id='username' class='validate'  data-role="materialtags"/>
                <label for='username'>ឈ្មោះ​អ្នកប្រើប្រាស់</label>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='password' name='password' id='password'/>
                <label for='password'>លេខ​កូដសម្ងាត់​</label>
              </div>
            </div>
            <span class='pink-text'>
              <?php echo  $error;?>
            </span>
            <br />
            <center>
              <div class='row'>
                <button type='submit' name='submit' class='col s12 btn btn-large waves-effect indigo'>ចូល​</button>
              </div>
            </center>
          </form>
          <p style="color:#3f51b5;font-size:12px;"><i class="material-icons">info_outline</i> សូម​ប្រើ​លេខ FC សាខាដើម្បីចូលប្រើប្រាស់ប្រព័ន្ធ</p>
        </div>
      </div>
    </div>
  </center>
  <div class="section"></div>
  <div class="section"></div>
</main>
<?php 
include('footer-login.php');
}
?>
<script>
    $(document).ready(function(){
      var spinner = jQuery('#loader');
      $.validator.setDefaults({
        ignore: []
      });
      $.validator.addMethod("regx", function(value, element, regexpr) {          
          return regexpr.test(value);
      }, "Please enter a valid pasword.");

      $("form.login-form").validate({
        // errorClass: "", //invalid form-error
        errorElement : 'span',       
        errorPlacement: function(error, element) {
            error.appendTo( element.parent() );
        },
        rules: {
          username: {
            required: true,
          },
          password:{
            required: true,
          },
        },
        //For custom messages
        messages: {
          username: {
            required: "សូមវាយបញ្ចូលឈ្មោះអ្នកប្រើប្រាស់!",
          },
          password:{
            required: "សូមវាយបញ្ចូលលេខសម្ងាត់!",
          },
        }
      });
    });
</script>
