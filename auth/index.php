<?php
$date = new DateTime();
$date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
$today_time = $date->format("Y-m-d h:i A");
$current_date_time = $date->format("Y-m-d h:i");
$thismonth  = $date->format("m");
$thisyear   = $date->format("Y");

include('../admin/session.php');
  // Set Language variable
  if(isset($_GET['lang']) && !empty($_GET['lang'])){
    $_SESSION['lang'] = $_GET['lang'];
    if(isset($_SESSION['lang']) && $_SESSION['lang'] != $_GET['lang']){
      echo "<script type='text/javascript'> location.reload(); </script>";
    }
  }

    // Include Language file
  if(isset($_SESSION['lang'])){
    include "../library/lang/lang_".$_SESSION['lang'].".php";
  }else{
    include "../library/lang/lang_kh.php";
  }

if(!isset($_SESSION['login_user_fc'])){
  header("location:http://feedback.local/");
  die();
}else{
  $page_title = isset($_GET['page'])?$_GET['page']:'';
  $title='ប្រព័ន្ធគ្រប់គ្រងព័ត៌មានត្រលប់របស់អតិថិជន';
  switch($page_title){
    case 'list':
      $title='បញ្ជីរបាយការណ៍ព័ត៌មានត្រឡប់អតិថិជន';
      break;
    case 'add':
      $title='បញ្ចូលរបាយការណ៍ថ្មី';
      break;
    case 'view':
      $title='មើលរបាយការណ៍';
      break;
    case 'edit':
      $title='កែប្រែរបាយការណ៍';
      break;
    case 'myaccount':
      $title='គណនីរបស់ខ្ញុំ';
      break;
    case 'changepassword':
      $title='ផ្លាស់ប្ដូរលេខសម្ងាត់';
      break;
  }
  include('../header-login.php');
  $menu = isset($_GET['menu'])?$_GET['menu']:'';

  $display_name = isset($_SESSION['login_user'])?$_SESSION['login_user']:'';
  $user_type  = isset($_SESSION['login_id'])?$_SESSION['login_id']:0;
  $user_id  = isset($_SESSION['user_id'])?$_SESSION['user_id']:0;
  $user_type_title = array(1=>"អ្នកគ្រប់គ្រងប្រព័ន្ធ", 2=>"មន្ត្រីទំនាក់ទំនងអតិថិជន",3=>"បណ្ដាញសង្គម", 4=>"សាខា");

  ?>
  <div class="row main-wrapper">
    <div class="col s2 m2">
        <div class="col s12 m12  sidebar-right">
          <a href="/"><img src="../images/Animation-Logo-Prasac.gif" width="100" style="display:block;margin:0 auto;"/></a>
          <?php echo '<br /><div class="row"><div class="col s12 centered" style="color:#4db848;font-size:1rem;"><i class="fa fa-user-circle-o fa-3x left" aria-hidden="true"></i>
                  <span class="left" style="margin-top:1rem;font-size:1.1rem;">'.$display_name.'</span>
                </div></div>';?>
          <div class="main">
            <ul class="list section table-of-contents" style="margin:0;padding:0;">
              <li class="sidebar-item <?php echo $menu=='myreport'?' active':'';?>" ><a class="" href="?menu=myreport&page=list" data-href="myreport"><?= _FEEDBACKREPORT; ?></a> <a href="?menu=myreport&page=add" class="waves-effect waves-light addnew right tooltipped" data-tooltip="<?= _ADDNEWREPORT; ?>"><i alt="" class="material-icons text-green">add_circle</i></a></li>
              <?php //if($user_type ==1){?>
              <li class="sidebar-item <?php echo $menu=='service_checking'?' active':'';?>" ><a class="" href="?menu=service_checking&page=service_checking-list" data-href="service_checking"><?= _SERVICEQUALITYCHECKING;?></a></li>
              <?php //}?>
              <?php //====Special account number menu=====
              if($user_type ==1){?>
              <li class="sidebar-item <?php echo $menu=='special_account'?' active':'';?>" ><a class="" href="?menu=special_account&page=special_account-list" data-href="service_checking"><?= _SPECIALACCOUNTREPORT;?></a></li>
              <?php }?>
              <li class="sidebar-item <?php echo $menu=='myaccount'?' active':'';?>" ><a class="" href="?menu=myaccount&page=myaccount" data-href="myaccount"><?= _MYACCOUNT; ?></a></li>
              <?php if($user_type !=4){?>
                <li class="sidebar-item <?php echo $menu=='changepassword'?' active':'';?>" ><a class="" href="?menu=changepassword&page=changepassword&user_id=<?php echo $user_id;?>" data-href="changepassword"><?= _CHANGEPASSWORD; ?></a></li>
              <?php }?>
              <?php if($user_type ==1){?>
                <li class="sidebar-item <?php echo $menu=='deleted_report'?' active':'';?>" ><a class="" href="?menu=deleted_report&page=deleted_report"><?= _DELETEREPORT; ?></a></li>
              <?php }?>
              <li class="sidebar-item logout"><a href="http://feedback.local/admin/logout.php"><i alt="" class="material-icons logout-button left" style="color:#ee6e73;">exit_to_app</i> <?= _LOGOUT; ?> </a></li>
            </ul>
          </div>
          <br />
          <div class="col s12 m12 center">
              <a href="?lang=kh"><img src="../images/kh.png" alt="" width="30"/></a>
              <a href="?lang=en"><img src="../images/uk.png" alt="" width="30"/></a>
          </div>
          <div class="col s12 m12 center">
            <small>Version 1.0.0</small>
          </div>
        </div>
    </div>
    <div class="col s10 m10 main-container">
      <div class="col s12 m12 center-container"> 
        <?php
        $page_selected = 'list.php';
        if(isset($_GET['page'])){
          $page_selected = $_GET['page'].'.php';
        }else{
          $page_selected = 'list.php';
        }
        include($page_selected);
      ?>
      </div>
    </div>
  </div>    
  <?php include('../admin/footer.php');?><!--Import jQuery before materialize.js-->
  <script>
    var spinner = jQuery('#loader');
    $(document).ready(function(){
      $("#service_checking_branch").chained("#service_checking_regional");
      if($('#service_checking_branch').length){
          $('select#service_checking_branch').formSelect();
          $("select#account_status").material_select();
          var $selectId = $("#service_checking_branch");
          var $index = $selectId.find(":selected").index();
          var lastPos = 0;
          $selectId.children().each(function () { lastPos++; });
          var currScrollPos = ($index / lastPos) * parseInt($selectId[0].scrollHeight);
          $selectId.scrollTop(currScrollPos);
      }

      $("#delete_report").on('click',function(){
        var $restored_item = $(this).closest('tr');
        var $spinner_class = $(this).find(".fa-refresh");
        if(confirm("តើលោកអ្នកពិតជាចង់លុបរបាយការណ៍នេះមែនទេ?")){
          var $item_id = $(this).data('item');
          spinner.show();
          jQuery.ajax({
              url: "delete.php",
              type: "POST",
              data:{'item':$item_id,'status':'trush_delete'},
              success: function(data) {
                spinner.hide();
                $restored_item.remove();
                $restored_item.remove();
                showToast('<i class="material-icons left" style="color:#4db848;">check_circle</i>&nbsp;<span style="color:#4db848;padding:5px 10px;">របាយការណ៍របស់លោកអ្នកបានលុប</span>',1500);
              },
          });
        }
       window.history.go(-1);
      });

      $(".reload_report").on("click", function(){
        $(this).find(".fa-refresh").addClass(" fa-spin");
        var $restored_item = $(this).closest('tr');
        var $spinner_class = $(this).find(".fa-refresh");
        if(confirm("តើលោកអ្នកពិតជាចង់ទាញយករបាយការណ៍នេះមកវិញមែនទេ?")){
          var $item_id = $(this).data('item');
          spinner.show();
          jQuery.ajax({
              url: "delete.php",
              type: "POST",
              data:{'item':$item_id,'status':'restore'},
              success: function(data) {
                spinner.hide();
                if('Success' == data){
                  $spinner_class.removeClass("fa-spin");
                  $restored_item.remove();
                  showToast('<i class="material-icons left" style="color:#4db848;">check_circle</i>&nbsp;<span style="color:#4db848;padding:5px">របាយការណ៍របស់លោកអ្នកត្រូវបានទាញយកមកវិញដោយជោគជ័យ</span>',1500);
                }
              }
          });
        }
      });
      $(".permamently_delete_report").on("click", function(){
        $(this).find(".fa-refresh").addClass(" fa-spin");
        var $restored_item = $(this).closest('tr');
        var $spinner_class = $(this).find(".fa-refresh");
        if(confirm("តើលោកអ្នកពិតជាចង់លុបរបាយការណ៍នេះចេញពីប្រព័ន្ធមែនទេ?")){
          var $item_id = $(this).data('item');
          spinner.show();
          jQuery.ajax({
              url: "delete.php",
              type: "POST",
              data:{'item':$item_id,'status':'permanent_delete'},
              success: function(data) {
                spinner.hide();
                if('Success' == data){
                  $spinner_class.removeClass("fa-spin");
                  $restored_item.remove();
                  showToast('<i class="material-icons left" style="color:#4db848;">check_circle</i>&nbsp;<span style="color:#4db848;padding:5px 10px;">របាយការណ៍របស់លោកអ្នកត្រូវបានលុបចេញពីប្រព័ន្ធ</span>',1500);
                }
              }
          });
        }
      });
      



      $('input#client_phone').bind('keyup mouseup', function(){
        $(this).val($.trim($(this).val()));
      });
      
      if($( "#fcsite" ).length && $("#fcsite").val()!=''){
        $("#branch_name,#regional_name").parent().find('label').addClass(' active');
        var id_check_length =   $("#fcsite").val().length;
        var fcsite = $("#fcsite").val();
        if(id_check_length==3){
          spinner.show();
          jQuery.ajax({
              url: "filter-regional.php",
              type: "POST",
              data:{'fcsite':fcsite},
              success: function(data) {
                //console.log(data);
                  spinner.hide();
                  var json_data   =   JSON.parse(data);
                  $("#branch_name").val(json_data['branch_name']);
                  $("#regional_name").val(json_data['regional_name']);
              },
              error: function(jqXHR, textStatus, errorThrown) {
                $("#branch_name").val('');
                  $("#regional_name").val('');
                //console.log(errorThrown);
              }
          });
        }
      }

      $("#fcsite").bind("change keyup", function(e) {
        if($(this).attr("readonly") !='readonly'){
          $("#branch_name,#regional_name").parent().find('label').addClass(' active');
          var id_check_length =   $(this).val().length;
          var fcsite = $(this).val();
          if(id_check_length==3){
            spinner.show();
            jQuery.ajax({
                url: "filter-regional.php",
                type: "POST",
                data:{'fcsite':fcsite},
                success: function(data, textStatus, jqXHR) {
                  // console.log(data);
                    spinner.hide();
                    var json_data   =   JSON.parse(data);
                    $("#branch_name").val(json_data['branch_name']);
                    $("#regional_name").val(json_data['regional_name']);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
          }
        }
      });

     


      $.validator.addMethod("greaterThan", 
      function(value, element, params) {

          if (!/Invalid|NaN/.test(new Date(value))) {
              return new Date(value) > new Date($(params).val());
          }

          return isNaN(value) && isNaN($(params).val()) 
              || (Number(value) > Number($(params).val())); 
      },'Must be greater than {0}.');


      $.validator.setDefaults({
        ignore: []
      });

      $.validator.addMethod("regx", function(value, element, regexpr) {          
          return regexpr.test(value);
      }, "Please enter a valid pasword.");

      $("form.newreport_branch,form.editreport_branch").validate({
        // errorClass: "", //invalid form-error
        errorElement : 'span',       
        errorPlacement: function(error, element) {
            error.appendTo( element.parent() );
        },
        rules: {
          client_name: {
            required: true,
          },
          client_gender:{
            required: true,
          },
          issue_category:{
            required:true,
          },
          client_financial_service:{
            required:true,
          },
          feedback_category:{
            required:true,
          },
          issue_detail:{
            required:true,
          },
          solution:{
            required:true,
          },
          result_status:{
            required:true,
          },
          response_channel:{
            required:true,
          },
          staff_id:{
            required:true,
            minlength:5,
            maxlength:5,
          },
          fcsite:{
            required:true,
            minlength:3,
            maxlength:3,
          },
          branch_name:{
            required:true
          },
          regional_name:{
            required:true
          },
          response_position:{
            required:true
          },
        },
        //For custom messages
        messages: {
          client_name: {
            required: "Please enter client name!",
          },
          client_gender:{
            required: "Please select client gender!",
          },
          issue_category:{
            required:"Please select client category!",
          },
          client_financial_service:{
            required:"Please select client type!",
          },
          feedback_category:{
            required:"Please select client area!",
          },
          issue_detail:{
            required:"Please enter issue detail information!",
          },
          solution:{
            required:"Please enter solution description!",
          },
          result_status:{
            required: "Please select result!",
          },
          response_channel:{
            required:"Please enter channel!",
          },
          staff_id:{
            required:"Staff ID!",
            nlength:"Min 5 digits",
            maxlength:"Min 5 digits"
          },
          fcsite:{
            required:"FC!",
            minlength:"Min 3 digits",
            maxlength:"Max 3 digits",
          },
          branch_name:{
            required:"Required!"
          },
          regional_name:{
            required:"Required!"
          },
          staff_position:{
            required:"Please select your position!"
          },
          result_status:{
            required:"Please choose issue status!",
          },
        }
      });



      //Form add report social
      $("form.newreport_social").validate({
        // errorClass: "", //invalid form-error
        errorElement : 'span',       
        errorPlacement: function(error, element) {
            error.appendTo( element.parent() );
        },
        rules: {
          client_name: {
            required: true,
          },
          client_gender:{
            required: true,
          },
          client_phone: {
            required:true,
            minlength: 9,
            maxlength:10,
          },
          issue_category:{
            required:true,
          },
          client_financial_service:{
            required:true,
          },
          feedback_category:{
            required:true,
          },
          issue_detail:{
            required:true,
          },
          solution:{
            required:true,
          },
          result_status:{
            required:true,
          },
          issue_date:{
            required:true,
          },
          issue_time:{
            required:true,
          },
          response_channel:{
            required:true,
          },
          fcsite:{
            required:true,
            minlength:3,
            maxlength:3,
          },
          branch_name:{
            required:true
          },
          regional_name:{
            required:true
          },
          staff_id:{
            required:true,
            minlength:5,
            maxlength:5,
          },
        },
        //For custom messages
        messages: {
          client_name: {
            required:"Required!",
          },
          client_gender:{
            required: "Required!",
          },
          client_phone: {
            required:"Required!",
            minlength:"Min 9 digits !",
            maxlength:"Min 10 digits !",
          },
          issue_category:{
            required:"Required!",
          },
          client_financial_service:{
            required:"Required!",
          },
          feedback_category:{
            required:"Required!",
          },
          issue_detail:{
            required:"Required!",
          },
          solution:{
            required:"Required!",
          },
          result_status:{
            required:"Required!",
          },
          issue_date:{
            required:"Required!",
          },
          issue_time:{
            required:"Required!",
          },
          response_channel:{
            required:"Required!",
          },
          fcsite:{
            required:"Required!",
            minlength:"Min 3 digits !",
            maxlength:"Max 3 digits !",
          },
          branch_name:{
            required:"Required!"
          },
          regional_name:{
            required:"Required!"
          },
          staff_id:{
            required:"Required!",
            minlength:"Min 5 digits !",
            maxlength:"Max 5 digits !",
          },
        }
      });


      //Form add report Call Center
      $("form.newreport_callcenter,form.editreport_callcenter").validate({
        // errorClass: "", //invalid form-error
        errorElement : 'span',       
        errorPlacement: function(error, element) {
            error.appendTo( element.parent() );
        },
        rules: {
          client_name: {
            required: true,
          },
          client_gender:{
            required: true,
          },
          client_phone: {
            required:true,
            minlength: 9,
            maxlength:10,
            regx: /^([0-9]{9,10})$/,
          },
          issue_category:{
            required:true,
          },
          client_financial_service:{
            required:true,
          },
          feedback_category:{
            required:true,
          },
          issue_detail:{
            required:true,
          },
          solution:{
            required:true,
          },
          result_status:{
            required:true,
          },
          issue_date:{
            required:true,
          },
          issue_time:{
            required:true,
          },
          response_channel:{
            required:true,
          },
          fcsite:{
            required:true,
            minlength:3,
            maxlength:3,
          },
          branch_name:{
            required:true
          },
          regional_name:{
            required:true
          },
          staff_id:{
            required:true,
            minlength:5,
            maxlength:5,
          },
        },
        //For custom messages
        messages: {
          client_name: {
            required:"Required!",
          },
          client_gender:{
            required: "Required!",
          },
          client_phone: {
            required:"Required!",
            minlength:"Min 9 digits !",
            maxlength:"Min 10 digits !",
            regx:"Latin number will be accepted",
          },
          issue_category:{
            required:"Required!",
          },
          client_financial_service:{
            required:"Required!",
          },
          feedback_category:{
            required:"Required!",
          },
          issue_detail:{
            required:"Required!",
          },
          solution:{
            required:"Required!",
          },
          result_status:{
            required:"Required!",
          },
          issue_date:{
            required:"Required!",
          },
          issue_time:{
            required:"Required!",
          },
          response_channel:{
            required:"Required!",
          },
          fcsite:{
            required:"Required!",
            minlength:"Min 3 digits !",
            maxlength:"Max 3 digits !",
          },
          branch_name:{
            required:"Required!"
          },
          regional_name:{
            required:"Required!"
          },
          staff_id:{
            required:"Required!",
            minlength:"Min 5 digits !",
            maxlength:"Max 5 digits !",
          },
        }
      });
   
      //Form change password
      $.validator.addMethod("pwcheck", function(value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
            && /[a-z]/.test(value) // has a lowercase letter
            && /\d/.test(value) // has a digit
      });

      $.validator.addMethod("greaterThan",function(value, element, params) {
          if (!/Invalid|NaN/.test(new Date(value))) {
              return new Date(value) > new Date($(params).val());
          }

          return isNaN(value) && isNaN($(params).val()) 
              || (Number(value) > Number($(params).val())); 
      },'Must be greater than {0}.');

      $.validator.addMethod("NotEqualTo", function(value, element, param) {
        var str1 = value  || '';
        var str2 = $(param).val() || '';
        return $.trim(str1) != $.trim(str2);
      },'Invalid: Two values are matching');

      // validate signup form on keyup and submit
      $("form#change_password").validate({
        errorElement : 'span',       
        errorPlacement: function(error, element) {
            error.appendTo( element.parent() );
        },
        rules: {
          old_password: {
            required: true,
            minlength:5,
            maxlength:10,
            NotEqualTo:"#new_password",
          },
          new_password: {
            required: true,
            minlength:5,
            maxlength:10,
            NotEqualTo: "#old_password",
            pwcheck: true,
          },
          password_confirm: {
            required: true,
            equalTo: "#new_password",
            minlength:5,
            maxlength:10,
            pwcheck: true,
          }
        },
        messages: {
          old_password: {
            required: "សូមបំពេញលេខសម្ងាត់បច្ចុប្បន្ន!",
            minlength: jQuery.validator.format("លេខ​សម្ងាត់​យ៉ាងហោចណាស់ {0} ខ្ទង់"),
            maxlength: jQuery.validator.format("លេខ​សម្ងាត់​យ៉ាងវែងបំផុត {0} ខ្ទង់"),
            NotEqualTo:"លេខសម្ងាត់ចាស់ និងលេខសម្ងាត់ថ្មីមិនអាចដូចគ្នាបានទេ"
          },
          new_password: {
            required: "សូមបំពេញលេខសម្ងាត់ថ្មី!",
            minlength: jQuery.validator.format("លេខ​សម្ងាត់​ថ្មីយ៉ាងហោចណាស់ {0} ខ្ទង់"),
            maxlength:jQuery.validator.format("លេខ​សម្ងាត់​យ៉ាងវែងបំផុត {0} ខ្ទង់"),
            NotEqualTo: "លេខសម្ងាត់ចាស់ និងលេខសម្ងាត់ថ្មីមិនអាចដូចគ្នាបានទេ",
            pwcheck:"លេខសម្ងាត់ត្រូវមានយ៉ាងតិច អក្សរធំមួយ អក្សរតូចមួយ លេខមួយ ដែលមានពី 5-10 ខ្ទង់",
          },
          password_confirm: {
            required: "សូមបំពេញបញ្ជាក់លេខសម្ងាត់ថ្មី!",
            minlength: jQuery.validator.format("លេខ​សម្ងាត់​ថ្មីយ៉ាងហោចណាស់ {0} ខ្ទង់"),
            maxlength:jQuery.validator.format("លេខ​សម្ងាត់​យ៉ាងវែងបំផុត {0} ខ្ទង់"),
            equalTo: "សូមផ្ទៀងផ្ទាត់ជាមួយ​លេខ​សម្ងាត់​ថ្មីខាងលើ!",
            pwcheck:"លេខសម្ងាត់ត្រូវមានយ៉ាងតិច អក្សរធំមួយ អក្សរតូចមួយ លេខមួយ ដែលមានពី 5-10 ខ្ទង់",
          }
        },
        // specifying a submitHandler prevents the default submit, good for the demo
        submitHandler: function() {
          spinner.show();
          var data = $("form#change_password").serialize();
          $.ajax({
            type:"POST",
            url:"changepassword.php",
            cache:false,
            data:data,
            success: function(data)
            {
              spinner.hide();
              showToast('<i class="material-icons left">check_circle</i>  លេខសម្ងាត់លោកអ្នក​ត្រូវបានផ្លាស់ប្ដូរ​យ៉ាងជោគជ័យ',3000);
              console.log(data);
            }
          });
        },
        // set this class to error-labels to indicate valid fields
        success: function(label) {
          console.log(label);
        },
      });
      

      $("input.service_checking_reported_staff_id").on('keyup',function(){
        var id_check_length =   $(this).val().length;
        var staff_id_number = $(this).val();
        if(id_check_length==5){
          spinner.show();
          $.ajax({
              url: "filter-staff.php",
              type: "POST",
              data:{'staff_id':staff_id_number},
              success: function(data) {
                spinner.hide();
                if(data==0){
                  $(".modal-service-checking #staff_checking_status").html('<div class="fail"><span>រកមិនឃើញបុគ្គលិកដែលមានអត្តលេខ '+staff_id_number+' នោះទេ</span></div>');
                  $("input.service_checking_reported_staff_id").val("");
                }else{
                  $(".modal-service-checking #staff_checking_status").html('<div class="success"><i class="fa fa-check-circle fa-2x done left"></i><span>'+data+'</span></div>');
                }
              },
              error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
              }
          });
        }
      });


      //Search Branch info by entered branch fc
      $("input#new_staff_branch_fc").on('keyup',function(e){
        var id_check_length =   $(this).val().length;
        var fcsite_number = $(this).val();
        if(id_check_length == 3){
          spinner.show();
          $.ajax({
              url: "filter-branch.php",
              type: "POST",
              data:{'fcsite':fcsite_number,'filter_type':'Single'},
              success: function(response) {
                spinner.hide();
                if(response==0){
                  $(".modal-add-new-staff #staff_checking_status").html('<div class="fail"><span>រកមិនឃើញសាខាដែលមានលេខកូដ '+fcsite_number+' នោះទេ</span></div>');
                  $(".modal-add-new-staff input#new_staff_branch_fc").val("");
                }else{
                  var datasource = JSON.parse(response);
                  $(".modal-add-new-staff #branch_id").val(datasource.id);
                  $(".modal-add-new-staff #regional_id").val(datasource.regional_id);
                  $(".modal-add-new-staff #staff_checking_status").html('<div class="success"><i class="fa fa-check-circle fa-2x done left"></i><span>'+datasource.branch_name+'</span></div>');
                }
              },
              error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
              }
          });
        }
      });

      // validate add new staff
      $("form#service_checking_new_staff").validate({
        errorElement : 'span',       
        errorPlacement: function(error, element) {
            error.appendTo( element.parent() );
        },
        rules: {
          new_staff_id: {
            required: true,
            minlength:5,
            maxlength:5,
            regx: /^([0-9]{5,5})$/,
          },
          new_staff_name_kh: {
            required: true,
          },
          new_staff_name_en: {
            required: true,
          },
          new_staff_position:{
            required: true,
          },
          new_staff_branch_fc:{
            required: true,
            minlength:3,
            maxlength:3,
            regx: /^([0-9]{3,3})$/,
          }
        },
        messages: {
          new_staff_id: {
            required: "សូមបំពេញអត្តលេខរបស់បុគ្គលិក!",
            minlength: $.validator.format("អត្តលេខរបស់បុគ្គលិកយ៉ាងហោចណាស់ {0} ខ្ទង់"),
            maxlength: $.validator.format("អត្តលេខរបស់បុគ្គលិកយ៉ាងវែងបំផុត {0} ខ្ទង់"),
            regx:"សូមវាយបញ្ចូលលេខឡាតាំង (ឧ. 09072)",
          },
          new_staff_name_kh: {
            required: "សូមវាយបញ្ចូលឈ្មោះបុគ្គលិកជាខ្មែរ!",
          },
          new_staff_name_en: {
            required: "សូមវាយបញ្ចូលឈ្មោះបុគ្គលិកជាឡាតាំង!",
          },
          new_staff_position:{
            required:"សូមវាយបញ្ចូលតួនាទីបុគ្គលិក!",
          },
          new_staff_branch_fc:{
            required:"សូមវាយបញ្ចូលលេខកូដសាខា!",
            minlength:$.validator.format("លេខកូដសាខាយ៉ាងហោចណាស់ {0} ខ្ទង់"),
            maxlength:$.validator.format("លេខកូដសាខាយ៉ាងវែងបំផុត {0} ខ្ទង់"),
            regx:"សូមវាយបញ្ចូលលេខកូដសាខា (ឧ. 001)",
          }
        },
         // specifying a submitHandler prevents the default submit, good for the demo
         submitHandler: function() {
          spinner.show();
          var data = $("form#service_checking_new_staff").serialize();
          $.ajax({
              url: "new-staff.php",
              type: "POST",
              data:data,
              success: function(data){
                  spinner.hide();
                  console.log(data);
                  if(data){
                    showToast('<span style="color:#4db848;"><i class="material-icons left done">check_circle</i> បុគ្គលិកម្នាក់ត្រូវបានរក្សាទុកដោយជោគជ័យ!</span>',1500);
                    $('.modal.open').modal('close');
                    setTimeout(function () {
                        location.reload();
                    },2000);
                  }else{
                    showToast('<span style="color:#4db848;"><i class="material-icons left fail">times</i> មិនជោគជ័យទេ!</span>',1500);
                  }
              },
              error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
              }
          });
        },
        // set this class to error-labels to indicate valid fields
        success: function(label) {
          console.log(label);
        },
      });



      $(".modal-footer .modal-action, .modal-fotter button").click(function(e){
        $(".modal-service-checking #staff_checking_status").text("");
      });

      $("button.btn_submit_update,.btn_delete_staff").click(function(e){
        e.preventDefault();
        var service_checking_note = $(this).parent().parent().find(".service_checking_note").val();
        var service_checking_reported_staff_id = $(this).parent().parent().find(".service_checking_reported_staff_id").val();
        var data_action = $(this).data('action');
        if(service_checking_note =='' && service_checking_reported_staff_id ==''){
         
          $(this).parent().parent().find("#service_checking_note").parent().find("span.error").remove();
          $(this).parent().parent().find("#service_checking_note").after('<span class="error">សូមវាយបញ្ចូលកំណត់សម្គាល់របស់លោកអ្នកចំពោះបុគ្គលិក!</span>');

          $(this).parent().parent().find(".service_checking_reported_staff_id").parent().find("span.error").remove();
          $(this).parent().parent().find(".service_checking_reported_staff_id").after('<span class="error">សូមវាយបញ្ចូលអត្តលេខរបស់លោកអ្នក!</span>');
        }

        if(service_checking_note !='' && service_checking_reported_staff_id ==''){
         
          $(this).parent().parent().find(".service_checking_note").parent().find("span.error").remove();
          $(this).parent().parent().find(".service_checking_reported_staff_id").after('<span class="error">សូមវាយបញ្ចូលអត្តលេខរបស់លោកអ្នក!</span>');
        }

        if(service_checking_note =='' && service_checking_reported_staff_id !=''){
          $(this).parent().parent().find(".service_checking_reported_staff_id").parent().find("span.error").remove();
          $(this).parent().parent().find(".service_checking_note").after('<span class="error">សូមវាយបញ្ចូលកំណត់សម្គាល់របស់លោកអ្នកចំពោះបុគ្គលិក!</span>');
       }

       if(service_checking_note !='' && service_checking_reported_staff_id !=''){
            spinner.show();
            var data = $(this).parent().parent().find("form.service_checking_form").serialize() + "&action_type=" + data_action;
            $(this).parent().parent().find(".service_checking_note").parent().find("span.error").remove();
            $(this).parent().parent().find(".service_checking_reported_staff_id").parent().find("span.error").remove();
            $.ajax({
                url: "service_checking_update.php",
                type: "POST",
                data:data,
                success: function(data){
                  console.log(data);
                  spinner.hide();
                  if(data){
                    showToast('<span style="color:#4db848;"><i class="material-icons left done">check_circle</i> របាយការណ៍របស់លោកអ្នកត្រូវបានរក្សាទុកដោយជោគជ័យ!</span>',1500);
                    $('.modal.open').modal('close');
                    setTimeout(function () {
                        location.reload();
                    },2000);
                  }else{
                    showToast('<span style="color:#4db848;"><i class="material-icons left fail">times</i> បរាជ័យ!</span>',1500);
                  }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                  console.log(errorThrown);
                }
            });
        }

      });

      $("input.service_checking_reported_staff_id,input#new_staff_branch_fc").on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
          event.preventDefault();
          $(this).parent().find("span.error").remove();
        }
      });

      function isNormalInteger(str) {
          return /^\+?(0|[1-9]\d*)$/.test(str);
      }



      $(".reload-page").click(function(){
        location.reload();
      });

      $("a.btn-edit-staff").on("click",function(){
        var staff_id = $(this).data("staff");
        spinner.show();
        $.ajax({
          url: "service_checking_udpate_staff.php",
          type: "POST",
          data:{'staff_id':staff_id,'action_type':'select'},
          success: function(response){
            spinner.hide();
            // console.log(response);
            $('.modal-service-checking.modal.open').modal('close');
            var response_obj = $.parseJSON(response);
            $("button.btn_update_staff").attr('data-staff',staff_id);

            $("input#new_staff_id_edit").val(response_obj.staff_id);
            $("input#new_staff_id_edit").parent().find('label').addClass(" active");

            $("input#new_staff_name_kh_edit").val(response_obj.staff_fullname_kh);
            $("input#new_staff_name_kh_edit").parent().find('label').addClass(" active");
            $("input#new_staff_name_en_edit").val(response_obj.staff_fullname_en);
            $("input#new_staff_name_en_edit").parent().find('label').addClass(" active");

            $("input#new_staff_branch_fc_edit").val(response_obj.branch_fc);
            $("input#new_staff_branch_fc_edit").parent().find('label').addClass(" active");

            $('#new_staff_position_edit').find('option[value="'+response_obj.staff_position+'"]').prop('selected', true);
            $("#new_staff_position_edit").formSelect();
            $('.modal#modal_edit_staff').modal('open');

            $(".modal-add-new-staff.open #branch_id_edit").val(response_obj.branch_id);
            $(".modal-add-new-staff.open #regional_id_edit").val(response_obj.regional_id);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
          }
        });
      });

      $("button.btn_update_staff").click(function(e){
        e.preventDefault();
        var staff_id = $(this).data("staff");
        var data_serrialized = $(this).parent().parent().parent().find('.service_checking_edit_staff').serialize() + "&staff_id=" + staff_id +"&action_type=update";
        spinner.show();
        $.ajax({
          url: "service_checking_udpate_staff.php",
          type: "POST",
          data:data_serrialized,
          success: function(response){
            spinner.hide();
            console.log(response);
            showToast('<span style="color:#4db848;"><i class="material-icons left done">check_circle</i> ព័ត៌មានបុគ្គលិកត្រូវបានកែប្រែដោយជោគជ័យ!</span>',1500);
            $('.modal.open').modal('close');
            setTimeout(function () {
                location.reload();
            },2000);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
          }
        });
      });

    
      jQuery("#btn-truncate-service-checking").on("click",function(e){
        e.preventDefault();
        if(confirm("តើលោកគ្រូ អ្នកគ្រូបានទាញយករបាយការណ៍ថ្ងៃនេះហើយឬនៅ? បើលុបចេញហើយមិនអាចយកមកវិញបានទេ!")){
          spinner.show();
          $.ajax({
            url: "service_checking_update.php",
            type: "POST",
            data:"action_type=truncate",
            success: function(data){
              spinner.hide();
              if(data=='truncate'){
                showToast('<span style="color:#4db848;"><i class="material-icons left done">check_circle</i> របាយការណ៍ត្រូវបានលុយចេញពីប្រព័ន្ធ!</span>',1500);
                setTimeout(function () {
                    location.reload();
                },1500);
              }else{
                showToast('<span style="color:#4db848;"><i class="material-icons left fail">times</i> បរាជ័យ!</span>',1500);
              }
            },
            error: function(jqXHR, textStatus, errorThrown) {
              console.log(errorThrown);
            }
          });
        }
      });

      $('ul.dropdown-content li').first().click(function(e){    
          $('ul.dropdown-content li:not(:first-child)').each(function(index) {
            $(this).find("input[type=checkbox]").prop("checked", $('ul.dropdown-content li').first().find("input[type=checkbox]").prop("checked"));                       
          });
      });

      if($("ul li.sidebar-item.active")){
        loadSpecialAccount('','','','','');
      }

      // validate add new staff
      $("form#special_account_update").validate({
        errorElement : 'span',       
        errorPlacement: function(error, element) {
            error.appendTo( element.parent() );
        },
        rules: {
          staff_id: {
            required: true,
            minlength:5,
            maxlength:5,
            regx: /^([0-9]{5,5})$/,
          }
        },
        messages: {
          staff_id: {
            required: "សូមបំពេញអត្តលេខរបស់បុគ្គលិក!",
            minlength: $.validator.format("អត្តលេខរបស់បុគ្គលិកយ៉ាងហោចណាស់ {0} ខ្ទង់"),
            maxlength: $.validator.format("អត្តលេខរបស់បុគ្គលិកយ៉ាងវែងបំផុត {0} ខ្ទង់"),
            regx:"សូមវាយបញ្ចូលលេខឡាតាំង (ឧ. 09072)",
          }
        },
         // specifying a submitHandler prevents the default submit, good for the demo
         submitHandler: function() {
          var $url = "https://www.prasac.com.kh/en/special-account-api";
          var $account_id = $(".modal.open #account_id").val();
          var $account_status = $(".modal.open #account_status option:selected").val();
          var $staff_id = $(".modal.open #staff_id").val();
          var $remark   = $(".modal.open textarea#remark").val();
          spinner.show();
          $.ajax({
              type: "GET",
              url: $url,
              dataType: "text",
              data:{ 'action': 'update','account_id':$account_id,'account_status':$account_status,'staff_id':$staff_id,'remark_text':$remark},
              error: function (request, error) {
                alert(" Can't do because: " + error);
              },
              success: function(data){
                  spinner.hide();
                  if(data){
                    loadSpecialAccount('','','','','');
                    showToast('<span style="color:#4db848;"><i class="material-icons left done">check_circle</i> លោកអ្នកបានផ្លាស់ប្ដូរលទ្ធផលគណនីលេខពិសេសដោយជោគជ័យ!</span>',1500);
                    $('.modal.open').modal('close');
                  }else{
                    showToast('<span style="color:#4db848;"><i class="material-icons left fail">times</i> មិនជោគជ័យទេ!</span>',1500);
                  }
              },
              error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
              }
          });
        },
        // set this class to error-labels to indicate valid fields
        success: function(label) {
          console.log(label);
        },
      });

      //=====Search Form===
      $("#btn_sp_search").on('click',function(e){
        var $search_client_name = $("#search_client_name").val();
        var $search_client_phone = $("#search_client_phone").val();
        var $search_date = $("#search_date").val();
        var $account_status = $("#account_status").val();
        var $search_staff_id = $("#search_staff_id").val();
        loadSpecialAccount($search_client_name,$search_client_phone,$search_date,$account_status,$search_staff_id);
      });
      //=====End Search====

      //===Export====
      $("#btn_sp_export").on('click',function(e){
        var $search_client_name = $("#search_client_name").val();
        var $search_client_phone = $("#search_client_phone").val();
        var $search_date = $("#search_date").val();
        var $account_status = $("#account_status").val();
        var $search_staff_id = $("#search_staff_id").val();
        e.preventDefault();
        spinner.show();
        var data = $('form#special_account_search').serializeArray();
        $.ajax({
            type:"POST",
            url:"../admin/export_special.php",
            cache:false,
            data:data,
            success: function(data)
            {
              console.log(data);
              spinner.hide();
              showToast('<span style="color:#4db848;"><i class="material-icons left">check_circle</i>  របាយការណ៍របស់លោកអ្នកដោនឡូតបានយ៉ាងជោគជ័យ</span>',3000);
              var a = document.createElement('a');
              a.href =data;
              a.download = 'SpecialAccountReport.xlsx';
              a.click();
              a.remove();
              console.log(data);
            }
        });
        
        loadSpecialAccount($search_client_name,$search_client_phone,$search_date,$account_status,$search_staff_id);
      });
      //====End Export==

      //====Branch selected for branch====
      $(document).on('change','#account_status',function() {
        var $status = $(this).val();
        if($status==2){
          $("#branch_section").slideDown();
        }else{
          $("#branch_section").slideUp();
        }
      });

      $(document).on('click','.btn-edit-special',function(e){
        var $url = "https://www.prasac.com.kh/en/special-account-api";
        var $account_id = $(this).data('account_id');
        spinner.show();
        $.ajax({
          type: "GET",
          url: $url,
          dataType: "text",
          data: { 'action': 'edit','account_id':$account_id},
          error: function (request, error) {
            console.log(arguments);
            alert(" Can't do because: " + error);
          },
          success: function (data) {
            var json = $.parseJSON(data)[0];
            spinner.hide();
            $(".special_account_update")[0].reset();
            $("#special-form").modal('open');
            var $account_id     = json.account_id;
            var $account_number = json.account_number;
            var $client_name    = json.client_name;
            var $client_phone   = json.client_phone;
            var $account_number = accountNumberFormat(json.account_number);
            var $account_price  = json.account_price;
            var $staff_id       = json.staff_id;
            var $account_status = json.status;
            var $remark         = json.remark;

            $("#account_id").val($account_id);
            $("#client_name").val($client_name);
            $("#client_name").siblings('label').addClass('active');

            $("#client_phone").val($client_phone);
            $("#client_phone").siblings('label').addClass('active');

            $("#account_number").val($account_number);
            $("#account_number").siblings('label').addClass('active');

            $("#account_price").val($account_price);
            $("#account_price").siblings('label').addClass('active');

            $("#staff_id").val($staff_id);
            $("#staff_id").siblings('label').addClass('active');

            $(".modal.open #account_status").find('option[value="'+$account_status+'"]').prop('selected', true);
            $('.modal.open #account_status').formSelect();

            $("#remark").val($remark);
            $("#remark").siblings('label').addClass('active');
          }
        });
      });

    });//End document ready

    
    // function changeOption() {
    //   alert($("#account_status option:selected").val());
    // }

    function accountNumberFormat(account_number){
      var accNo = account_number.split(" ").join(""); // remove whitespace
        if (accNo.length > 0) {
          accNo = accNo.match(new RegExp('.{1,3}', 'g')).join(" ");
        }
        return accNo;
    }

    function loadSpecialAccount($client_name='',$client_phone='',$booking_date='',$account_status='',$search_staff_id=''){
      $("table.account-list>tbody").html('');
      $html_content = '';
      var $url = "https://www.prasac.com.kh/en/special-account-api";
      spinner.show();
      $.ajax({
          type: "GET",
          url: $url,
          dataType: "text",
          data:{ 'action': 'load','client_name':$client_name,'client_phone':$client_phone,'booking_date':$booking_date,'account_status':$account_status,'staff_id':$search_staff_id},
          error: function (request, error) {
            alert(" Can't do because: " + error);
          },
          success: function(data){
            if(data.length>0){
              var json_data = $.parseJSON(data);
              $.each(json_data, function(index, value) {
                index++;
                var $is_read = value.is_read==1?'read-items':'unread-items';
                var $status_class = '';
                var $icon_status  = '';
                var $status_title = '';
                var $status_title_en = '';
                var $account_status = value.status;
                var $remark_status = value.remark!=''?'tooltipped':'';
                

                if($account_status ==1){
                  $status_class = 'pending';
                  $icon_status  = 'info';
                  $status_title = 'មិនទាន់បានបើកគណនី';
                  $status_title_en = 'Pending';
                }
                if($account_status ==2){
                  $status_class = 'done';
                  $icon_status  = 'check_circle';
                  $status_title = 'បានបើកគណនីរួចរាល់';
                  $status_title_en = 'Done';
                }


                if($account_status ==3){
                  $status_class = 'fail';
                  $icon_status  = 'cancel';
                  $status_title = 'លុបការកក់';
                  $status_title_en = 'Cancel';
                }
                
                
                $html_content +='<tr class="'+$is_read+' row-item">';
                $html_content +='<td class="center view">'+index+'</td>';
                $html_content +='<td class="center view account-number">'+value.account_number+'</td>';
                $html_content +='<td class="text-right view">'+value.account_price+'</td>';
                $html_content +='<td class="view">'+value.client_name+'</td>';
                $html_content +='<td class="center view">'+value.client_phone+'</td>';
                $html_content +='<td class="center view">'+value.created_date+'</td>';
                $html_content +='<td class="text-left '+$status_class+' tooltipped" style="font-size:12px;"  data-position="left" data-tooltip="'+$status_title+'"><i style="float:left;margin-top:0px;" alt="" class="material-icons '+$status_class+'">'+$icon_status+'</i>&nbsp;'+$status_title_en+'</td>';
                $html_content +='<td class="text-left view '+$remark_status+'" data-tooltip="'+value.remark+'"><span class="one-row-text">'+value.remark+'</span></td>';
                $html_content += '<td class="center"><a class="modal-trigger btn-edit-special tooltipped" data-position="left" data-tooltip="ផ្លាស់ប្ដូរស្ថានភាពគណនី" href="#"  style="color:#4db848;" data-account_id="'+value.account_id+'"><i class="fa fa-edit"></i></a></td>';
                $html_content +='</tr>';
              });

              $("table.account-list>tbody").html($html_content);
              spinner.hide();
              //===Format Number===
              $('.row-item>.account-number').each(function() {
                var accNo = jQuery(this).html().split(" ").join(""); // remove whitespace
                if (accNo.length > 0) {
                  accNo = accNo.match(new RegExp('.{1,3}', 'g')).join(" ");
                }
                $(this).html(accNo);
              });
            }else{
              $html_none = '<tr class="row-item"><td class="center view" colspan="9">គ្មានទិន្នន័យនោះទេ!</td></tr>';
              $("table.account-list>tbody").html($html_none);
              spinner.hide();
            }
            $('.tooltipped').tooltip();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
          }
      });

    }
  </script>
  <?php 
}
?>