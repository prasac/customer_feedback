<?php 
    include('../admin/session.php');
    $fc_number =  isset($_POST['fcsite'])?$_POST['fcsite']:0;
    $filter_type = isset($_POST['filter_type'])?$_POST['filter_type']:0;
    if($filter_type=='Single'){
        $query = "SELECT id,regional_id,CONCAT(fc,'_',name_kh) AS branch_name FROM branches  WHERE fc='$fc_number' LIMIT 1";
        $result_query = $conn->query($query);
        if($result_query->num_rows>0){
            $postData = array();
            $row = mysqli_fetch_object($result_query);
            echo json_encode($row);
        }else{
            echo 0;
        }
    }else{
        $query = "SELECT id,fc,name_kh,regional_id FROM branches  WHERE fc LIKE '%$fc_number%'";
        $result_query = $conn->query($query);
        if($result_query->num_rows>0){
            $postData = array();
            while($row = mysqli_fetch_object($result_query)) {
                array_push($postData,array(
                    "name" =>  $row->name_kh,
                    "branch_id" =>$row->id,
                    "fc" =>$row->fc,
                    "name_fc" =>$row->fc.'_'.$row->name_kh,
                    'regional_id' =>$row->regional_id,
                ));
            }
            echo json_encode($postData);
        }else{
            echo 0;
        }
    }
    exit;
?>