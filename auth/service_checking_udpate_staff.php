<?php 
    include('../admin/session.php');
    $date = new DateTime();
    $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
    $current_date_time = $date->format("Y-m-d h:i:s");
    $action_type = isset($_POST['action_type'])?$_POST['action_type']:'';
    $staff_id_post = isset($_POST['staff_id'])?$_POST['staff_id']:'';
    try {
        if($action_type=="select"){
            $query_staff = $conn->query("SELECT 
              staff.staff_id AS staff_id, 
              staff.staff_position AS staff_position,
              staff.staff_fullname_kh AS staff_fullname_kh,
              staff.staff_fullname_en AS staff_fullname_en,
              branch.fc AS branch_fc,
              staff.branch_id AS branch_id,
              staff.regional_id AS regional_id
            FROM tbl_service_quality_checking_staff as staff LEFT JOIN branches as branch ON branch.id=staff.branch_id
            WHERE staff.staff_id=$staff_id_post LIMIT 1");

            $query_staff_obj = mysqli_fetch_object($query_staff);
            echo json_encode($query_staff_obj);
        }
        if($action_type=="update"){
            $new_staff_id = isset($_POST['new_staff_id'])?$_POST['new_staff_id']:'';
            $new_staff_name_kh = isset($_POST['new_staff_name_kh'])?$_POST['new_staff_name_kh']:'';
            $new_staff_name_en = isset($_POST['new_staff_name_en'])?$_POST['new_staff_name_en']:'';
            $new_staff_position_edit = isset($_POST['new_staff_position_edit'])?$_POST['new_staff_position_edit']:'';
            $branch_id = isset($_POST['new_branch_id'])?$_POST['new_branch_id']:'';
            $regional_id = isset($_POST['new_regional_id'])?$_POST['new_regional_id']:'';
            $query_script = "UPDATE `tbl_service_quality_checking_staff` SET `staff_id` = '$new_staff_id',`staff_fullname_kh`='$new_staff_name_kh',`staff_fullname_en`='$new_staff_name_en',`staff_position`='$new_staff_position_edit',`branch_id`='$branch_id',`regional_id`='$regional_id' WHERE `staff_id`=$staff_id_post LIMIT 1";
            $query_staff_update =  $conn->query($query_script);
            // var_dump($query_script);
        }

    }catch (Exception $e) {
        echo 'Caught exception:',$e->getMessage(),"\n";
    }
    exit;
?>