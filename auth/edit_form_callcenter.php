            
    <?php 
    $query = "SELECT 
            id.id as feedback_id,
            cd.id as client_id,
            id.fc as branch_fc,
            cd.client_name as client_name,
            cd.client_gender as gender,
            cd.client_phone as phone,
            cd.client_category as category,
            cd.client_type as clienttype,
            id.id as issue_id,
            id.staff_id as staff_id,
            id.staff_position as staff_position_id,
            id.detail_submited as issue,
            id.issue_date as issuedate,
            DATE_FORMAT(id.issue_date,'%I:%i %p') as issue_time,
            id.is_read as is_read,
            id.detail_responsed as solution,
            id.status as report_status,
            id.area as area,
            fs.title as status_en,
            fs.title_kh as status_kh,
            us.display_name as agent_name,
            ch.title_kh as channel,
            br.name_kh as branch_name,
            re.region as regional_code,
            re.name as regional_name,
            icat.title_kh as issue_category,
            id.client_channel as committed_by,
            id.issue_date as issue_date,
            id.service_action as service_action
    FROM tbl_issue_detail as id 
        INNER JOIN tbl_client_detail as cd ON cd.id=id.client_id
        INNER JOIN tbl_issue_category as icat ON icat.id=cd.client_category
        INNER JOIN tbl_user as us on id.user_id=us.id
        INNER JOIN tbl_channel as ch on ch.id=id.client_channel
        INNER JOIN branches as br on br.fc=id.fc
        INNER JOIN tbl_regional as re on re.id=br.regional_id
        INNER JOIN tbl_feedback_status as fs ON fs.id=id.status 
        WHERE id.id=$item_id LIMIT 1";
    $result = $conn->query($query);
    if(($result) && $result->num_rows>0){
        $row = mysqli_fetch_object($result);
    ?>
    <form method="POST" action="" class="editreport_callcenter">
        <div class="row">
            <div class="input-field col s4 offset-s2">
                <input id="client_name" type="text" name="client_name" value="<?php echo $row->client_name; ?>" class="validate">
                <label for="client_name" class="active">ឈ្មោះអតិថិជន</label>
            </div>
            <div class="input-field col s4">
                <select name="client_gender">
                    <option value=""></option>
                    <option value="1" <?php echo $row->gender==1? 'selected':''; ?>> ប្រុស</option>
                    <option value="2" <?php echo $row->gender==2? 'selected':''; ?>><i class="fa fa-female" aria-hidden="true"></i> ស្រី</option>
                </select>
                <label for="client_gender" class="active">ភេទ</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s4  offset-s2">
                <input id="client_phone"  name="client_phone" value="<?php echo $row->phone; ?>" type="tel" class="validate" placeholder="ឧទាហរណ៍៖ 012345678">
                <label for="client_phone" class="active">លេខទូរស័ព្ទ</label>
            </div>
            <div class="input-field col s4">
                <select name="issue_category" id="issue_category" class="validate">
                    <option  value=""></option>
                    <?php 
                        $query = "SELECT * from tbl_issue_category ORDER BY id ASC";
                        $result_query = $conn->query($query);
                        if(($result_query) && $result_query->num_rows>0){
                            while($rows = mysqli_fetch_object($result_query)) {
                                echo '<option';
                                echo ($row->category==$rows->id)?' selected':'';
                                echo ' value="'.$rows->id.'">'.$rows->title_kh.'</option>';
                            }
                        }
                    ?>
                </select>
                <label for="issue_category" class="active">ប្រភេទមតិ/ព័ត៌មាន</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s2 offset-s2" class="validate">
                <select name="client_financial_service" id="main_service" class="browser-default">
                    <option  value=""></option>
                    <?php 
                    $query = "SELECT * from tbl_financial_services WHERE id=parent_id ORDER BY tbl_financial_services.order ASC";
                    $result_query = $conn->query($query);
                    if(($result_query) && $result_query->num_rows>0){
                        while($rows = mysqli_fetch_object($result_query)) {
                            echo '<option';
                            echo ($row->clienttype==$rows->id)?' selected':'';
                            echo ' value="'.$rows->id.'">'.$rows->title_kh.'</option>';
                        }
                        echo '<option value="0"';
                        echo $row->clienttype==0?' selected':'';
                        echo '>ផ្សេងៗ</option>';
                    }
                    ?>
                </select>
                <label for="client_financial_service" class="active">ប្រភេទសេវាហិរញ្ញវត្ថុ</label>
            </div>
            <div class="input-field col s2" class="validate">
                    <select name="sub_financial_service" id="sub_service" class="browser-default">
                        <option value=""></option>
                        <?php 
                        $query = "SELECT * from tbl_financial_services WHERE id !=parent_id ORDER BY tbl_financial_services.order ASC";
                        $result_query = $conn->query($query);
                        if(($result_query) && $result_query->num_rows>0){
                            while($rows = mysqli_fetch_object($result_query)) {
                                echo '<option';
                                echo ($row->service_action==$rows->id)?' selected':'';
                                echo ' value="'.$rows->id.'" class="'.$rows->parent_id.'">'.$rows->title_kh.'</option>';
                            }
                        }
                        ?>
                    </select>
                    <label for="sub_financial_service" class="active">បង្ខាំង/ដំណើរការ</label>
                </div>
            <div class="input-field col s4">
                <select name="feedback_category" class="validate">
                    <option  value=""></option>
                    <?php 
                    $query = "SELECT * from tbl_area ORDER BY id ASC";
                    $result_query = $conn->query($query);
                    if(($result_query) && $result_query->num_rows>0){
                        while($rows = mysqli_fetch_object($result_query)) {
                            echo '<option';
                            echo ($row->area==$rows->id)?' selected':'';
                            echo ' value="'.$rows->id.'">'.$rows->title_kh.'</option>';
                        }
                    }
                    ?>
                </select>
                <label for="feedback_category" class="active">ប្រភេទ</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s4 offset-s2" >
                <textarea name="issue_detail" id="issue_detail" class="materialize-textarea" data-length="1500" maxlength="1500"><?php echo $row->issue; ?></textarea>
                <label for="issue_detail">បញ្ហា/ព័ត៌មាន</label>
            </div>
            <div class="input-field col s4">
                <textarea name="solution" id="solution" class="materialize-textarea" data-length="2500" maxlength="2500"><?php echo $row->solution; ?></textarea>
                <label for="solution">ការឆ្លើយតប/ដំណោះស្រាយ</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col col s4  offset-s2 result-status">
                <select name="result_status">
                    <option  value=""></option>
                    <option value="1" <?php echo $row->report_status == 1?' selected ':'';?> >Done</option>
                    <option value="2" <?php echo $row->report_status == 2?' selected':'';?>>Pending</option>
                    <option value="3" <?php echo $row->report_status == 3?' selected ':'';?>>Fail</option>
                </select>
                <label for="result_status" class="active">លទ្ធផល</label>
            </div>
            <div class="input-field col s4">
                <select name="related_position">
                    <option value="0"></option>
                    <?php 
                    $query = "SELECT * from tbl_agent ORDER BY tbl_agent.order ASC";
                    $result_query = $conn->query($query);
                    if(($result_query) && $result_query->num_rows>0){
                        while($rows = mysqli_fetch_object($result_query)) {
                            echo '<option';
                            echo ($row->staff_position_id==$rows->id)?' selected':'';
                            echo ' value="'.$rows->id.'">'.$rows->title_kh.'</option>';
                        }
                    }
                    ?>
                </select>
                <label for="related_position" class="active">បង្កដោយ</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s2 offset-s2">
                <?php 
                    $date = new DateTime();
                    $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
                    $current_date_time = $date->format("d-m-Y");
                ?>
                <input type="text" class="issue_date_edit" id="issue_date_edit" name="issue_date" value="<?php echo $row->issue_date?date("Y-m-d",strtotime($row->issue_date)):'';?>">
                <label for="issue_date_edit">កាលបរិច្ឆេទបញ្ហា/ព័ត៌មាន</label>
            </div>
            <div class="input-field col s2">
                <input type="text" class="issue_time" id="issue_time" name="issue_time" value="<?php echo $row->issue_time?$row->issue_time:'';?>">
                <label for="issue_time" class="active">ម៉ោង</label>
            </div>
            <div class="input-field col s4">
                <select name="response_channel">
                    <option  value=""></option>
                    <?php 
                    $user_type = isset($_SESSION['login_id'])?$_SESSION['login_id']:'';
                    $condition = $user_type==2? " WHERE id IN(4,5,6,7,8,9)":'';
                    $query = "SELECT * FROM tbl_channel $condition ORDER BY id ASC";
                    $result_query = $conn->query($query);
                    if(($result_query) && $result_query->num_rows>0){
                        while($rows = mysqli_fetch_object($result_query)) {
                            echo '<option';
                            echo ($row->committed_by==$rows->id)?' selected':'';
                            echo ' value="'.$rows->id.'">'.$rows->title_kh.'</option>';
                        }
                    }
                    ?>
                </select>
                <label for="response_channel" class="active">តាមរយៈ</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s2 offset-s2">
                <input id="fcsite" name="fcsite" type="text" class="validate" placeholder="ឧទាហរណ៍៖ 423" value="<?php echo $row->branch_fc;?>">
                <label for="fcsite">កូដសាខា</label>
            </div>
            <div class="input-field col s2">
                <input id="branch_name" name="branch_name" type="text" class="validate" readonly>
                <label for="branch_name">សាខា</label>
            </div>
            <div class="input-field col s2">
                <input id="regional_name" name="regional_name" type="text" class="validate" readonly>
                <label for="regional_name">ភូមិភាគ</label>
            </div>
            <div class="input-field col s2">
                <input id="staff_id" name="staff_id" type="text" class="validate" value="<?php echo $row->staff_id;?>"​ placeholder="ឧទាហរណ៍៖ 09072">
                <label for="staff_id">អត្តលេខបុគ្គលិក</label>
            </div>
        </div>
        
        <div class="row">
            <div class="input-field col s7 offset-s5">
                <input type="hidden" value="<?php echo isset($_SESSION['login_user_fc'])?$_SESSION['login_user_fc']:0;?>" name="logged_branch_id"/>
                <input type="hidden" value="<?php echo $row->feedback_id;?>" name="feedback_id"/>
                <input type="hidden" value="<?php echo $row->client_id;?>" name="client_id"/>
                <button class="btn waves-effect waves-light" type="submit" name="btn_update_callcenter"><i class="fa fa-floppy-o"></i> រក្សាទុករបាយការណ៍
                </button>
            </div>
        </div>
    </form>
    <?php 
    }
    ?>