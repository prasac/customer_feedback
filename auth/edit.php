<?php 
  //If add new report form is submitted
  $date = new DateTime();
  $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));

  //Update status to unread when user click edit report
  $issue_id = isset($_GET['item'])?$_GET['item']:0;
  if(isset($_SESSION['login_id']) && $_SESSION['login_id'] ==1){ // Only admin which enable to update status to is read when click on view
      if($issue_id){
          $result_isread  =   $conn->query("UPDATE tbl_issue_detail SET is_read=1 WHERE id=$issue_id LIMIT 1");
      }
  }
  
  if(isset($_POST['btn_update_branch'])){
    ob_start();
    $feedback_id = $_POST['feedback_id'];
    $client_id = $_POST['client_id'];
    $client_name = $_POST['client_name'];
    $client_gender = $_POST['client_gender'];
    $client_phone = isset($_POST['client_phone'])?$_POST['client_phone']:0;
    $client_category =$_POST['issue_category'];
    $client_type = $_POST['client_financial_service'];

    $query_cd = "UPDATE `tbl_client_detail` SET `client_name` = '$client_name',client_gender=$client_gender,client_phone='$client_phone',client_category=$client_category,client_type=$client_type WHERE id=$client_id LIMIT 1";
    //echo $query_cd;exit;
    $result_client_query  = $conn->query($query_cd);
    if($result_client_query){
      $current_datetime = $date->format("Y-m-d H:i:s");
    //   $issue_date = $_POST['issue_date'].' '.date("H:i:s",strtotime($date->format("H:i:s"))); `issue_date`='$issue_date',
      $client_area = $_POST['feedback_category'];
      $response_channel = $_POST['response_channel'];
      $staff_id = $_POST['staff_id'];
      $related_position = $_POST['related_position'];
      $issue_detail = mysqli_real_escape_string($conn,$_POST['issue_detail']);
      $solution = mysqli_real_escape_string($conn,$_POST['solution']);
      $result_status = $_POST['result_status'];
      $result_status = $_POST['result_status'];
      $last_inserted_client_id  = $conn->insert_id;
      $query_issue  = "UPDATE `tbl_issue_detail` SET 
                     `area`=$client_area,`client_channel`=$response_channel,`staff_id`='$staff_id',`staff_position`=$related_position,`detail_submited`='$issue_detail',`detail_responsed`='$solution',`status`='$result_status',`modified_date`='$current_datetime' WHERE id=$feedback_id LIMIT 1"; 
       // echo $query_issue;exit;
      $result_issue_detail  = $conn->query($query_issue);

      echo '
          <!--Import jQuery before materialize.js-->
          <script type="text/javascript" src="http://feedback.local/library/jquery.min.js"></script>
          <!--Import Materialize-Stepper JavaScript -->
          <script src="http://feedback.local/library/materialize-v1.0.0/js/materialize.min.js"></script>
          <script src="http://feedback.local/library/materialize-datetimepicker-master/js/materializedatetimepicker.js"></script>
          <script type="text/javascript" src="http://feedback.local/library/jquery-chained.min.js"></script>
          <script type="text/javascript" src="http://feedback.local/library/jquery-validation/dist/jquery.validate.min.js"></script>';

      if($result_issue_detail){
        echo '
          <script>
                $(document).ready(function(e){
                  showToast("<i class=material-icons style=color:#4db848;>check_circle</i>&nbsp;<span style=color:#4db848;padding:5px 10px;>របាយការណ៍ ត្រូវបាន​រក្សាទុក​ដោយជោគជ័យ</span>",1500);
                  setTimeout(function() {
                    window.history.go(-2);
                  }, 1500);
                });
          </script>';
      }else{
          echo '
          <script>
                $(document).ready(function(e){
                  showToast("<i class=material-icons fail>cancel</i>&nbsp;<span style=color:red;padding:5px 10px;>របាយការណ៍ មិនត្រូវបានរក្សាទុកនោះទេ</span>",1500);
                  window.history.go(-2);
                });
          </script>';
      }
      ob_end_flush();
    }
  } 

  //If add new report form is submitted from social user
  if(isset($_POST['btn_update_social']) || isset($_POST['btn_update_callcenter'])){
    ob_start();
    $feedback_id = $_POST['feedback_id'];
    $client_id = $_POST['client_id'];
    $client_name = $_POST['client_name'];
    $client_gender = $_POST['client_gender'];
    $client_phone = $_POST['client_phone'];
    $client_category = $_POST['issue_category'];
    $client_type = $_POST['client_financial_service'];
    $sub_financial_service = isset($_POST['sub_financial_service']) && $_POST['sub_financial_service'] !=''?$_POST['sub_financial_service']:0;
    $query_cd = "UPDATE `tbl_client_detail` SET `client_name` = '$client_name',`client_gender`=$client_gender,`client_phone`='$client_phone',`client_category`=$client_category,`client_type`=$client_type WHERE id=$client_id LIMIT 1";
    $result_client_query  = $conn->query($query_cd);
    // echo $query_cd;exit;
    if($result_client_query){
      $user_id  = isset($_SESSION['user_id'])?$_SESSION['user_id']:'';
      $issue_date = $_POST['issue_date'].' '.date("H:i",strtotime($_POST['issue_time']));
      $modified_date = $date->format("Y-m-d H:i:s");
      $fcsite =  $_POST['fcsite'];
      $client_area = $_POST['feedback_category'];
      $response_channel = $_POST['response_channel'];
      $staff_id = $_POST['staff_id'];
      $related_position = $_POST['related_position'];
      $issue_detail = mysqli_real_escape_string($conn,$_POST['issue_detail']);
      $solution =  mysqli_real_escape_string($conn,$_POST['solution']);
      $result_status = $_POST['result_status'];
      $query_issue  = "UPDATE `tbl_issue_detail` SET 
                    `fc`='$fcsite',`area`='$client_area',`client_channel`='$response_channel',`service_action`='$sub_financial_service',`staff_id`='$staff_id',`staff_position`='$related_position',`detail_submited`='$issue_detail',`detail_responsed`='$solution',`status`='$result_status',`issue_date`='$issue_date',`modified_date`='$modified_date' WHERE id=$feedback_id LIMIT 1"; 
      $result_issue_detail  = $conn->query($query_issue);
    //   echo $query_issue;exit;

      echo '
          <!--Import jQuery before materialize.js-->
          <script type="text/javascript" src="http://feedback.local/library/jquery.min.js"></script>
          <!--Import Materialize-Stepper JavaScript -->
          <script src="http://feedback.local/library/materialize-v1.0.0/js/materialize.min.js"></script>
          <script src="http://feedback.local/library/materialize-datetimepicker-master/js/materializedatetimepicker.js"></script>
          <script type="text/javascript" src="http://feedback.local/library/jquery-chained.min.js"></script>
          <script type="text/javascript" src="http://feedback.local/library/jquery-validation/dist/jquery.validate.min.js"></script>';
      if($result_issue_detail){
        echo '
          <script>
             $(document).ready(function(e){
                showToast("<i class=material-icons style=color:#4db848;>check_circle</i>&nbsp;<span style=color:#4db848;padding:5px 10px;>របាយការណ៍ ត្រូវបាន​រក្សាទុក​ដោយជោគជ័យ</span>",1500);
                setTimeout(function() {
                  window.history.go(-2);
                }, 1500);
              });
          </script>';
      }else{
          echo '
          <script>
                $(document).ready(function(e){
                  showToast("<i class=material-icons fail>cancel</i>&nbsp;<span style=color:red;padding:5px 10px;>របាយការណ៍ មិនត្រូវបានរក្សាទុកនោះទេ</span>",1500);
                  window.history.go(-2);
                });
          </script>';
      }
      ob_end_flush();
    }
  } 
  ?>
<!-- Modal Structure -->
<div class="col s12">
    <div class="row">
        <blockquote>កែប្រែរបាយការណ៍</blockquote>
        <?php 
            $login_user_type = $_SESSION['login_id']?$_SESSION['login_id']:'';
            $item_id    =   isset($_GET['item'])?$_GET['item']:0;
            switch($login_user_type){
                case 1:
                    $edit_form = 'edit_form_callcenter.php';
                    break;
                case 2:
                    $edit_form = 'edit_form_callcenter.php';
                    break;
                case 3:
                    $edit_form = 'edit_form_social.php';
                    break;
                case 4:
                    $edit_form = 'edit_form_branch.php';
                    break;
                case 5:
                    $edit_form = 'edit_form_callcenter.php';
                    break;
            }
            include($edit_form);
        ?>
    </div>
</div>