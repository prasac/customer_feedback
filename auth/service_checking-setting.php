
      <blockquote>ការកំណត់</blockquote>
      <?php
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
        $current_date_time = $date->format("Y-m-d h:i:s");
      ?>
        <div class="row">
            <div class="col s12 l12">
                <form class="form-horizontal import_form" action="" method="POST" enctype="multipart/form-data">
                    <div class="col s12 l12 right">
                        <div class="row">
                            <div class="right green-text">
                                <div class="left">
                                    <span class="left reload-text">ផ្ទុកទិន្នន័យឡើងវិញ</span> <i class="material-icons reload-page right">cached</i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s4 l4 right">
                        <div class="row"  style="margin-bottom:0;">
                            <div class="file-field input-field">
                                <button style="margin-left:15px;" type="submit" name="btn_upload_file" class="waves-effect waves-light btn right"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                <div class="btn">
                                    <span><i class="fa fa-file-excel-o" aria-hidden="true"></i></span>
                                    <input type="file" name="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" >
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Browse yYour xlsx file to upload">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s8 offset-s2 left">
                        <?php
                        include('../library/PHPExcel/Classes/PHPExcel.php');
                        $message_text ='';
                        if(isset($_POST['btn_upload_file'])){
                            $csvMimes = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){
                                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                                    $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                                    $n=1;
                                    $uploads_dir = '../uploads';
                                    $name = $_FILES["file"]["name"];
                                    $inputFileName = '../uploads/'.$name;
                                    $files = glob('../uploads/*'); // get all file names
                                    foreach($files as $file){ // iterate files
                                        if(is_file($file)) {
                                            unlink($file); // delete file
                                        }
                                    }

                                    move_uploaded_file($_FILES['file']['tmp_name'], "$uploads_dir/$name");
                                    
                                    try{
                                        $inputFileType  =   PHPExcel_IOFactory::identify($inputFileName);
                                        $objReader      =   PHPExcel_IOFactory::createReader($inputFileType);
                                        $objPHPExcel    =   $objReader->load($inputFileName);
                                        $num_exist = 0;
                                        $rowData = array();
                                        $RowNum = 0;
                                        echo '<div class="left"><p class="done"><b><i class="fa fa-info-circle fa-2x"></i> សូមធ្វើការជ្រើសរើស Sheet ដែលលោកអ្នកចង់ Import</b></p>';
                                        
                                        for($i=0;$i<$objPHPExcel->getSheetCount();$i++){
                                            $sheet =  $objPHPExcel->getSheet($i);
                                            if($sheet->getSheetState()=='visible'){
                                                $sheetTitle = $sheet->getTitle();
                                                $checkedDefault = $i ==0 ?' checked="checked"':'';
                                                echo "<span>
                                                        <label><input name='sheet[]'  value='$i' type='checkbox' $checkedDefault /> <span>  $sheetTitle</span></label>
                                                    </span>";
                                            }
                                        }
                                        echo '</div>';
                                        echo '<div class="right newsuggestion">
                                                <p class="done"><b><i class="fa fa-info-circle fa-2x"></i> ជ្រើសរើសរបៀបដែលលោកអ្នកត្រូវ Import</b></p>
                                                <span><label><input name="functionalities" value="1" type="radio" checked="checked" /> <span> Override All</span></label>
                                                <span><label><input name="functionalities" value="2" type="radio" /> <span> New Only</span></label>
                                                <span><label><input name="functionalities" value="3" type="radio" /> <span> Update Or New</span></label></span>
                                                <button style="margin-left:15px;" type="submit" id="process_import" name="process_import" class="waves-effect waves-light btn right">Start Import</button>';
                                        echo '</div>';
                                        fclose($csvFile);
                                    }catch(Exception $e){
                                        die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                                    }
                                }else{
                                    $message_text = 'Not supported file.';
                                }
                                
                            }else{
                                $message_text = 'No file uploaded to import';
                            }
                        }
                        $existing_staff = $update_existing_staff = $override_staff = 0;
                        $existing_position = $update_existing_position = $override_position = 0;
                        $existing_indicator = $update_existing_indicator = $override_indicator = 0;

                        if(isset($_POST['process_import'])){
                            try{
                                $path = "../uploads/"; ///the folder path
                                $theFilePath = "";
                                $theFileName = glob($path . "*.xlsx");
                                $full_file_path = $theFileName[0];

                                $inputFileType  =   PHPExcel_IOFactory::identify($full_file_path);
                                $objReader      =   PHPExcel_IOFactory::createReader($inputFileType);
                                $objPHPExcel    =   $objReader->load($full_file_path);
                                echo '<div class="left"><p class="done"><b><i class="fa fa-info-circle fa-2x"></i> សូមធ្វើការជ្រើសរើស Sheet ដែលលោកអ្នកចង់ Import</b></p>';
                                for($i=0;$i<$objPHPExcel->getSheetCount();$i++){
                                    $sheet =  $objPHPExcel->getSheet($i);
                                    if($sheet->getSheetState() == 'visible'){
                                        $sheetTitle = $sheet->getTitle();
                                        $checkedDefault = in_array($i,$_POST['sheet'])?' checked="checked"':'';
                                        echo "<span>
                                                <label><input name='sheet[]'  value='$i' type='checkbox' $checkedDefault /> <span>  $sheetTitle</span></label>
                                            </span>";
                                    }
                                }
                                echo '</div>';
                                ?>
                                <div class="right newsuggestion">
                                    <p class="done"><b><i class="fa fa-info-circle fa-2x"></i> ជ្រើសរើសរបៀបដែលលោកអ្នកត្រូវ Import</b></p>
                                    <span><label><input name="functionalities" value="1" type="radio" <?php echo isset($_POST['functionalities']) && $_POST['functionalities'] == 1? ' checked="checked"' :''?>/> <span> Override All</span></label>
                                    <span><label><input name="functionalities" value="2" type="radio" <?php echo isset($_POST['functionalities']) && $_POST['functionalities'] == 2? ' checked="checked"' :''?>/> <span> New Only</span></label>
                                    <span><label><input name="functionalities" value="3" type="radio" <?php echo isset($_POST['functionalities']) && $_POST['functionalities'] == 3? ' checked="checked"' :''?>/> <span> Update & New</span></label></span>
                                    <button style="margin-left:15px;" type="submit" id="process_import" name="process_import" class="waves-effect waves-light btn right">Start Import</button>
                                </div>
                                <?php
                                //Get the funcationality of selected
                                $functionality = isset($_POST['functionalities'])?$_POST['functionalities']:0;
                                if(isset($_POST['sheet'])){
                                    for($i=0;$i<count($_POST['sheet']);$i++){
                                        //Check sheet 
                                        $sheet_checked = $_POST['sheet'][$i];
                                        $objPHPExcel->setActiveSheetIndex($sheet_checked);
                                        $sheet =  $objPHPExcel->getActiveSheet();
                                        $sheetTitle = $sheet->getTitle();
                                        $highestRow = $sheet->getHighestRow(); 
                                        $highestColumn = $sheet->getHighestColumn();

                                        if($sheet_checked == 0){//The Staffing sheet checked
                                            
                                            if($functionality == 1){//If Override All Option checked
                                                $truncate_staff = $conn->query("TRUNCATE tbl_service_quality_checking_staff");
                                                if($truncate_staff){
                                                    for ($row = 1; $row <= $highestRow; $row++){
                                                        $staff_id = $sheet->getCellByColumnAndRow(0,$row)->getValue();
                                                        $staff_name_kh = $sheet->getCellByColumnAndRow(1,$row)->getValue();
                                                        $staff_name_en = $sheet->getCellByColumnAndRow(2,$row)->getValue();
                                                        $staff_position = $sheet->getCellByColumnAndRow(3,$row)->getValue();
                                                        $staff_branch = $sheet->getCellByColumnAndRow(4,$row)->getValue();
                                                        $staff_regional = $sheet->getCellByColumnAndRow(5,$row)->getValue();
                                                        if($row>1){
                                                            $inserted_query = "INSERT INTO `tbl_service_quality_checking_staff`
                                                                        (`staff_id`,`staff_fullname_kh`,`staff_fullname_en`,`staff_position`,`branch_id`,`regional_id`,`status`,`created_date`,`modified_date`) 
                                                                        VAlUES('$staff_id','$staff_name_kh','$staff_name_en','$staff_position','$staff_branch','$staff_regional',1,'$current_date_time','$current_date_time')";
                                                            $conn->query($inserted_query);
                                                            $override_staff++;
                                                        }
                                                    }
                                                }
                                            }

                                            if($functionality == 2){//If New Only Option checked
                                                for ($row = 1; $row <= $highestRow; $row++){
                                                    $staff_id = $sheet->getCellByColumnAndRow(0,$row)->getValue();
                                                    $staff_name_kh = $sheet->getCellByColumnAndRow(1,$row)->getValue();
                                                    $staff_name_en = $sheet->getCellByColumnAndRow(2,$row)->getValue();
                                                    $staff_position = $sheet->getCellByColumnAndRow(3,$row)->getValue();
                                                    $staff_branch = $sheet->getCellByColumnAndRow(4,$row)->getValue();
                                                    $staff_regional = $sheet->getCellByColumnAndRow(5,$row)->getValue();
                                                    if($row>1){
                                                        $check_old_staffs = $conn->query("SELECT * FROM tbl_service_quality_checking_staff WHERE staff_id=$staff_id LIMIT 1");
                                                        if($check_old_staffs && $check_old_staffs->num_rows> 0){ //If found exist not insert
                                                            $existing_staff++;
                                                        }else{
                                                            $inserted_query = "INSERT INTO `tbl_service_quality_checking_staff`
                                                                    (`staff_id`,`staff_fullname_kh`,`staff_fullname_en`,`staff_position`,`branch_id`,`regional_id`,`status`,`created_date`,`modified_date`) 
                                                                    VAlUES('$staff_id','$staff_name_kh','$staff_name_en','$staff_position','$staff_branch','$staff_regional',1,'$current_date_time','$current_date_time')";
                                                            $conn->query($inserted_query);
                                                        }
                                                    }
                                                }
                                                
                                            }

                                            if($functionality == 3){ //If New & Update Option checked
                                                for ($row = 1; $row <= $highestRow; $row++){
                                                    $staff_id = $sheet->getCellByColumnAndRow(0,$row)->getValue();
                                                    $staff_name_kh = $sheet->getCellByColumnAndRow(1,$row)->getValue();
                                                    $staff_name_en = $sheet->getCellByColumnAndRow(2,$row)->getValue();
                                                    $staff_position = $sheet->getCellByColumnAndRow(3,$row)->getValue();
                                                    $staff_branch = $sheet->getCellByColumnAndRow(4,$row)->getValue();
                                                    $staff_regional = $sheet->getCellByColumnAndRow(5,$row)->getValue();
                                                    if($row>1){
                                                        $check_old_staff = $conn->query("SELECT * FROM tbl_service_quality_checking_staff WHERE staff_id='$staff_id' LIMIT 1");
                                                        if($check_old_staff && $check_old_staff->num_rows > 0){ //If found exist not insert
                                                            $update_query = "UPDATE `tbl_service_quality_checking_staff`
                                                                            SET `staff_fullname_kh`='$staff_name_kh',`staff_fullname_en`='$staff_name_en',`staff_position`='$staff_position',`branch_id`='$staff_branch',`regional_id`='$staff_regional',`modified_date`='$current_date_time'
                                                                            WHERE staff_id=$staff_id LIMIT 1";
                                                            $update_existing_staff++;
                                                        }else{
                                                            $update_query = "INSERT INTO `tbl_service_quality_checking_staff`
                                                                    (`staff_id`,`staff_fullname_kh`,`staff_fullname_en`,`staff_position`,`branch_id`,`regional_id`,`status`,`created_date`,`modified_date`) 
                                                                    VAlUES('$staff_id','$staff_name_kh','$staff_name_en','$staff_position','$staff_branch','$staff_regional',1,'$current_date_time','$current_date_time')";
                                                        }
                                                        // echo  $update_query;
                                                        $conn->query($update_query);
                                                    }
                                                }
                                                
                                            }
                                           
                                        }

                                        if($sheet_checked==1){//The Position checked

                                            if($functionality == 1){//If Override All Option checked
                                                $truncate_staff = $conn->query("TRUNCATE tbl_service_quality_checking_staff_position");
                                                if($truncate_staff){
                                                    for ($row = 1; $row <= $highestRow; $row++){
                                                        $title_kh = $sheet->getCellByColumnAndRow(1,$row)->getValue();
                                                        $title_en = $sheet->getCellByColumnAndRow(2,$row)->getValue();
                                                        $position_order = $sheet->getCellByColumnAndRow(3,$row)->getValue();
                                                        if($row>1){
                                                            $inserted_query = "INSERT INTO `tbl_service_quality_checking_staff_position`
                                                                        (`staff_position_kh`,`staff_position_en`,`status`,`order`,`created_date`,`modified_date`) 
                                                                        VAlUES('$title_kh','$title_en',1,'$position_order','$current_date_time','$current_date_time')";
                                                            $conn->query($inserted_query);
                                                            $override_position++;
                                                        }
                                                    }
                                                }
                                            }

                                            if($functionality == 2){//If New Only Option checked
                                                for ($row = 1; $row <= $highestRow; $row++){
                                                    $position_id = $sheet->getCellByColumnAndRow(0,$row)->getValue();
                                                    $title_kh = $sheet->getCellByColumnAndRow(1,$row)->getValue();
                                                    $title_en = $sheet->getCellByColumnAndRow(2,$row)->getValue();
                                                    $position_order = $sheet->getCellByColumnAndRow(3,$row)->getValue();
                                                    if($row>1){
                                                        $check_old_staffs = $conn->query("SELECT * FROM `tbl_service_quality_checking_staff_position` WHERE id=$position_id LIMIT 1");
                                                        if($check_old_staffs && $check_old_staffs->num_rows> 0){ //If found exist not insert
                                                            $existing_position++;
                                                        }else{
                                                            $inserted_query = "INSERT INTO `tbl_service_quality_checking_staff_position`
                                                                                (`staff_position_kh`,`staff_position_en`,`status`,`order`,`created_date`,`modified_date`) 
                                                                                VAlUES('$title_kh','$title_en',1,'$position_order','$current_date_time','$current_date_time')";
                                                            $conn->query($inserted_query);
                                                        }
                                                    }
                                                }
                                                
                                            }

                                            if($functionality == 3){ //If New & Update Option checked
                                                for ($row = 1; $row <= $highestRow; $row++){
                                                    $position_id = $sheet->getCellByColumnAndRow(0,$row)->getValue();
                                                    $title_kh = $sheet->getCellByColumnAndRow(1,$row)->getValue();
                                                    $title_en = $sheet->getCellByColumnAndRow(2,$row)->getValue();
                                                    $position_order = $sheet->getCellByColumnAndRow(3,$row)->getValue();
                                                    if($row>1){
                                                        $check_old_staff = $conn->query("SELECT * FROM `tbl_service_quality_checking_staff_position` WHERE id='$position_id' LIMIT 1");
                                                        if($check_old_staff && $check_old_staff->num_rows > 0){ // If found exist not insert
                                                            $inserted_query = "UPDATE `tbl_service_quality_checking_staff_position`
                                                                                SET `staff_position_kh`='$title_kh',`staff_position_en`='$title_en',`order`='$position_order',`modified_date`='$current_date_time'
                                                                                WHERE id=$position_id LIMIT 1";
                                                            $update_existing_position++;
                                                        }else{
                                                            $inserted_query = "INSERT INTO `tbl_service_quality_checking_staff_position`
                                                                                (`staff_position_kh`,`staff_position_en`,`status`,`order`,`created_date`,`modified_date`) 
                                                                                VAlUES('$title_kh','$title_en',1,'$position_order','$current_date_time','$current_date_time')";
                                                        }
                                                        $conn->query($inserted_query);
                                                    }
                                                }
                                                
                                            }
                                        }


                                        if($sheet_checked==2){//The Indicator sheet checked

                                            if($functionality == 1){//If Override All Option checked
                                                $truncate_staff = $conn->query("TRUNCATE tbl_service_quality_checking_staff_key_indicator");
                                                if($truncate_staff){
                                                    for ($row = 1; $row <= $highestRow; $row++){
                                                        $indicator_title_kh = $sheet->getCellByColumnAndRow(1,$row)->getValue();
                                                        $indicator_title_en = $sheet->getCellByColumnAndRow(2,$row)->getValue();
                                                        $indicator_note = $sheet->getCellByColumnAndRow(3,$row)->getValue();
                                                        $indicator_order = $sheet->getCellByColumnAndRow(4,$row)->getValue();
                                                        if($row>1){
                                                            $inserted_query = "INSERT INTO `tbl_service_quality_checking_staff_key_indicator`
                                                                        (`title_en`,`title_kh`,`note`,`status`,`order`,`created_date`,`modified_date`) 
                                                                        VAlUES('$indicator_title_en','$indicator_title_kh','$indicator_note',1,'$indicator_order','$current_date_time','$current_date_time')";
                                                            $conn->query($inserted_query);
                                                            $override_indicator++;
                                                        }
                                                    }
                                                }
                                            }

                                            if($functionality == 2){//If New Only Option checked
                                                for ($row = 1; $row <= $highestRow; $row++){
                                                    $indicator_id = $sheet->getCellByColumnAndRow(0,$row)->getValue();
                                                    $indicator_title_kh = $sheet->getCellByColumnAndRow(1,$row)->getValue();
                                                    $indicator_title_en = $sheet->getCellByColumnAndRow(2,$row)->getValue();
                                                    $indicator_order = $sheet->getCellByColumnAndRow(3,$row)->getValue();
                                                    if($row>1){
                                                        $check_old_staffs = $conn->query("SELECT * FROM `tbl_service_quality_checking_staff_key_indicator` WHERE id=$indicator_id LIMIT 1");
                                                        if($check_old_staffs && $check_old_staffs->num_rows> 0){ //If found exist not insert
                                                            $existing_indicator++;
                                                        }else{
                                                            $inserted_query = "INSERT INTO `tbl_service_quality_checking_staff_key_indicator`
                                                                                    (`title_en`,`title_kh`,`note`,`status`,`order`,`created_date`,`modified_date`) 
                                                                                    VAlUES('$indicator_title_en','$indicator_title_kh',1,'$indicator_order','$current_date_time','$current_date_time')";
                                                            $conn->query($inserted_query);
                                                        }
                                                    }
                                                }
                                                
                                            }

                                            if($functionality == 3){ //If New & Update Option checked
                                                for ($row = 1; $row <= $highestRow; $row++){
                                                    $indicator_id = $sheet->getCellByColumnAndRow(0,$row)->getValue();
                                                    $indicator_title_kh = $sheet->getCellByColumnAndRow(1,$row)->getValue();
                                                    $indicator_title_en = $sheet->getCellByColumnAndRow(2,$row)->getValue();
                                                    $indicator_note = $conn->real_escape_string($sheet->getCellByColumnAndRow(3,$row)->getValue());
                                                    $indicator_order = $sheet->getCellByColumnAndRow(4,$row)->getValue();
                                                    if($row>1){
                                                        $check_old_staff = $conn->query("SELECT * FROM `tbl_service_quality_checking_staff_key_indicator` WHERE id='$indicator_id' LIMIT 1");
                                                        if($check_old_staff && $check_old_staff->num_rows > 0){ // If found exist not insert
                                                            $update_query = "UPDATE `tbl_service_quality_checking_staff_key_indicator`
                                                                                SET `title_kh`='$indicator_title_kh',`title_en`='$indicator_title_en',`note`='$indicator_note',`order`='$indicator_order',`modified_date`='$current_date_time'
                                                                                WHERE id=$indicator_id LIMIT 1";
                                                            $update_existing_indicator++;
                                                        }else{
                                                            $update_query = "INSERT INTO `tbl_service_quality_checking_staff_key_indicator`
                                                                                (`title_kh`,`title_en`,`note`,`status`,`order`,`created_date`,`modified_date`) 
                                                                                VAlUES('$indicator_title_kh','$indicator_title_en','$indicator_note',1,'$indicator_order','$current_date_time','$current_date_time')";
                                                        }
                                                        // echo $update_query;
                                                        $conn->query($update_query);
                                                    }
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }catch(Exception $e){
                                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                            }
                        }
                        ?>
                    </div>
                </form>
                <?php 
                    echo $override_staff !=''?"<div class='col s8 offset-s2 done messegn-import'><i class='fa fa-info-circle fa-2x'></i> Total imported staffs are/is<b> $override_staff </b></div>":'';
                    echo $existing_staff !=''?"<div class='col s8 offset-s2 done messegn-import'><i class='fa fa-info-circle fa-2x'></i> Existing Staff Exclude from importare/is <b> $existing_staff </b></div>":'';
                    echo $update_existing_staff !=''?"<div class='col s8 offset-s2 done messegn-import'><i class='fa fa-info-circle fa-2x'></i> Number of existing staff updated are/is<b> $update_existing_staff </b></div>":'';

                    echo $override_position !=''?"<div class='col s8 offset-s2 done messegn-import'><i class='fa fa-info-circle fa-2x'></i> Total imported positions are/is<b> $override_position </b></div>":'';
                    echo $existing_position !=''?"<div class='col s8 offset-s2 done messegn-import'><i class='fa fa-info-circle fa-2x'></i> Existing positions exclude from import are/is<b> $existing_position </b></div>":'';
                    echo $update_existing_position !=''?"<div class='col s8 offset-s2 done messegn-import'><i class='fa fa-info-circle fa-2x'></i> Number of existing positions updated are/is<b> $update_existing_position </b></div>":'';

                    echo $override_indicator !=''?"<div class='col s8 offset-s2 done messegn-import'><i class='fa fa-info-circle fa-2x'></i> Total imported key indicators are/is <b> $override_indicator </b></div>":'';
                    echo $existing_indicator !=''?"<div class='col s8 offset-s2 done messegn-import'><i class='fa fa-info-circle fa-2x'></i> Existing key indicators from import are/is<b> $existing_indicator </b></div>":'';
                    echo $update_existing_indicator !=''?"<div class='col s8 offset-s2 done messegn-import'><i class='fa fa-info-circle fa-2x'></i> Number of existing key indicator updated  are/is<b> $update_existing_indicator </b></div>":'';
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <ul class="tabs tabs-fixed-width">
                    <li class="tab col s3 tab-title"><a href="#staff_setting">រាយនាមបុគ្គលិក</a></li>
                    <li class="tab col s3 tab-title"><a href="#positions_setting">តួនាទីបុគ្គលិក</a></li>
                    <li class="tab col s3 tab-title"><a href="#indicator_setting">សូចនាករគុណភាពសេវាកម្ម</a></li>
                </ul>
            </div>
            <?php 
                $query = "SELECT
                        staff.id,
                        staff.staff_id,
                        staff.staff_fullname_kh,
                        staff.staff_fullname_en,
                        pos.staff_position_kh,
                        b.fc AS branch_fc,
                        r.region AS region_code
                    FROM tbl_service_quality_checking_staff AS staff
                    INNER JOIN branches AS b ON staff.branch_id = b.id
                    INNER JOIN tbl_regional AS r ON staff.regional_id=r.id
                    INNER JOIN tbl_service_quality_checking_staff_position AS pos ON pos.id=staff.staff_position
                    ORDER BY
                        staff.regional_id,
                        staff.branch_id ASC";
                $query_staff = $conn->query($query);
            ?>
            <div id="staff_setting" class="col s12">
                <!-- <a href="#!" class="modal-trigger btn btn-report right"  data-target="service_checking_staff"><i class="fa fa-user" aria-hidden="true"></i> បញ្ចូលបុគ្គលិកថ្មី</a> -->
                <?php echo ($query_staff) && $query_staff->num_rows>0?'<p class="done"><i class="fa fa-users" aria-hidden="true"></i> ចំនួនបុគ្គលិកសរុបសម្រាប់ធ្វើរបាយការណ៍គឺ៖ <b>'.$query_staff->num_rows."</b> នាក់</p>":'';?>
                <table class="highlight bordered admin-table responsive-table table-setting">
                    <thead>
                        <tr>
                            <th style="width:10%;">អត្តលេខបុគ្គលិក</th>
                            <th style="width:20%;">ឈ្មោះបុគ្គលិក(ខ្មែរ)</th>
                            <th style="width:20%;">ឈ្មោះបុគ្គលិក(ឡាតាំង)</th>
                            <th style="width:20%;">តួនាទី</th>
                            <th style="width:10%;">លេខ FC</th>
                            <th style="width:10%;">ភូមិភាគ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if($query_staff->num_rows>0){
                            while($row = mysqli_fetch_object($query_staff)) {
                                echo "<tr>
                                        <td class='center'>$row->staff_id</td>
                                        <td>$row->staff_fullname_kh</td>
                                        <td>$row->staff_fullname_en</td>
                                        <td>$row->staff_position_kh</td>
                                        <td class='center'>$row->branch_fc</td>
                                        <td class='center'>$row->region_code</td>
                                        </tr>";
                                        //<td class='center'><a class='done' href='Edite'><i class='fa fa-pencil-square-o fa-2x'></i></a> <a class='fail' href='Delete'><i class='fa fa-trash fa-2x'></i></a></td>
                            }
                        }else{
                            echo '<tr><td colspan="6" class="centered"><p style="color:#ee6e73;paddin:0;margin:0;">សូមអភ័យទោស! រកពុំឃើញបុគ្គលិកនៅក្នុងប្រព័ន្ធនោះទេ</p></td></tr>';
                        }
                        
                    ?>
                    </tbody>
                </table>
            </div>
            <div id="positions_setting" class="col s12">
                <!-- <a href="#!"  data-target="service_checking_position" class="modal-trigger btn btn-report right"><i class="fa fa-user-plus" aria-hidden="true"></i> បញ្ចូលតួនាទីថ្មី</a> -->
                <?php 
                    $query = "SELECT * FROM tbl_service_quality_checking_staff_position AS id";
                    $query_position = $conn->query($query);
                ?>
                <?php echo ($query_position) && $query_position->num_rows>0?'<p class="done"><i class="fa fa-users" aria-hidden="true"></i> ចំនួនតួនាទីបុគ្គលិកសរុបគឺ៖ <b>'.$query_position->num_rows."</b> តួនាទី</p>":'';?>
                <table class="highlight bordered admin-table responsive-table table-setting">
                    <thead>
                        <tr>
                            <th>តួនាទីបុគ្គលិក(ខ្មែរ)</th>
                            <th>តួនាទីបុគ្គលិក(ឡាតាំង)</th>
                            <th>លេខរៀងតម្រៀប</th>
                            <th>ស្ថានភាពតួនាទី</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            while($row = mysqli_fetch_object($query_position)) {
                                $status = $row->status?"<span class='center wrapper done'><i class='fa fa-check-circle done'></i> Active</span>":'';
                                echo "<tr>
                                        <td class='center'>$row->staff_position_kh</td>
                                        <td>$row->staff_position_en</td>
                                        <td class='center'>$row->order</td>
                                        <td class='center'>$status</td>
                                     </tr>";
                            }
                        ?>
                    </tbody>
                </table>
                 <!-- Modal Trigger -->
                 <div class="modal fade modal-fixed-footer modal-service-checking" id="service_checking_position">
                    <form method="POST" action="#" class="service-checking-position service_checking_new_staff" >
                        <div class="modal-content">
                            <blockquote>បញ្ចូលតួនាទីថ្មី</blockquote>
                            <p class="center">សូមលោកគ្រូ អ្នកគ្រូមេតា្តបំពេញសូចនាករថ្មីទៅតាមព័ត៌មានដូចខាងក្រោម</p><br />
                            <div class="row">
                                <div class="input-field col s6 offset-s3">
                                    <label for="title_kh" class="active">ឈ្មោះសូចនាករជាខ្មែរ<span class="required">(*)</span></label>
                                    <input id="title_kh" name="title_kh" type="text" class="validate" value="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6 offset-s3">
                                    <label for="title_en" class="active">ឈ្មោះសូចនាករជាឡាតាំង<span class="required">(*)</span></label>
                                    <input id="title_en" name="title_en" type="text" class="validate" value="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6 offset-s3">
                                    <label for="order_number" class="active">លេខរៀងតម្រៀប</label>
                                    <input id="order_number" name="order_number" type="number" class="validate" min="1" value="">
                                </div>
                                <div class="input-fiels col s6 offset-s3">
                                    <label for="indicator_note" class="active">កំណត់សម្គាល់(Optional)</label>
                                    <textarea name="indicator_note" id="indicator_note" cols="35" row="100" class="materialize-textarea validate" data-length="500" maxlength="500"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn waves-effect waves-light btn_submit_update" data-action="update" form="service_checking"  type="submit" name="btn_submit_update"><i class="fa fa-floppy-o"></i> រក្សាទុក</button>
                            <button type="reset" class="modal-action btn orange darken-1"><i class="fa fa-minus"></i> សម្អាត</button>
                            <a href="#!" class="modal-action modal-close btn deep-orange darken-2"><i class="fa fa-times"></i> ចាកចេញ</a>
                        </div>
                    </form>
                </div>
            </div>
            <div id="indicator_setting" class="col s12">
                <!-- <a href="#!" class="modal-trigger btn btn-report right"  data-target="service_checking_indicator"><i class="fa fa-puzzle-piece" aria-hidden="true"></i> បញ្ចូលសូចនាករថ្មី</a> -->
                <?php 
                $query = "SELECT * FROM tbl_service_quality_checking_staff_key_indicator AS id WHERE id.status=1 ORDER BY id.order ASC";
                $query_indicator = $conn->query($query);

                echo ($query_indicator) && $query_indicator->num_rows>0?'<p class="done"><i class="fa fa-users" aria-hidden="true"></i> ចំនួនសូចនាករសរុបគឺ៖ <b>'.$query_indicator->num_rows."</b></p>":'';?>
                
                <table class="highlight bordered admin-table responsive-table table-setting">
                    <thead>
                        <tr>
                            <th>ឈ្មោះសូចនាករ(ខ្មែរ)</th>
                            <th>ឈ្មោះសូចនាករ(ឡាតាំង)</th>
                            <th>កំណត់សម្គាល់</th>
                            <th>លេខរៀងតម្រៀប</th>
                            <th>ស្ថានភាពសូចនាករ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $query = "SELECT * FROM tbl_service_quality_checking_staff_key_indicator AS id WHERE id.status=1 ORDER BY id.order ASC";
                            $query_indicator = $conn->query($query);
                            while($row = mysqli_fetch_object($query_indicator)) {
                                $status = $row->status?"<span class='center wrapper done'><i class='fa fa-check-circle done'></i> Active</span>":'';
                                echo "<tr>
                                        <td class='center'>$row->title_kh</td>
                                        <td>$row->title_en</td>
                                        <td class='tooltipped' data-position='top' data-tooltip='$row->note'><span class='one-row-text'>$row->note</span></td>
                                        <td class='center'>$row->order</td>
                                        <td class='center'>$status</td>
                                     </tr>";
                            }
                        ?>
                    </tbody>
                </table>
                <!-- Modal Trigger -->
                <div class="modal fade modal-fixed-footer modal-service-checking" id="service_checking_indicator">
                    <form method="POST" action="#" class="service-checking-indicator service_checking_new_staff" >
                        <div class="modal-content">
                            <blockquote>បញ្ចូលសូចនាករថ្មី</blockquote>
                            <p class="center">សូមលោកគ្រូ អ្នកគ្រូមេតា្តបំពេញសូចនាករថ្មីទៅតាមព័ត៌មានដូចខាងក្រោម</p><br />
                            <div class="row">
                                <div class="input-field col s6 offset-s3">
                                    <label for="title_kh" class="active">ឈ្មោះសូចនាករជាខ្មែរ<span class="required">(*)</span></label>
                                    <input id="title_kh" name="title_kh" type="text" class="validate" value="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6 offset-s3">
                                    <label for="title_en" class="active">ឈ្មោះសូចនាករជាឡាតាំង<span class="required">(*)</span></label>
                                    <input id="title_en" name="title_en" type="text" class="validate" value="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6 offset-s3">
                                    <label for="order_number" class="active">លេខរៀងតម្រៀប</label>
                                    <input id="order_number" name="order_number" type="number" class="validate" min="1" value="">
                                </div>
                                <div class="input-fiels col s6 offset-s3">
                                    <label for="indicator_note" class="active">កំណត់សម្គាល់(Optional)</label>
                                    <textarea name="indicator_note" id="indicator_note" cols="35" row="100" class="materialize-textarea validate" data-length="500" maxlength="500"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn waves-effect waves-light btn_submit_update" data-action="update" form="service_checking"  type="submit" name="btn_submit_update"><i class="fa fa-floppy-o"></i> រក្សាទុក</button>
                            <button type="reset" class="modal-action btn orange darken-1"><i class="fa fa-minus"></i> សម្អាត</button>
                            <a href="#!" class="modal-action modal-close btn deep-orange darken-2"><i class="fa fa-times"></i> ចាកចេញ</a>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <style>
        .row-report{
            background:#ffffff6b;
            margin-top:2rem;
            padding:15px;
        }
        .row-space{
            margin:10px 0;
        }
        .row-report .row-space a.green-text:hover,
        .row-report .row-space a.green-text:hover ~ .row-report .green-text.row-space{
            color:#ee6e73 !important;
        }
        table.table-setting tbody tr td{
          padding:5px;
        }
        table.table-setting tbody tr td:hover{
          text-decoration: underline solid;
          cursor:pointer;
        }
        textarea#service_checking_note{
          min-height:80px;
        }
        .pending, .pending:after, .result-status p span.pending, .result-status p span.pending:after{
          color:#f0ad4e;
        }
        .fail, .fail:after, .result-status p span.fail, .result-status p span.fail:after{
          color:#ec0808a6;
        }
        #qc_setting,#position_setting{
            padding:15px;
        }
        .modal-trigger.btn{
            margin:10px 0;
            padding:0 5px;
        }
        i.fa.fa-2x{
            font-size:1.5rem;
        }
        table.table-setting tbody tr td:hover{
            text-decoration:none;
            cursor:default;
        }
        form.service-checking-indicator textarea{
            border:1px solid #9e9e9e;
            height:100px !important;
        }
        label [type=checkbox]+span:not(.lever){
            padding-left: 2rem;
            padding-right:10px;
        }
        [type=checkbox]+span:not(.lever):before, [type=checkbox]:not(.filled-in)+span:not(.lever):after{
            border: 1px solid #4db848;
        }
        div.done.messegn-import{
            padding:5px 10px;
            margin-top:15px;
            background:#cae2c4;
        }
        </style>