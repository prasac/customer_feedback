<?php
  include('../admin/session.php');
  if(!isset($_SESSION['login_user'])){
    header("location:http://feedback.prasac.local");
  }else{
    include('../admin/header.php');
    $header_text = 'ប្រព័ន្ធគ្រប់គ្រង់សម្ភារៈផ្សព្វផ្សាយ';
    include('../admin/head.php');
    $date = new DateTime();
    $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
    $today_time = $date->format("Y-m-d H:i A");
    $updated_date = $date->format("Y-m-H h:i");
    $branch_id  = isset($_SESSION['login_branch_id'])?$_SESSION['login_branch_id']:0;
    ?>
    <div class="row">
        <nav class="nav-main">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="/" class="breadcrumb">ទំព័រដើម</a>
                    <a href="#" class="breadcrumb">ព័ត៌មានគណនី</a>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="col s12 m12 l12">
            <blockquote>
                <h1>ព័ត៌មានគណនី</h1>
            </blockquote>
            <div class="row">
                <div class="col s2 m2 l2"></div>
                <div class="col s12 m8 l8">
                    <?php 
                        if(isset($_POST['btn-save-data'])){
                            $reporter_name      = isset($_POST['reporter_name'])?$_POST['reporter_name']:'';
                            $reporter_position  = isset($_POST['reporter_position'])?$_POST['reporter_position']:'';
                            $query = "UPDATE branches SET reporter='$reporter_name', reporter_position='$reporter_position', modified_date='$updated_date' where id=$branch_id LIMIT 1";
                            $result = $conn->query($query);
                            echo '
                            <script type="text/javascript" src="http://feedback.local/library/jquery-2.2.min.js"></script>
                            <script src="http://feedback.local/library/materialize-v1.0.0/js/materialize.min.js"></script>
                            <script>
                                    $(document).ready(function(){
                                        var spinner = jQuery("#loader");
                                        spinner.show();';
                            if($result){
                                echo "spinner.hide();
                                    showToast(' ព័ត៌មានគណនីរបស់លោកអ្នកកែប្រែបានយ៉ាងជោគជ័យ',3000);";
                                        
                            }
                            echo '});
                            </script>';
                        }
                    ?>
                    <?php 
                        $loggedin_user_fc = $_SESSION['login_user_fc'];
                        $rid    =   isset($_SESSION['regional_id'])?$_SESSION['regional_id']:0;
                        $querys = "select b.modified_date as modified_date,b.fc as fc,b.name_kh as branch_name, r.name as regional_name,b.reporter as reporter,b.reporter_position as reporter_position
                        from branches as b inner join tbl_regional as r on r.id=b.regional_id where b.id=$branch_id limit 1";
                        $query_branch_regional = $conn->query($querys);
                        $query_branch_regional_obj =  mysqli_fetch_object($query_branch_regional);
                    ?>
                    <form actioni="" method="post">
                        <div class="form-group row">
                            <label for="reporter_position" class="col-form-label">លេខ FC</label>
                            <div class="col-sm-10"><b><?= $query_branch_regional_obj->fc;?></b></div>
                        </div>
                        <div class="form-group row">
                            <label for="reporter_position" class="col-form-label">ឈ្មោះសាខា</label>
                            <div class="col-sm-10"><b><?= $query_branch_regional_obj->branch_name;?></b></div>
                        </div>
                        <div class="form-group row">
                            <label for="reporter_position" class="col-form-label">ឈ្មោះ​ភូមិភាគ</label>
                            <div class="col-sm-10"><b><?= $query_branch_regional_obj->regional_name;?></b></div>
                        </div>
                        <div class="form-group row">
                            <label for="reporter_name" class="col-form-label">ឈ្មោះអ្នកផ្ដល់របាយការណ៍</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control-plaintext" id="reporter_name" name="reporter_name" placeholder="ឈ្មោះអ្នកផ្ដល់របាយការណ៍" value="<?= $query_branch_regional_obj->reporter;?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="reporter_position" class="col-form-label">តួនាទីអ្នក​ផ្ដល់របាយការណ៍</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control-plaintext" id="reporter_position" name="reporter_position"  placeholder="តួនាទីអ្នក​ផ្ដល់របាយការណ៍" value="<?= $query_branch_regional_obj->reporter_position;?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="last_updated" class="col-form-label">ថ្ងៃកែប្រែចុងក្រោយ</label>
                            <div class="col-sm-10">
                                <input type="text" readonly class="form-control-plaintext" id="last_updated" name="last_updated" value="<?= $query_branch_regional_obj->modified_date;?>" placeholder="តួនាទីអ្នក​ផ្ដល់របាយការណ៍">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-6 mx-auto">
                                <button type="submit" class="btn-success waves-effect waves-light btn btn-large" name="btn-save-data">រក្សាទុក</button>
                                <a href="/" class="btn-warning waves-effect waves-light btn btn-large">មិនរក្សាទុក</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  <?php
  }
  include('../admin/footer.php');
  ?>
  