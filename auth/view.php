
<?php 
    $issue_id = isset($_GET['item'])?$_GET['item']:0;
    if(isset($_SESSION['login_id']) && $_SESSION['login_id'] ==1){ // Only admin which enable to update status to is read when click on view
        if($issue_id){
            $result_isread  =   $conn->query("UPDATE tbl_issue_detail SET is_read=1 WHERE id=$issue_id LIMIT 1");
        }
    }

    $query = "SELECT 
        id.id as feedback_id,
        cd.id as client_id,
        id.fc as branch_fc,
        cd.client_name as client_name,
        cd.client_gender as gender,
        cd.client_phone as phone,
        cd.client_category as category,
        cd.client_type as clienttype,
        id.id as issue_id,
        id.staff_id as staff_id,
        id.staff_position as staff_position_id,
        id.detail_submited as issue,
        id.issue_date as issuedate,
        id.is_read as is_read,
        id.detail_responsed as solution,
        id.area as area,
        id.status as issue_status,
        fs.title as status_en,
        fs.title_kh as status_kh,
        us.display_name as agent_name,
        ch.title_kh as channel,
        br.name_kh as branch_name,
        re.region as regional_code,
        re.name as regional_name,
        id.modified_date as modified_date,
        id.is_deleted as is_deleted,
        icat.title_kh as issue_category,
        clt.title_kh as client_type,
        area.title_kh as client_area,
        (select title_kh from tbl_financial_services where id=id.service_action) as service_action
    FROM tbl_issue_detail as id
        INNER JOIN tbl_client_detail as cd ON cd.id=id.client_id
        INNER JOIN tbl_area as area ON area.id=id.area
        INNER JOIN tbl_issue_category as icat ON icat.id=cd.client_category
        INNER JOIN tbl_user as us on id.user_id=us.id
        INNER JOIN tbl_channel as ch on ch.id=id.client_channel
        INNER JOIN branches as br on br.fc=id.fc
        INNER JOIN tbl_regional as re on re.id=br.regional_id
        INNER JOIN tbl_feedback_status as fs ON fs.id=id.status
        LEFT JOIN tbl_financial_services as clt ON clt.id=cd.client_type
        WHERE id.id=$issue_id LIMIT 1";
    $result = $conn->query($query);
    if(($result) && $result->num_rows>0){
        $row = mysqli_fetch_object($result);
        $gender_array = array(1=>'ប្រុស',2=>'ស្រី');
        $gender_array_en = array(1=>'Male',2=>'Female');
        $others = 'ផ្សេងៗ';
        ?>
        <div class="col s12">
            <div class="row">
                <blockquote>មើលរបាយការណ៍</blockquote>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                    <b> ឈ្មោះអតិថិជន៖  </b>  <?php echo $row->client_name;?>
                    </div>
                </div>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                    <b>  ភេទ ៖   </b> <?php echo $gender_array[$row->gender];?>
                    </div>
                </div>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                    <b> លេខទូរស័ព្ទ ៖   </b> <?php echo $row->phone;?>
                    </div>
                </div>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                    <b> ប្រភេទព័ត៌មាន ៖  </b>  <?php echo $row->issue_category;?>
                    </div>
                </div>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                    <b> ប្រភេទសេវាហិរញ្ញវត្ថុ ៖   </b> <?php echo $row->client_type?$row->client_type:$others;?>
                    </div>
                </div>
                <?php if($_SESSION['login_id']==2){?>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                    <b> បង្ខាំង/ដំណើរការ ៖   </b> <?php echo $row->service_action;?>
                    </div>
                </div>
                <?php }?>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                    <b> ប្រភេទ ៖   </b> <?php echo $row->client_area;?>
                    </div>
                </div>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                        <b>បញ្ហា ៖ </b><br /> <?php echo $row->issue;?>
                    </div>
                </div>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                        <b>ដំណោះស្រាយ ៖ </b> <br /> <?php echo $row->solution;?>
                    </div>
                </div>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                        <b>តាមរយៈ ៖ </b>  <?php echo $row->channel;?>
                    </div>
                </div>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                        <b>អត្តលេខបុគ្គលិក ៖  </b> <?php echo $row->staff_id;?>
                    </div>
                </div>
                <?php 
                    $query = "SELECT * FROM tbl_agent WHERE id=$row->staff_position_id ORDER BY id ASC LIMIT 1";
                    $result_query = $conn->query($query);
                    if(($result_query) && $result_query->num_rows>0){
                    echo ' <div class="view-grid-col">
                                <div class="input-field col s8 offset-s2">';
                                echo '<b>បង្កដោយ  ៖ </b> ';
                                    $rows = mysqli_fetch_object($result_query);
                                    echo $rows->title_kh;
                            echo '</div>
                            </div>';
                    }
                ?>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                        <b>កូដសាខា ៖  </b> <?php echo $row->branch_fc;?>
                    </div>
                </div>

                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                        <b>ឈ្មោះសាខា ៖  </b> <?php echo $row->branch_name;?>
                    </div>
                </div>

                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                        <b>ភូមិភាគ ៖  </b> <?php echo $row->regional_name;?>
                    </div>
                </div>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                        <b>កាលបរិច្ឆេទបញ្ហា/ព័ត៌មាន ៖  </b> <?php echo date("Y-m-d h:i A", strtotime($row->issuedate));?>
                    </div>
                </div>
                 <?php if($row->is_deleted==1){ ?>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                        <b>កាលបរិច្ឆេទលុបរបាយការណ៍ចុងក្រោយ ៖  </b> <?php echo date("Y-m-d h:i A", strtotime($row->modified_date));?>
                    </div>
                </div>
                <?php }?>
                <div class="view-grid-col">
                    <div class="input-field col s8 offset-s2">
                        <?php 
                            $status_icon = 'check_circle';
                            $status_class = 'done';
                            $icon_status  = 'check_circle';
                            switch($row->issue_status){
                                case 1:
                                    $status_class = 'done';
                                    $icon_status  = 'check_circle';
                                break;
                                case 2:
                                    $status_class = 'pending';
                                    $icon_status  = 'info';
                                break;
                                case 3:
                                    $status_class = 'fail';
                                    $icon_status  = 'cancel';
                                break;
                            }
                        ?>
                        <b class="left">លទ្ធផល ៖ </b>
                        <span class="<?php echo $status_class;?> left"><i alt="<?php echo $row->status_en;?>" class="material-icons <?php echo $status_class;?> left" style="margin:0;"><?php echo $icon_status;?></i> <?php echo $row->status_en;?></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s8 offset-s2 text-right">
                    <a  href="#" onclick="window.history.go(-1); return false;" class="link-action tooltipped" data-position="top" data-tooltip="ត្រលប់ក្រោយ" ><i class="fa fa-long-arrow-left"></i> ត្រលប់ក្រោយ</a> | 
                    <a href="index.php?menu=myreport&page=edit&item=<?php echo $issue_id?>" class="link-action tooltipped" data-position="top" data-tooltip="កែប្រែរបាយការណ៍នេះ" ><i class="fa fa-edit"></i> កែប្រែ</a> | 
                    <a style="color:red;" id="delete_report" data-item="<?php echo $issue_id?>" href="#" class="link-action tooltipped" data-position="top" data-tooltip="លុបរបាយការណ៍នេះចេញពីប្រព័ន្ធ"><i class="fa fa-trash"></i> លុបចេញ</a>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
