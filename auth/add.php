<?php 
  //If add new report form is submitted
  $date = new DateTime();
  $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
  if(isset($_POST['btn_submit_branch'])){
    ob_start();
    $client_name = $_POST['client_name'];
    $client_gender = $_POST['client_gender'];
    $client_phone = isset($_POST['client_phone'])?$_POST['client_phone']:0;
    $client_category =$_POST['issue_category'];
    $client_type = $_POST['client_financial_service'];
    $loggedin_branch_id = $_POST['logged_branch_id'];

    $query_client_detail = "INSERT INTO `tbl_client_detail` (`client_name`,`client_gender`,`client_phone`,`client_category`,`client_type`) VALUES('$client_name','$client_gender','$client_phone','$client_category','$client_type')";
   
    $result_client_query  = $conn->query($query_client_detail);
    
    if($result_client_query){
      $user_id  = isset($_SESSION['user_id'])?$_SESSION['user_id']:'';
      $current_datetime = $date->format("Y-m-d H:i:s");
      $issue_date    =  $_POST['issue_date'].' '.$date->format("H:i:s");
      $fcsite = $loggedin_branch_id;
      $client_area = $_POST['feedback_category'];
      $response_channel = $_POST['response_channel'];
      $staff_id = $_POST['staff_id'];
      $related_position = isset($_POST['related_position']) && $_POST['related_position'] !=''?$_POST['related_position']:0;
      $issue_detail = mysqli_real_escape_string($conn,$_POST['issue_detail']);
      $solution = mysqli_real_escape_string($conn,$_POST['solution']);
      $result_status = $_POST['result_status'];
      $result_status = $_POST['result_status'];
      $last_inserted_client_id  = $conn->insert_id;
      $user_type = $_POST['user_type']?$_POST['user_type']:'0';
      $query_issue  = "INSERT INTO `tbl_issue_detail` (`user_id`,`fc`,`area`,`client_id`,`client_channel`,`staff_id`,`staff_position`,`operator_id`,`detail_submited`,`detail_responsed`,`issue_date`,`status`,`created_date`,`modified_date`) 
                       VALUES('$user_id','$fcsite','$client_area','$last_inserted_client_id','$response_channel','$staff_id','$related_position','$user_type','$issue_detail','$solution','$issue_date','$result_status','$current_datetime','$current_datetime')";
      // echo $query_issue;exit;
      $result_issue_detail  = $conn->query($query_issue);

      echo '
          <!--Import jQuery before materialize.js-->
          <script type="text/javascript" src="http://feedback.local/library/jquery.min.js"></script>
          <!--Import Materialize-Stepper JavaScript -->
          <script src="http://feedback.local/library/materialize-v1.0.0/js/materialize.min.js"></script>
          <script src="http://feedback.local/library/materialize-datetimepicker-master/js/materializedatetimepicker.js"></script>
          <script type="text/javascript" src="http://feedback.local/library/jquery-chained.min.js"></script>
          <script type="text/javascript" src="http://feedback.local/library/jquery-validation/dist/jquery.validate.min.js"></script>';

      if($result_issue_detail){
        echo '
          <script>
                $(document).ready(function(e){
                  showToast("<i class=material-icons style=color:#4db848;>check_circle</i>&nbsp;<span style=color:#4db848;padding:5px 10px;>របាយការណ៍ ត្រូវបាន​រក្សាទុក​ដោយជោគជ័យ</span>",3000);
                  setTimeout(function() {
                    window.location.href = "?menu=myreport";
                  }, 1500);
                });
          </script>';
      }else{
          echo '
          <script>
                $(document).ready(function(e){
                  showToast("<i class=material-icons fail>cancel</i>&nbsp;<span style=color:red;padding:5px 10px;>របាយការណ៍ មិនត្រូវបានរក្សាទុកនោះទេ</span>",3000);
                });
          </script>';
      }
      ob_end_flush();
    }
  }
  $date = new DateTime();
  $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
  //If add new report form is submitted
  if(isset($_POST['btn_submit_social']) || isset($_POST['btn_submit_callcenter'])){
    ob_start();
    $client_name = $_POST['client_name'];
    $client_gender = $_POST['client_gender'];
    $client_phone = $_POST['client_phone'];
    $client_category = $_POST['issue_category'];
    $client_type = $_POST['client_financial_service'];
    $sub_financial_service = isset($_POST['sub_financial_service']) && $_POST['sub_financial_service'] !=''?$_POST['sub_financial_service']:0;
    $query_cd = "INSERT INTO `tbl_client_detail` (`client_name`,`client_gender`,`client_phone`,`client_category`,`client_type`) VALUES('$client_name','$client_gender','$client_phone','$client_category','$client_type')";
    $result_client_query  = $conn->query($query_cd);
    if($result_client_query){
      $user_id  = isset($_SESSION['user_id'])?$_SESSION['user_id']:'';
      $posted_datetime = $_POST['issue_date'].' '.date("H:i:s",strtotime($_POST['issue_time']));
      $current_datetime = $date->format("Y-m-d H:i:s");
      $fcsite =  $_POST['fcsite'];
      $client_area = $_POST['feedback_category'];
      $response_channel = $_POST['response_channel'];
      $staff_id = $_POST['staff_id'];
      $related_position = isset($_POST['related_position']) && $_POST['related_position'] !=''?$_POST['related_position']:0;
      $issue_detail = mysqli_real_escape_string($conn,$_POST['issue_detail']);
      $solution = mysqli_real_escape_string($conn,$_POST['solution']);
      $result_status = $_POST['result_status'];
      $result_status = $_POST['result_status'];
      $last_inserted_client_id  = $conn->insert_id;
      $user_type = $_POST['user_type']?$_POST['user_type']:'0';
      $query_issue  = "INSERT INTO `tbl_issue_detail` (`user_id`,`fc`,`area`,`client_id`,`client_channel`,`service_action`,`staff_id`,`staff_position`,`operator_id`,`detail_submited`,`detail_responsed`,`issue_date`,`status`,`created_date`,`modified_date`) 
                       VALUES('$user_id','$fcsite','$client_area','$last_inserted_client_id','$response_channel','$sub_financial_service','$staff_id','$related_position','$user_type','$issue_detail','$solution','$posted_datetime','$result_status','$current_datetime','$current_datetime')";
      $result_issue_detail  = $conn->query($query_issue);
      
      echo '
          <!--Import jQuery before materialize.js-->
          <script type="text/javascript" src="http://feedback.local/library/jquery.min.js"></script>
          <!--Import Materialize-Stepper JavaScript -->
          <script src="http://feedback.local/library/materialize-v1.0.0/js/materialize.min.js"></script>
          <script src="http://feedback.local/library/materialize-datetimepicker-master/js/materializedatetimepicker.js"></script>
          <script type="text/javascript" src="http://feedback.local/library/jquery-chained.min.js"></script>
          <script type="text/javascript" src="http://feedback.local/library/jquery-validation/dist/jquery.validate.min.js"></script>';

      if($result_issue_detail){
        echo '
          <script>
                $(document).ready(function(e){
                  showToast("<i class=material-icons style=color:#4db848;>check_circle</i>&nbsp;<span style=color:#4db848;padding:5px 10px;>របាយការណ៍ ត្រូវបាន​រក្សាទុក​ដោយជោគជ័យ</span>",3000);
                  setTimeout(function() {
                    window.location.href = "?menu=myreport";
                  }, 1500);
                });
          </script>';
      }else{
          echo '
          <script>
                $(document).ready(function(e){
                  showToast("<i class=material-icons fail>cancel</i>&nbsp;<span style=color:red;padding:5px 10px;>របាយការណ៍ មិនត្រូវបានរក្សាទុកនោះទេ</span>",3000);
                });
          </script>';
      }
      ob_end_flush();
    }
  }


  


  ?>
<!-- Modal Structure -->
<div class="col s12">
    <div class="row">
        <blockquote>បញ្ចូលរបាយការណ៍ថ្មី</blockquote>
        <?php 
            $login_user_type = $_SESSION['login_id']?$_SESSION['login_id']:'';
            switch($login_user_type){
              case 1:
                  $add_form = 'add_form_callcenter.php';
                  break;
              case 2:
                  $add_form = 'add_form_callcenter.php';
                  break;
              case 3:
                  $add_form = 'add_form_social.php';
                  break;
              case 4:
                  $add_form = 'add_form_branch.php';
                  break;
              case 5:
                  $add_form = 'add_form_callcenter.php';
                  break;
              default:
                  $add_form = 'add_form_branch.php';
                  break;
          }
          include($add_form);
        ?>
    </div>
</div>