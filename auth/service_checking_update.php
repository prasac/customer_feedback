<?php 
    include('../admin/session.php');
    $date_access_search = ' AND DATE(created_date)=CURDATE() ';
    try {
        $action_type = isset($_POST['action_type'])?$_POST['action_type']:'';
        if($action_type == "update"){
            $updated_reported_by = isset($_POST['service_checking_reported_staff_id'])?$_POST['service_checking_reported_staff_id']:'';
            $updated_report_staff_id = isset($_POST['service_checking_staff_id'])?$_POST['service_checking_staff_id']:'';
            $indicator_fields = array();
            $full_fields_update = array();
            $indicator_query = $conn->query("SELECT * FROM tbl_service_quality_checking_staff_key_indicator AS indicator ORDER BY indicator.order DESC");

            while($row_indicator = mysqli_fetch_object($indicator_query)){
                array_push($indicator_fields,"key_indicator$row_indicator->id");
                $fields_name = "service_checking_".strtolower(trim($row_indicator->title_en));
                $update_fields = '`key_indicator'.$row_indicator->id.'`='."'".$_POST[$fields_name]."'";
                array_push($full_fields_update,$update_fields);
            }
            $full_fields_update = implode(",",$full_fields_update);

            $query_udpate = "UPDATE `tbl_service_quality_checking` SET $full_fields_update, `reported_by`='$updated_reported_by' WHERE `staff_id`=$updated_report_staff_id  $date_access_search LIMIT 1";
            $conn->query($query_udpate);
            echo $query_udpate;
        }

        if($action_type == "delete"){
            $staff_id = isset($_POST['service_checking_staff_id'])?$_POST['service_checking_staff_id']:'';
            $conn->query("DELETE FROM tbl_service_quality_checking_staff WHERE id=$staff_id LIMIT 1");
             $conn->query("DELETE FROM `tbl_service_quality_checking` WHERE `staff_id`=$staff_id");
            echo 'delete';
        }

        if($action_type == "truncate"){
            $truncat_query = "TRUNCATE TABLE tbl_service_quality_checking";
            $conn->query($truncat_query );
            echo 'truncate';
        }
    }catch (Exception $e) {
        echo 'Caught exception:',$e->getMessage(),"\n";
    }
    exit;
?>