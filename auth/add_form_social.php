        <form method="POST" action="" class="newreport_social">
            <div class="row">
                <div class="input-field col s4 offset-s2">
                    <input id="client_name" type="text" name="client_name" class="validate" placeholder="សុខ ភក្ដី">
                    <label for="client_name">ឈ្មោះអតិថិជន</label>
                </div>
                <div class="input-field col s4">
                    <select name="client_gender">
                        <option value="1" data-icon=""> ប្រុស</option>
                        <option value="2" data-icon=""><i class="fa fa-female" aria-hidden="true"></i> ស្រី</option>
                    </select>
                    <label for="client_gender" class="active">ភេទ</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s4  offset-s2">
                    <input id="client_phone"  name="client_phone" type="tel" class="validate" placeholder="012345678">
                    <label for="client_phone">លេខទូរស័ព្ទ</label>
                </div>
                <div class="input-field col s4">
                    <select name="issue_category" id="issue_category" class="validate">
                         <option value=""></option>
                        <?php 
                            $query = "SELECT * from tbl_issue_category ORDER BY id ASC";
                            $result_query = $conn->query($query);
                            if(($result_query) && $result_query->num_rows>0){
                                while($row = mysqli_fetch_object($result_query)) {
                                    echo '<option value="'.$row->id.'">'.$row->title_kh.'</option>';
                                }
                            }
                        ?>
                    </select>
                    <label for="issue_category" class="active">ប្រភេទមតិ/ព័ត៌មាន</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s4 offset-s2" class="validate">
                    <select name="client_financial_service">
                         <option value=""></option>
                        <?php 
                        $query = "SELECT * from tbl_financial_services WHERE id=parent_id ORDER BY tbl_financial_services.order ASC";
                        $result_query = $conn->query($query);
                        if(($result_query) && $result_query->num_rows>0){
                            while($row = mysqli_fetch_object($result_query)) {
                                echo '<option value="'.$row->id.'">'.$row->title_kh.'</option>';
                            }
                            echo '<option value="0">សេវាផ្សេងៗ</option>';
                        }
                        ?>
                    </select>
                    <label for="client_financial_service" class="active">ប្រភេទសេវាហិរញ្ញវត្ថុ</label>
                </div>
                <div class="input-field col s4">
                    <select name="feedback_category" class="validate ">
                         <option value=""></option>
                        <?php 
                        $query = "SELECT * from tbl_area ORDER BY id ASC";
                        $result_query = $conn->query($query);
                        if(($result_query) && $result_query->num_rows>0){
                            while($row = mysqli_fetch_object($result_query)) {
                                echo '<option value="'.$row->id.'">'.$row->title_kh.'</option>';
                            }
                        }
                        ?>
                    </select>
                    <a href="#notice_category" class="notice-category modal-trigger" style="color:red;padding:10px 0;position:absolute;right:0;font-size:12px;top:30px;"><i class="fa fa-info-circle"></i> ឧទាហរណ៍</a>
                    <label for="feedback_category" class="active">ប្រភេទ</label>
                </div>
                <div id="notice_category" class="modal">
                    <div class="modal-content">
                        <h4 style="color:#ee6e73;text-align:center;">ឧទាហរណ៍</h4>
                        <ul class="list">
                            <li class="item-list">
                                <strong>ផលិតផល និងសេវា៖ </strong> 
                                <p>លក្ខខណ្ឌបើក/ស្នើសុំ អត្រាការប្រាក់និងការទូទាត់ កម្រៃសេវា របៀបប្រើប្រាស់ លក្ខខណ្ឌ/របៀបសង លក្ខណៈផលិតផល និងសេវា...</p>
                            </li>
                            <li class="item-list">
                                <strong>ល្បឿនផ្ដល់សេវា៖ </strong> 
                                <p>រយៈពេលធ្វើប្រតិបត្តិការ រយៈពេលដោះស្រាយបញ្ហា/ផ្ដល់ព័ត៌មាន រយៈពេលដែលអតិថិជនរង់ចាំ...</p>
                            </li>
                            <li class="item-list">
                                <strong>យកចិត្តទុកដាក់៖ </strong>
                                <p>សួរពីគោលបំណងអតិថិជន សម្របសម្រួលអតិថិជនរង់ចាំ ផ្ដល់ដំណោះស្រាយជូនអតិថិជន ឆ្លើយតបនឹងអតិថិជន ការប្រាស្រ័យទាក់ទងនឹងអតិថិជន ការសន្យាណាត់ជួប សម្រួលចរាចរជូនអតិថិជន ចាក់សោម៉ូតូ បាំងឆត្រ បើកទ្វារ លើកទឹក/ភេសជ្ជៈ ...</p>
                            </li>
                            <li class="item-list">
                                <strong>ឥរិយាបថ៖</strong>
                                <p>ការប្រើប្រាស់ពាក្យសម្តី កាយវិការ ទឹកមុខ អត្តចរិត...</p>
                            </li>
                            <li class="item-list">
                                <strong>តុបតែងកាយ៖ </strong>
                                <p>សម្លៀកបំពាក់ (មានរបៀបរៀបរយ) ក្លិនខ្លួន ក្លិនមាត់ ម៉ូតសក់/ពណ៌សក់ អនាម័យ/ពណ៌ក្រចក ការផាត់មុខ ពុកមាត់/ចង្ការ តឺនុយ ស្បែកជើងប៊ូ/ឃ្លុបខ្មៅ...</p>
                            </li>
                            <li class="item-list">
                                <strong>ចំណេះដឹង៖</strong>
                                <p>ចំណេះដឹងរបស់បុគ្គលិកពីផលិតផលនិងសេវា លក្ខខណ្ឌនិងនីតិវិធី ការប្រើប្រាស់ប្រព័ន្ធធនាគារស្នូល...</p>
                            </li>
                            <li class="item-list">
                                <strong>ជំនាញ៖ </strong> <p>ការប្រើប្រាស់ភាសា កុំព្យូទ័រ គណនាលេខ/ការប្រាក់...</p>
                            </li>
                            <li class="item-list">
                                <strong>ការិយាល័យ៖</strong> 
                                <p>អនាម័យនិងសណ្តាប់ធ្នាប់ក្នុង/ក្រៅការិយាល័យ ចំណតឡាន/ម៉ូតូ បន្ទប់ទឹក ក្លិន ត្រជាក់ និងផាសុកភាព...</p>
                            </li>
                            <li class="item-list">
                                <strong>សម្ភារៈ៖</strong>
                                <p>តុផ្ដល់ព័ត៌មាន បញ្ជរបេឡា ខិត្តប័ណ្ណផ្សព្វផ្សាយ ពាក្យស្នើសុំប្រើប្រាស់សេវាហិរញ្ញវត្ថុប្រាសាក់ កៅអីអង្គុយរង់ចាំ ម៉ាស៊ីនត្រជាក់ កង្ហារ ធុងទឹកក្តៅ/ត្រជាក់ កែវជ័រសម្រាប់អតិថិជន ម៉ាស៊ីនវាស់កម្ដៅ អាល់កុលលាងសម្អាតដៃ សម្ភារៈបន្ទប់ទឹក ម៉ាស៊ីនរាប់សាច់ប្រាក់ សោចាក់ម៉ូតូអតិថិជន ឧបករណ៍សម្រួលចរាចរ ឆត្រប្រាសាក់...</p>
                            </li>
                            <li class="item-list">
                                <strong>ផ្សេងៗ៖</strong> 
                                <p>រាល់ចំណុចដែលខុសពីចំណុចខាងលើ (ឧ. សួរពីការជ្រើសរើសបុគ្គលិក ...)</p>
                            </li>
                        </ul>
                    </div>
                    <div id="seperator"></div>
                    <div class="modal-footer">
                        <a href="#!" class="modal-close waves-effect waves-green btn-flat" style="color:#ee6e73;"><i class="fa fa-close"></i> ចាកចេញ</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s4 offset-s2" >
                    <textarea name="issue_detail" id="issue_detail" cols="35" row="30" class="materialize-textarea" data-length="900" maxlength="900" placeholder="បញ្ហា"></textarea>
                    <label for="issue_detail">បញ្ហា/ព័ត៌មាន</label>
                </div>
                <div class="input-field col s4">
                    <textarea name="solution" id="solution" cols="35" row="30" class="materialize-textarea" data-length="900" maxlength="900" placeholder="ដំណោះស្រាយ"></textarea>
                    <label for="solution">ការឆ្លើយតប/ដំណោះស្រាយ</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col col s4  offset-s2 result-status">
                    <select name="result_status">
                        <option value=""></option>
                        <option value="1" >Done</option>
                        <option value="2">Pending</option>
                        <option value="3">Fail</option>
                    </select>
                    <label for="result_status" class="active">លទ្ធផល</label>
                </div>
                <div class="input-field col s4">
                    <select name="related_position">
                        <option value=""></option> 
                        <?php 
                        $query = "SELECT * from tbl_agent ORDER BY tbl_agent.order ASC";
                        $result_query = $conn->query($query);
                        if(($result_query) && $result_query->num_rows>0){
                            while($row = mysqli_fetch_object($result_query)) {
                                echo '<option value="'.$row->id.'">'.$row->title_kh.'</option>';
                            }
                        }
                        ?>
                    </select>
                    <label for="related_position" class="active">បង្កដោយ</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s2 offset-s2">
                    <?php 
                        $date = new DateTime();
                        $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
                        $current_date_time = $date->format("d-m-Y");
                    ?>
                    <input type="text" class="issue_date" id="issue_date" name="issue_date" value="" readonly>
                    <label for="issue_date">កាលបរិច្ឆេទបញ្ហា/ព័ត៌មាន</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" class="issue_time" id="issue_time" name="issue_time" value="" readonly>
                    <label for="issue_time" class="active">ម៉ោង</label>
                </div>
                <div class="input-field col s4">
                    <select name="response_channel">
                        <option value=""></option>
                        <?php 
                        $user_type = isset($_SESSION['login_id'])?$_SESSION['login_id']:'';
                        $query = "SELECT * FROM tbl_channel WHERE id IN(2,3,5) ORDER BY id ASC";
                        $result_query = $conn->query($query);
                        if(($result_query) && $result_query->num_rows>0){
                            while($row = mysqli_fetch_object($result_query)) {
                                echo '<option value="'.$row->id.'">'.$row->title_kh.'</option>';
                            }
                        }
                        ?>
                    </select>
                    <label for="response_channel" class="active">តាមរយៈ</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s2 offset-s2">
                    <input id="fcsite" name="fcsite" type="text" class="validate" placeholder="ឧទាហរណ៍៖ 423" value="<?php echo isset($_SESSION['login_user_fc']) && $_SESSION['login_id']==1?$_SESSION['login_user_fc']:'';?>">
                    <label for="fcsite">កូដសាខា</label>
                </div>
                <div class="input-field col s2">
                    <input id="branch_name" name="branch_name" type="text" class="validate" readonly>
                    <label for="branch_name">សាខា</label>
                </div>
                <div class="input-field col s2">
                    <input id="regional_name" name="regional_name" type="text" class="validate" readonly>
                    <label for="regional_name">ភូមិភាគ</label>
                </div>
                <div class="input-field col s2">
                    <input id="staff_id" name="staff_id" type="text" class="validate" value="<?php echo isset($_SESSION['login_user_fc'])?$_SESSION['login_user_fc']:'';?>" readonly>
                    <label for="staff_id">អត្តលេខបុគ្គលិក</label>
                </div>
            </div>
           
            <div class="row">
                <div class="input-field col s7 offset-s5">
                     <input type="hidden" name="user_type" value="<?php echo $_SESSION['login_id'];?>" />
                    <input type="hidden" value="<?php echo isset($_SESSION['user_id'])?$_SESSION['user_id']:0;?>" name="user_id"/>
                    <button class="btn waves-effect waves-light" type="submit" name="btn_submit_social">
                        <i class="fa fa-floppy-o"></i> រក្សាទុករបាយការណ៍
                    </button>
                </div>
            </div>
        </div>