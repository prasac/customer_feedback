
    <?php
    $gender_array = array(1=>'ប្រុស',2=>'ស្រី');
    $gender_array_en = array(1=>'Male',2=>'Female');
    ?>
    <div class="col s12">
        <div class="row">
            <blockquote>គណនីរបស់ខ្ញុំ</blockquote>
            <div class="view-grid-col">
                <div class="input-field col s8 offset-s2">
                    <b> ឈ្មោះ និងលេខសម្ងាត់ប្រើប្រាស់ ៖</b>  <?php echo $_SESSION['login_user_fc'];?>
                </div>
                <div class="input-field col s8 offset-s2">
                    <b> លេខ FC ៖</b>  <?php echo $_SESSION['login_user_fc'];?>
                </div>
                <div class="input-field col s8 offset-s2">
                    <b> ច្ងៃចូលប្រើប្រាស់ចុងក្រោយ ៖</b>  <?php echo date("d-M-Y h:i A",strtotime($_SESSION['last_login_date']));?>
                </div>
                <div class="input-field col s8 offset-s2">
                    <?php
                        $user_right = array(1=>'អ្នកគ្រប់គ្រង',2=>'ការិយាល័យទំនាក់ទំនងអតិថិជន',3=>'បណ្ដាញផ្សព្វផ្សាយសង្គម',4=>'សាខា',5=>'មន្រ្តីផ្ដល់ជំនួយព័ត៌មានវិទ្យា');
                    ?>
                    <b> សិទ្ធិប្រើប្រាស់ ៖</b>  <?php echo $user_right [$_SESSION['login_id']];?>
                </div>
            </div>
        </div>
    </div>