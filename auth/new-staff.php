<?php 
    include('../admin/session.php');
    $date = new DateTime();
    $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
    $current_date_time = $date->format("Y-m-d h:i:s");

    $new_staff_id =  $_POST['new_staff_id'];
    $new_staff_name_kh =  $_POST['new_staff_name_kh'];
    $new_staff_name_en =  $_POST['new_staff_name_en'];
    $new_staff_position =  $_POST['new_staff_position'];
    $regional_id        = $_POST['regional_id'];
    $branch_id =  $_POST['branch_id'];

    $query = "INSERT INTO `tbl_service_quality_checking_staff` 
            (`staff_id`,`stafF_fullname_kh`,`staff_fullname_en`,`staff_position`,`regional_id`,`branch_id`,`status`,`created_date`,`modified_date`) 
            VALUES('$new_staff_id','$new_staff_name_kh','$new_staff_name_en','$new_staff_position','$regional_id','$branch_id',1,'$current_date_time','$current_date_time')";
    $result_query = $conn->query($query);
    echo $query;
    $last_inserted_staff_id  = $conn->insert_id;
    if($result_query){
        $indicator_query = "SELECT indicator.id as id FROM tbl_service_quality_checking_staff_key_indicator AS indicator WHERE indicator.status=1 ORDER BY indicator.order";
        $indicator_run_query = $conn->query($indicator_query);
        while($row_indicator = mysqli_fetch_object($indicator_run_query)) {
            $query_insert_staff = "INSERT INTO `tbl_service_quality_checking`(`staff_id`,`key_indicator`,`created_date`,`modified_date`) 
                                  VALUES('$last_inserted_staff_id',$row_indicator->id,'$current_date_time','$current_date_time')";
            $conn->query($query_insert_staff);
        }

        //Update Note default content
        $update_query = "UPDATE tbl_service_quality_checking SET key_indicator_value = '' WHERE key_indicator=1 AND staff_id IN($last_inserted_staff_id) AND key_indicator_value ='3'";
        $conn->query($update_query);
        // echo 1;
    }else{
        echo 0;
    }
    exit;
?>