
      <blockquote>បញ្ជីព័ត៌មានត្រលប់ពីអតិថិជន</blockquote>
        <?php 
          $date = new DateTime();
          $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
          $today_date = $date->format("Y-m-01");
          $last_date_of_the_month = $date->format("Y-m-t");
        ?>
          <form action="" method="GET" class="filter-form" id="filterform">
            <div class="col s12 right filter-section" style="padding:0;">
              <div class="input-field col s2" style="padding-left:0;">
                  <input type="text" class="validate" id="search_client_name" name="search_client_name" placeholder="សុខ តារា" value="<?php echo isset($_GET['search_client_name'])?$_GET['search_client_name']:'';?>">
                  <label for="search_client_name" class="active">ឈ្មោះអតិថិជន</label>
              </div>
              <div class="input-field col s2">
                  <input type="text" class="search_date_from validate" id="search_date_from" name="search_date_from"  placeholder="2020-06-25" value="<?php echo isset($_GET['search_date_from'])?$_GET['search_date_from']:$today_date;?>">
                  <label for="search_date_from" class="active">ចាប់ពីថ្ងៃ</label>
              </div>
              <div class="input-field col s2">
                  <input type="text" class="search_date_to validate" id="search_date_to" name="search_date_to"  placeholder="2020-06-25" value="<?php echo isset($_GET['search_date_to'])?$_GET['search_date_to']:$last_date_of_the_month;?>">
                  <label for="search_date_to" class="active">ដល់ថ្ងៃ</label>
              </div>
              <div class="input-field col s2">
                <select name="search_result"> 
                    <option value=""></option>
                    <option value="1" <?php echo isset($_GET['search_result']) && $_GET['search_result']==1?' selected':'';?>> Done</option>
                    <option value="2" <?php echo isset($_GET['search_result']) && $_GET['search_result']==2?' selected':'';?>> Pending</option>
                    <option value="3" <?php echo isset($_GET['search_result']) && $_GET['search_result']==3?' selected':'';?>> Fail</option>
                </select>
                <label for="search_result" class="active">លទ្ធផល</label>
              </div>
              <?php if(isset($_SESSION['login_id']) && ($_SESSION['login_id'] == 2 || $_SESSION['login_id'] == 5)){?>
              <div class="input-field col s2">
                  <input type="text" class="validate" id="search_staff_id" name="search_staff_id"  placeholder="09072" value="<?php echo isset($_GET['search_staff_id'])?$_GET['search_staff_id']:'';?>">
                  <label for="search_staff_id" class="active">អត្តលេខបុគ្គលិក</label>
              </div>
              <?php }?>
            </div>
              <?php if(isset($_SESSION['login_id']) && $_SESSION['login_id'] == 1){ ?>
            <div class="col s12 right" style="padding:0;"> 
              <div class="input-field col s2" style="padding-left:0;left:0;">
                   <select name="report_operator"> 
                    <option value=""></option>
                      <?php
                          $user_type = array(2=>'ការិយាល័យទំនាក់ទំនងអតិថិជន',3=>'បណ្តាញសង្គម',4=>' សាខា',5=>'មន្រ្តីផ្ដល់ជំនួយព័ត៌មានវិទ្យានិងសេវាធនាគារចល័ត');
                          foreach($user_type as $key=>$value) {
                            echo '<option';
                            echo isset($_GET['report_operator']) && $_GET['report_operator']==$key?' selected':'';
                            echo ' value="'.$key.'">'.$value.'</option>';
                          }
                        ?>
                  </select>
                  <label for="report_channel" class="active">ប្រតិបត្តិករ</label>
              </div>
               <div class="input-field col s2">
                  <select name="issue_category"> 
                    <option value=""></option>
                      <?php 
                          $query = "SELECT id,title_kh,title from tbl_issue_category ORDER BY id ASC";
                          $result_query = $conn->query($query);
                          if(($result_query) && $result_query->num_rows>0){
                              while($row = mysqli_fetch_object($result_query)) {
                                  echo '<option';
                                  echo isset($_GET['issue_category'])  && $_GET['issue_category']==$row->id?' selected':'';
                                  echo ' value="'.$row->id.'">'.$row->title_kh.'</option>';
                              }
                          }
                        ?>
                  </select>
                  <label for="issue_category" class="active">ប្រភេទមតិ/ព័ត៌មាន</label>
              </div>
              <div class="input-field col s2">
                  <input type="text" class="validate" id="search_branch_fc" name="search_branch_fc"  placeholder="423" value="<?php echo isset($_GET['search_branch_fc'])?$_GET['search_branch_fc']:'';?>">
                  <label for="search_branch_fc" class="active">លេខកូដសាខា</label>
              </div>
               <div class="input-field col s2">
                  <input type="text" class="validate" id="search_staff_id" name="search_staff_id"  placeholder="09072" value="<?php echo isset($_GET['search_staff_id'])?$_GET['search_staff_id']:'';?>">
                  <label for="search_staff_id" class="active">អត្តលេខបុគ្គលិក</label>
              </div>
            </div>
              <?php }?>
              <button type="submit" class="btn waves-effect btn-success waves-left right" name="btn_filter_submit" style="cursor:pointer; padding:0 2rem;border:none;height: 35px;margin: 2px 10px;">ស្វែងរក</button>
      </form>

      <form action="#" method="POST" class="update-form" id="update-form">
        <?php 
        if(isset($_SESSION['login_id']) && $_SESSION['login_id']>0){ // IF logged in with admin user?>
          <div class="status-row">
            <div class="col s10" style="padding:0;">
             <div class="input-field col s3" style="padding:0;">
                <div class="button-container">
                  <span class="left" style="margin-top:8px;">ទាញយក ៖</span>
                  <button type="submit" name="btn-excel" class="btn-export-excel btn tooltipped" data-delay="30" data-tooltip="ទាញ​យក​ទិន្នន័យ​ជា EXCEL"><i style="font-size:2.3rem;" class="fa fa-file-excel-o btn-excel" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </div>
        <?php 
      }
      ?>
      <table class="highlight bordered admin-table responsive-table report-list">
        <thead>
          <tr>
              <th class="centered" style="width:15%;">កាលបរិច្ឆេទបញ្ហា/ព័ត៌មាន</th>
              <th class="centered" style="width:12%;">ឈ្មោះអតិថិជន</th>
              <th class="centered" style="width:10%;">លេខទូរស័ព្ទ</th>
              <th class="centered" style="width:15%;">ប្រភេទសេវាហិរញ្ញវត្ថុ</th>
              <th class="centered" style="width:32%;">បញ្ហា/ព័ត៌មាន</th>
              <th class="centered" style="width:5%;">លទ្ធផល</th>
              <th class="centered" style="width:5%;">កែប្រែ</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $conditions = "WHERE MONTH(id.issue_date) = MONTH(curdate())";
          if(isset($_SESSION['login_id']) && $_SESSION['login_id'] !=1){
            $fc_logged_user = isset($_SESSION['user_id'])?$_SESSION['user_id']:'';
            if($_SESSION['login_id'] == 4 || $_SESSION['login_id'] == 3){
              $conditions = "WHERE id.user_id=$fc_logged_user AND MONTH(id.issue_date) = MONTH(curdate())";
            }else{
              $conditions = "WHERE id.user_id IN(183,184,185,186,187,188,189,190,200,201,202,203,204,205,206) AND MONTH(id.issue_date) = MONTH(curdate())";
            }
          }
          if(isset($_GET['btn_filter_submit'])){
            $conditions = "";
            if(isset($_SESSION['login_id']) && $_SESSION['login_id'] !=1){
              $fc_logged_user = isset($_SESSION['user_id'])?$_SESSION['user_id']:'';
              if($_SESSION['login_id'] == 4 || $_SESSION['login_id'] == 3){
                $conditions = "WHERE id.user_id=$fc_logged_user";
              }else{
                $conditions = "WHERE id.user_id IN(183,184,185,186,187,188,189,190,200,201,202,203,204,205,206)";
              }
            }

            // $conditions = '';
            $client_name = isset($_GET['search_client_name'])?$_GET['search_client_name']:'';
            $search_date_from = isset($_GET['search_date_from'])?$_GET['search_date_from']:$today_date;
            $search_date_to = isset($_GET['search_date_to'])?$_GET['search_date_to']:$today_date;
            $search_result = isset($_GET['search_result'])?$_GET['search_result']:'';
            $search_branch_fc = isset($_GET['search_branch_fc'])?$_GET['search_branch_fc']:'';
            $search_branch_fc_id = explode('_', $search_branch_fc)[0];
            $search_staff_id = isset($_GET['search_staff_id'])?$_GET['search_staff_id']:'';
            $search_operator = isset($_GET['report_operator'])?$_GET['report_operator']:'';
            $issue_category = isset($_GET['issue_category'])?$_GET['issue_category']:'';

            if($client_name !='' || $search_date_from !='' || $search_date_to || $search_result !='' || $search_branch_fc !='' || $search_staff_id !='' ||  $search_operator !='' ||  $issue_category !=''){
              $arr = array();
              if(!empty($client_name)){
                $arr['client_name'] =  " (cd.client_name LIKE '%$client_name%' OR cd.client_phone LIKE '%$client_name%') "; 
              }
              if(!empty($search_date_from) && empty($search_date_to)){
                $arr['issue_date_from'] = " date(id.issue_date) >= '$search_date_from%' ";
              }
              if(!empty($search_date_to) && empty($search_date_from)){
                $arr['issue_date_from'] = " date(id.issue_date) <= '$search_date_to%' ";
              }

              if(!empty($search_date_to) && !empty($search_date_from)){
                $arr['issue_date_from'] = " date(id.issue_date) BETWEEN '$search_date_from%' AND '$search_date_to%' ";
              }

              if(!empty($search_result)){
                $arr['status'] =  " id.status=$search_result "; 
              }

              if(!empty($search_branch_fc)){
                $arr['fc'] = " id.fc = $search_branch_fc_id AND us.user_type=4";
              }

              if(!empty($search_staff_id) && empty($search_operator)){
                $arr['staff_id'] = " id.staff_id = $search_staff_id ";
              }

              if(!empty($search_operator)){
                $arr['operator_id'] = " id.operator_id=$search_operator";
              }

              if(!empty($issue_category)){
                $arr['issue_category'] = " cd.client_category = $issue_category";
              }

              $conditions .= ' AND '.implode('AND', $arr);
            }
          }

          $query_count = "SELECT * FROM tbl_issue_detail as id 
            INNER JOIN tbl_client_detail as cd ON cd.id=id.client_id
            INNER JOIN tbl_user as us on id.user_id=us.id
            INNER JOIN tbl_channel as ch on ch.id=id.client_channel
            INNER JOIN branches as br on br.fc=id.fc
            INNER JOIN tbl_regional as re on re.id=br.regional_id
            INNER JOIN tbl_feedback_status as fs ON fs.id=id.status
            INNER JOIN tbl_issue_category as cat ON cat.id=cd.client_category
            INNER JOIN tbl_area as areas ON areas.id=id.area
            $conditions
            AND id.is_deleted=0
            ORDER BY id.issue_date DESC";
          if($conn->query($query_count))
            $total_records_count = $conn->query($query_count)->num_rows;
          // echo $query_count;
          $query = "SELECT 
                      id.id as feedback_id,
                      cd.id as client_id,
                      cd.client_name as client_name,
                      cd.client_gender as gender,
                      cd.client_phone as phone,
                      cd.client_category as category,
                      cat.title_kh as category_name,
                      id.id as issue_id,
                      id.staff_id as staff_id,
                      id.detail_submited as issue,
                      id.issue_date as issuedate,
                      id.is_read as is_read,
                      id.detail_responsed as solution,
                      id.area as area,
                      areas.title_kh as feedback_area,
                      id.status as issue_status,
                      fs.title as status_en,
                      fs.title_kh as status_kh,
                      us.display_name as agent_name,
                      ch.title_kh as channel,
                      br.name_kh as branch_name,
                      re.region as regional_code,
                      re.name as regional_name,
                      id.staff_position as related_position,
                      id.service_action as service_saction,
                      id.fc as branch_fc,
                      (SELECT title_kh FROM tbl_financial_services WHERE id=cd.client_type LIMIT 1) as financial_service,
                      (SELECT title_kh FROM tbl_financial_services WHERE id=id.service_action LIMIT 1) as service_action,
                      (SELECT title_kh FROM tbl_agent WHERE id=id.staff_position LIMIT 1) as commited_by
                    FROM tbl_issue_detail as id 
                      INNER JOIN tbl_client_detail as cd ON cd.id=id.client_id
                      INNER JOIN tbl_user as us on id.user_id=us.id
                      INNER JOIN tbl_channel as ch on ch.id=id.client_channel
                      INNER JOIN branches as br on br.fc=id.fc
                      INNER JOIN tbl_regional as re on re.id=br.regional_id
                      INNER JOIN tbl_feedback_status as fs ON fs.id=id.status
                      INNER JOIN tbl_issue_category as cat ON cat.id=cd.client_category
                      INNER JOIN tbl_area as areas ON areas.id=id.area
                      $conditions
                      AND id.is_deleted=0
                      ORDER BY id.issue_date DESC";
                $result = $conn->query($query);
                // echo $query;
          echo isset($_GET['btn_filter_submit']) && $result?'<p style="padding:0;margin:0;float:left;margin: 0;;"><span style="color:#4db848;">លទ្ធផលរកឃើញ </span> <span style="color:#ee6e73;">'.$total_records_count.'</span><span style="color:#4db848;"> របាយការណ៍</span> </p>':'';
          $customer  = array();
          $i=1;
          if(($result) && $result->num_rows>0){
            while($row = mysqli_fetch_object($result)) {
              $is_read = '';
              if(isset($_SESSION['login_id']) && $_SESSION['login_id'] ==1){
                $is_read = $row->is_read==1?'read-items':'unread-items';
              }
              $status_class = 'done';
              $icon_status  = 'check_circle';
              $others = 'ផ្សេងៗ';
              switch($row->issue_status){
                case 1:
                  $status_class = 'done';
                  $icon_status  = 'check_circle';
                break;
                case 2:
                  $status_class = 'pending';
                  $icon_status  = 'info';
                break;
                case 3:
                  $status_class = 'fail';
                  $icon_status  = 'cancel';
                break;
              }
            ?>
            <input type="hidden" name="export_query_condition" value="<?php echo $conditions;?>" />
            <tr class="<?php echo $is_read;?> row-item">
              <td class="center view"><a href='index.php?menu=myreport&page=view&item=<?php echo $row->feedback_id;?>'><?php echo date('Y-m-d',strtotime($row->issuedate));?></span></a></td>
              <td class="view"><a href='index.php?menu=myreport&page=view&item=<?php echo $row->feedback_id;?>'><?php echo $row->client_name;?></span></a></td>
              <td class="center view"><a href='index.php?menu=myreport&page=view&item=<?php echo $row->feedback_id;?>'><?php echo (isset($row->phone) && $row->phone !='')?$row->phone:'N/A';?></a></td>
              <td class="center view"><a href='index.php?menu=myreport&page=view&item=<?php echo $row->feedback_id;?>'><?php echo $row->financial_service?$row->financial_service:$others;?></a></td>
              <td class="tooltipped view" data-tooltip="<?php echo $row->issue;?>"><span class="one-row-text"><a href='index.php?menu=myreport&page=view&item=<?php echo $row->feedback_id;?>'><?php echo $row->issue;?></a></span></td>
              <td class="center tooltipped" data-position="left" data-tooltip="<i style='margin-top:3px;' alt='<?php echo $row->status_en;?>' class='material-icons <?php echo $status_class;?> left'><?php echo $icon_status;?></i><span style='margin-top:5px;float:right;'><?php echo $row->status_en;?></span>"><i alt="<?php echo $row->status_en;?>" class="material-icons <?php echo $status_class;?>"><?php echo $icon_status;?></i></td>
              <td class="center tooltipped" data-position="left" data-tooltip="ធ្វើការកែប្រែរបាយការណ៍"><a  href="index.php?menu=myreport&page=edit&item=<?php echo $row->issue_id?>" style="color:#4db848;" ><i class="fa fa-edit"></i></a></td>
            </tr>
            <?php 
            $i++;
            }
          }else{
            echo '<tr class="pagination"><td colspan="8" class="centered"><p style="color:#ee6e73;paddin:0;margin:0;">ពុំ​មាន​របាយការណ៍​ដែល​លោក​អ្នក​ស្វែង​រក​​នោះ​ទេ</p></td></tr>';
          }
          ?>
        </tbody>
      </table>
    </form>
  <br />