<style>
   table.account-list>thead>tr>th,table.account-list>tbody>tr>td,table.account-list>tfoot>tr>td{
      padding:0 5px;
   }
   table.account-list #toast-container>.toast, .material-tooltip{
      margin-left:10px;
   }
   .modal {
      left: 0;
      right: 0;
      background-color: #fafafa;
      padding: 0;
      width:40%;
      will-change: top, opacity;
   }
   .modal.modal-fixed-footer .modal-footer{
      z-index:2;
   }
   #special-form.modal.open input[type=text]:not(.browser-default)[readonly="readonly"]{
     color: rgba(0,0,0,1);
   }
   #special-form.modal.open input[type=text]:not(.browser-default)[readonly="readonly"]+label{
      color: rgba(0,0,0,0.7);
   }
   #special-form.modal.open select:not(.browser-default)[readonly="readonly"],input.invalid[type=tel]:not(.browser-default):focus{
      box-shadow: none;
      webkit-box-shadow:none;
   }
   #special-form.modal.open .input-field .helper-text{
      margin-top: 5px;
   }
   textarea#remark{
      min-height: 100px;
   }
</style>
<blockquote>បញ្ជីគណនីលេខពិសេស</blockquote>
   <?php 
      $date = new DateTime();
      $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
      $today_date = $date->format("Y-m-01");
      $last_date_of_the_month = $date->format("Y-m-t");
   ?>
   <form action="" method="GET" class="filter-form" id="special_account_search">
      <div class="col s12 right filter-section" style="padding:0;">
         <div class="input-field col s2" style="padding-left:0;">
            <input type="text" class="validate" id="search_client_name" name="search_client_name" placeholder="សុខ តារា" value="<?php echo isset($_GET['search_client_name'])?$_GET['search_client_name']:'';?>">
            <label for="search_client_name" class="active">ឈ្មោះអតិថិជន</label>
         </div>
         <div class="input-field col s2">
            <input type="text" class="validate" id="search_client_phone" name="search_client_phone" placeholder="023 999 911" value="<?php echo isset($_GET['search_client_phone'])?$_GET['search_client_phone']:'';?>">
            <label for="search_client_phone" class="active">លេខទូរស័ព្ទ</label>
         </div>
         <div class="input-field col s2">
            <input type="text" class="search_date validate" id="search_date" name="search_date"  value="<?php echo isset($_GET['search_date'])?$_GET['search_date']:$last_date_of_the_month;?>">
            <label for="search_date" class="active">កាលបរិច្ឆេទ</label>
         </div>
         <div class="input-field col s2">
            <select name="account_status" id="account_status"> 
               <option value=""></option>
               <option value="1" <?php echo isset($_GET['search_result']) && $_GET['search_result']==1?' selected':'';?>> មិនទាន់បានបើកគណនី</option>
               <option value="2" <?php echo isset($_GET['search_result']) && $_GET['search_result']==2?' selected':'';?>> បានបើកគណនីរួចរាល់</option>
               <option value="3" <?php echo isset($_GET['search_result']) && $_GET['search_result']==3?' selected':'';?>> លុបគណនី</option>
            </select>
            <label for="account_status" class="active">លទ្ធផល</label>
         </div>
         <div class="input-field col s2">
            <input type="text" class="validate" id="search_staff_id" name="search_staff_id"  placeholder="09072" value="<?php echo isset($_GET['search_staff_id'])?$_GET['search_staff_id']:'';?>">
            <label for="search_staff_id" class="active">អត្តលេខបុគ្គលិក</label>
         </div>
         <button type="button" class="btn waves-effect btn-success waves-left" name="btn_sp_search" id="btn_sp_search" style="cursor:pointer; padding:0 2rem;border:none;height:33px;margin: 3px 5px;">ស្វែងរក</button>
         <div class="right" style="padding:0;">
            <div class="input-field" style="padding:0;">
               <div class="button-container">
                  <button type="submit" name="btn_sp_export" class="btn tooltipped" id="btn_sp_export" data-delay="30" data-tooltip="ទាញ​យក​ទិន្នន័យ​ជា EXCEL"><i style="font-size:2.3rem;" class="fa fa-file-excel-o btn-excel" aria-hidden="true"></i></button>
               </div>
            </div>
         </div>
      </div>
   </form>
   <table class="highlight bordered admin-table responsive-table account-list">
      <thead>
         <tr>
            <th class="centered" style="width:5%;">លេខរៀង</th>
            <th class="centered" style="width:10%;">លេខគណនី</th>
            <th class="centered" style="width:5%;">តម្លៃលេខ</th>
            <th class="centered" style="width:10%;">ឈ្មោះអតិថិជន</th>
            <th class="centered" style="width:10%;">លេខទូរស័ព្ទ</th>
            <th class="centered" style="width:10%;">កាលបរិច្ឆេទកក់</th>
            <th class="centered" style="width:5%;">លទ្ធផល</th>
            <th class="centered" style="width:15%;">បរិយាយ</th>
            <th class="centered" style="width:5%;">កែប្រែ</th>
         </tr>
      </thead>
      <tbody>
      
      </tbody>
   </table>
   <!-- Modal Structure -->
   <div id="special-form" class="modal modal-fixed-footer">
      <form method="POST" action="" class="special_account_update" id="special_account_update">
         <div class="modal-content">
            <blockquote>កែប្រែលទ្ធផលគណនីលេខពិសេស</blockquote><br />
            <div id="data_loader">
               <div class="row">
                     <div class="input-field col s8 offset-s2">
                        <input id="client_name" type="text" name="client_name" value="" class="validate" readonly="readonly" >
                        <label for="client_name" class="active">ឈ្មោះអតិថិជន</label>
                     </div>
               </div>
               <div class="row">
                  <div class="input-field col s8 offset-s2">
                     <input id="client_phone"  name="client_phone" value="" type="text" class="validate" readonly="readonly" >
                     <label for="client_phone"  class="active">លេខទូរស័ព្ទ</label>
                  </div>
               </div>

               <div class="row">
                  <div class="input-field col s8 offset-s2">
                     <input id="account_number"  name="account_number" value="" type="text" readonly="readonly" class="validate" style="font-size: 1.5rem;color:#ef7479;">
                     <label for="account_number"  class="active">លេខគណនី</label>
                  </div>
               </div>

               <div class="row">
                  <div class="input-field col s8 offset-s2">
                     <input id="account_price" name="account_price" value="" type="text" class="validate"  readonly="readonly" style="font-size: 1.5rem;color:#ef7479;" >
                     <label for="account_price"  class="active">តម្លៃ​($)</label>
                  </div>
               </div>

               <div class="row">
                  <div class="input-field col s8 offset-s2">
                     <select name="account_status" id="account_status"> 
                        <?php
                           $account_statuses = array(1=>'មិនទាន់បានបើកគណនី',2=>'បានបើកគណនីរួចរាល់',3=>'លុបគណនី');
                           foreach($account_statuses as $key=>$value) {
                              echo '<option value="'.$key.'">'.$value.'</option>';
                           }
                           ?>
                     </select>
                     <label for="report_channel" class="active green-text">លទ្ធផល</label>
                  </div>
               </div>
               <div class="row" id="branch_section" style="display:none;">
                  <div class="input-field col s8 offset-s2">
                     <input type="text" id="branch_id" name="branch_id" value="" class="validate" placeholder="001">
                     <label for="branch_id" class="active">សាខា</label>
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s8 offset-s2">
                     <input type="text" id="staff_id" name="staff_id" value="" class="validate" placeholder="ឧទាហរណ៍៖ 09072">
                     <label for="staff_id" class="active">អត្តលេខបុគ្គលិក</label>
                  </div>
               </div>

               <div class="row">
                  <div class="input-field col s8 offset-s2">
                     <textarea name="remark" id="remark"  class="materialize-textarea" placeholder="" data-length="250" maxlength="250"></textarea>
                     <label for="remark" class="active">បរិយាយ</label>
                     <span class="helper-text">សូមរៀបរាប់ខ្លះៗថាហេតុអ្វីបានជាអតិថិជន មិនទាន់បើកគណនី ឬ មិនបើកគណនី</span>
                  </div>
               </div>
               <input type="hidden" value="" name="account_id" id="account_id"/>
            </div>
         </div>
         <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect btn btn-warning waves-left"><i class="fa fa-close"></i> បិទ</a>
            <button type="submit" name="btn-update" class="btn waves-effect btn-success waves-left btn-update-status"><i class="fa fa-floppy-o"></i> កែប្រែ</button>
         </div>
      </form>
   </div>
   <br />