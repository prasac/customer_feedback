
      <blockquote class="moul_font">របាយការណ៍គុណភាពសេវាកម្ម</blockquote>
      <?php 
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
        $current_date_time = $date->format("Y-m-d h:i:s");
      ?>
        <div class="row">
          <div class="col s12 l12">
            <div class="col s12 l12 right">
              <div class="row" style="margin-bottom:0;">
                <div class="right green-text">
                  <div class="left">
                    <span class="left reload-text">ផ្ទុកទិន្នន័យឡើងវិញ</span> <i class="material-icons reload-page right">cached</i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row row-report">
            <div class="col s12 l12">
                <?php 
                    $file_dir = glob('../auto_download/Reports/*');
                    if(!empty($file_dir)){
                      foreach($file_dir as $file_list){
                        $display_name = explode('/',$file_list);
                        echo "<div class='col s6 l6 text-left green-text row-space'><i class='fa fa-file-excel-o'></i> <a class='green-text' download href='".$file_list. "'>".$display_name[3]." <i style='font-size:1.5rem;' class='material-icons btn-excel'>file_download</i></a></div>";
                      }
                    }else{
                      echo '<span class="warning pending text-center"><i class="material-icons left">info_outline</i> មិនទាន់មានរបាយការណ៍នៅឡើយ</span>';
                    }
                ?>
            </div>
        </div>

        <style>
            .row-report{
                background:#ffffff6b;
                margin-top:2rem;
                padding:15px;
            }
            .row-space{
                margin:10px 0;
            }
            .row-report .row-space a.green-text:hover,
            .row-report .row-space a.green-text:hover ~ .row-report .green-text.row-space{
                color:#ee6e73 !important;
            }
        </style>