
      <blockquote class="moul_font">របាយការណ៍គុណភាពសេវាកម្ម</blockquote>
      <?php 
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
        $current_date_time = $date->format("Y-m-d h:i:s");

        //check the active hour of the day
        $current_time = $date->format("h:i a");
        $morning = "7:30 am";
        $evening = "1:00 pm";
        $date1 = DateTime::createFromFormat('h:i a', $current_time);
        $date2 = DateTime::createFromFormat('h:i a', $morning);
        $date3 = DateTime::createFromFormat('h:i a', $evening);
        $branch_status = "inactive fail";
        $branch_status_message = "<b><i class='fa fa-times-circle fa-2x fail'></i> សូមអភ័យទោស!</b><br />លោកគ្រូ អ្នកគ្រូអាចបញ្ចូលរបាយការណ៍ត្រួតពិនិត្យគុណភាពផ្ដល់សេវាប្រចាំថ្ងៃនៅចន្លោះម៉ោង 7:30 ព្រឹក រហូតដល់ម៉ោង 1:00 រសៀល តែប៉ុណ្ណោះ!";
        $branch_status_icon = 'close';
        $branch_status_access_modal = 0;
        if ($date1 > $date2 && $date1 < $date3)
        {
          $branch_status = "active done";
          $branch_status_message = "<b><i class='fa fa-check-circle done fa-2x'></i> អបអរសាទរ!</b><br />លោកគ្រូ អ្នកគ្រូអាចបញ្ចូលរបាយការណ៍ត្រួតពិនិត្យគុណភាពផ្ដល់សេវាប្រចាំថ្ងៃក្នុងពេលនេះបានជាធម្មតា";
          $branch_status_icon = 'check_circle';
          $branch_status_access_modal = 1;
        }
      ?>
      <?php  
        if(isset($_SESSION['login_id']) && $_SESSION['login_id'] == 1){ 
        //If logged user is admin user
        ?>
        <div class="row">
          <div class="col s12 l12">
            <div class="col s12 l12 right">
              <div class="row" style="margin-bottom:0;">
                <div class="right green-text">
                  <div class="left">
                    <span class="left reload-text">ផ្ទុកទិន្នន័យឡើងវិញ</span> <i class="material-icons reload-page right">cached</i>
                  </div>
                  <a href="index.php?menu=service_checking&page=service_report-list" class="btn btn-report left"><i style="font-size:1.5rem;" class="material-icons btn-excel left">insert_chart</i> មើលរបាយការណ៍</a>
                  <a href="#!"  data-target="newstaff" class="modal-trigger btn btn-report"><i style="font-size:1.5rem;" class="material-icons left">person_add</i>បញ្ចូលបុគ្គលិកថ្មី</a>
                  <a href="index.php?menu=service_checking&page=service_checking-setting" class="btn btn-report"><i style="font-size:1.5rem;" class="material-icons left">settings</i>ការកំណត់</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s12 l12 right">
            <form class="form search-report-form" action="" method="POST">
              <div class="row">
                <!-- <div class="col l4 xl2">
                  <label for="search_date" class="active green-text">កាលបរិច្ចេទ</label>
                  <input type="text" class="search_date" id="search_date" name="search_date"  placeholder="<?php //echo date("Y-m-d");?>" value="<?php //echo isset($_POST['search_date'])?$_POST['search_date']:date("Y-m-d");?>">
                </div> -->
                <div class="col l4 xl2">
                  <label class="active green-text" for="position_search">តួនាទីបុគ្គលិក</label>
                  <select name="position_search[]" id="position_search" multiple>
                    <?php
                    $query_position_search= $conn->query("SELECT * FROM tbl_service_quality_checking_staff_position ORDER BY id");
                    $position_search = isset($_POST['position_search'])?$_POST['position_search']:false;
                      $i=0;
                      echo '<option value=""';
                      echo (!$position_search) || $position_search[0]==''?' selected':'';
                      echo '>ទាំងអស់</option>';
                      while($row = mysqli_fetch_object($query_position_search)) {
                        if($position_search && in_array($row->id, $position_search)){
                          echo '<option value="'.$row->id.'" selected>'.$row->staff_position_kh.'</option>';
                        }else{
                          echo '<option value="'.$row->id.'" >'.$row->staff_position_kh.'</option>';
                        }
                        $i++;
                      }
                    ?>
                  </select>
                </div>
                <div class="col l4 xl2">
                  <label class="active green-text" for="service_checking_regional">ភូមិភាគ</label>
                  <select name="service_checking_regional" id="service_checking_regional" class="browser-default">
                    <?php
                    $query_branch = $conn->query("SELECT id,name FROM tbl_regional ORDER BY region ");
                    $regional_posted = isset($_POST['service_checking_regional'])?$_POST['service_checking_regional']:false;
                      $i=0;
                      echo '<option value="" ';
                      echo !$regional_posted?'selected':'';
                      echo '>ទាំងអស់</option>';
                      while($row = mysqli_fetch_object($query_branch)) {
                        if($regional_posted && $regional_posted==$row->id){
                          echo '<option value="'.$row->id.'" selected>'.$row->name.'</option>';
                        }else{
                          echo '<option value="'.$row->id.'" >'.$row->name.'</option>';
                        }
                        $i++;
                      }
                    ?>
                  </select>
                </div>
                <div class="col l4 xl2">
                  <label class="active green-text" for="service_checking_branch">សាខា</label>
                  <select name="service_checking_branch[]" id="service_checking_branch" multiple >
                    <?php
                    $query_branch = $conn->query("SELECT id,name,name_kh,regional_id FROM branches ORDER BY regional_id ASC ");
                    $branch_posted = isset($_POST['service_checking_branch'])?$_POST['service_checking_branch']:0;
                      $i=0;
                      echo '<option value="" ';
                      echo (!$branch_posted) || $branch_posted[0]==''?' selected':'';
                      echo '>ទាំងអស់</option>';
                      while($row = mysqli_fetch_object($query_branch)) {
                        if($branch_posted && in_array($row->id, $branch_posted)){
                          echo '<option value="'.$row->id.'" selected class="'.$row->regional_id.'">'.$row->name_kh.'</option>';
                        }else{
                          echo '<option value="'.$row->id.'" class="'.$row->regional_id.'">'.$row->name_kh.'</option>';
                        }
                        $i++;
                      }

                    ?>
                  </select>
                </div>
                <div class="col l6 xl4">
                  <button class="btn btn-success waves-effect waves-light left" id="btn_regional_branch_filter" type="submit" name="btn_regional_branch_filter"><i style="font-size:1.5rem;" class="material-icons left">search</i>បង្ហាញ​ទិន្នន័យ</button>
                  <button type="submit" name="btn-excel" id="btn-export-service-checking" class="waves-effect waves-light btn btn-primary"><i style="font-size:1.5rem;" class="material-icons btn-excel left">file_download</i>ដោនឡូត</button>
                  <button type="submit" name="btn-truncat" id="btn-truncate-service-checking" class="waves-effect waves-light btn btn-primary"><i style="font-size:1.5rem;" class="material-icons btn-excel left" aria-hidden="true">delete_forever</i>លុបទិន្និន័យ</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      <?php 
      }
      ?>
        <?php  
        if(isset($_SESSION['login_id']) && $_SESSION['login_id'] != 1){ 
        //If logged user is not admin user

        ?>
        <h5 class="sub_title">សម្រាប់សាខា <?php echo  $_SESSION['login_user_fc'].'_'.$display_name; ?></h5>
        <div class="right green-text">
          <div class="left">
            <span class="left reload-text">ផ្ទុកទិន្នន័យឡើងវិញ</span> <i class="material-icons reload-page right">cached</i>
          </div>&nbsp;&nbsp;
          <div class="right">
            <span class="left reload-text">ស្ថានភាពសាខា </span> <i class="material-icons branch_status right tooltipped <?php echo $branch_status; ?>" data-tooltip="<?php echo $branch_status_message;?>" data-position="left"><?php echo $branch_status_icon;?></i>
          </div>
        </div>
          <?php 
            if($branch_status_access_modal){
          ?>
          <div class="row">
              <div class="col s6 l6 offset-l6">
              <a href="#!"  data-target="newstaff" class="modal-trigger btn btn-primary right btn-addnew" style="margin-top:0;"><i class="material-icons left">person_add</i>បញ្ចូលបុគ្គលិកថ្មី</a>
              </div>
          </div>
          <?php 
          }
        }
        $branch_access = ' AND staff.regional_id IN(1,6)';
        $position_search = " ";
        $date_access_search = ' AND DATE(checking.created_date)=CURDATE() ';
        $staff_id_arrays ='';
        $staff_id = '';
        
        if(isset($_POST['btn_regional_branch_filter'])){ //If filter buttion is submited
          $staff_id = 'AND checking.staff_id=staff.id';
          $search_date = isset($_POST['search_date'])?$_POST['search_date']:0;
          $position_search  = isset($_POST['position_search'])?implode(",", $_POST['position_search']):0;
          $service_checking_regional  = isset($_POST['service_checking_regional'])?$_POST['service_checking_regional']:0;
          $service_checking_branch  = isset($_POST['service_checking_branch'])?implode(",",$_POST['service_checking_branch']):0;
          
          if($search_date){
            $date_access_search = "AND DATE(checking.created_date) = '$search_date%'";
          }
          if($position_search){
            $position_search = " AND staff.staff_position IN($position_search)";
          }

          if($search_date && (!$service_checking_regional && !$service_checking_branch)){
            $branch_access = "AND DATE(checking.created_date) = '$search_date%'";
          }

          if($service_checking_regional && !$service_checking_branch){
            $branch_access = "AND staff.regional_id = $service_checking_regional";
          }else if($service_checking_regional && $service_checking_branch){
            $branch_access = "AND staff.regional_id = $service_checking_regional AND staff.branch_id IN($service_checking_branch)";
          }else{
            $branch_access = "  AND staff.regional_id IN(1,6)";
          }  
        }

          if(isset($_SESSION['login_id']) && $_SESSION['login_id'] != 1){ //If logged user is not admin user
            $loggedin_branch = $_SESSION['login_branch_id'];
            $branch_access = "AND staff.branch_id=$loggedin_branch";
            $query_staff = "SELECT DISTINCT staff.staff_id AS staff_id,staff.id AS id FROM tbl_service_quality_checking_staff AS staff WHERE staff.branch_id=$loggedin_branch and status=1 ORDER BY staff.id ASC";
            $query_result_staff = $conn->query($query_staff);
            $staff_id_array = array();
            $indicator_run_query = '';
            if(($query_result_staff) && $query_result_staff->num_rows>0){
              while($row = mysqli_fetch_object($query_result_staff)){
                //Find if the report already exist
                array_push($staff_id_array,$row->id);
                $find_existing = "SELECT * FROM tbl_service_quality_checking AS old WHERE old.staff_id=$row->id AND DATE(old.created_date)=CURDATE()";
                $find_existing_query = $conn->query($find_existing);
                // echo $find_existing;
                
                if(($find_existing_query) && $find_existing_query->num_rows==0 && $branch_status_access_modal){
                  $indicator_fields = array();
                  $default_indicator_value = array();
                  $value_ = '';
                  $indicator_query = $conn->query("SELECT * FROM tbl_service_quality_checking_staff_key_indicator AS indicator ORDER BY indicator.order ASC");
                  while($row_indicator = mysqli_fetch_object($indicator_query)){
                    array_push($indicator_fields,"key_indicator$row_indicator->id");
                    $value_ = ($row_indicator->id==1)?'0':'3';
                    array_push($default_indicator_value,'"'.$value_.'"');
                  }
                  $indicator_fields = implode(",",$indicator_fields);
                  $default_indicator_value = implode(",",$default_indicator_value);

                  $query_insert_staff = "INSERT INTO tbl_service_quality_checking(staff_id,$indicator_fields,created_date,modified_date)
                                          VALUES('$row->id',$default_indicator_value,'$current_date_time','$current_date_time')";
                  $result_query = $conn->query($query_insert_staff);
                }
              }
            }
            $staff_id_arrays = implode(",", $staff_id_array);
            $staff_id = " AND checking.staff_id IN($staff_id_arrays)";
          }
         
        ?>

        <table class="highlight bordered admin-table responsive-table service-quality-check-list">
          <thead>
            <tr>
                <th class="centered" style="width:4%;">អត្តលេខ</th>
                <th class="centered" style="width:8%;">ឈ្មោះ</th>
                <th class="centered" style="width:13%;">តួនាទី</th>
                <?php 
                  $query = "SELECT * FROM tbl_service_quality_checking_staff_key_indicator AS indicator ORDER BY indicator.order ASC";
                  $result_indicator = $conn->query($query);
                  $number_of_indicator_width = (70/$result_indicator->num_rows);
                  if(($result_indicator) && $result_indicator->num_rows>0){
                    $number_of_indicator_widths = 0;
                    while($row = mysqli_fetch_object($result_indicator)) {
                      $number_of_indicator_widths = $row->id==1?$number_of_indicator_width+5:$number_of_indicator_width;
                      $tooltipped_class = $row->note !=''?' tooltipped':'';
                      $info_icon = '<i class="material-icons pending">info</i>';
                      echo '<th class="centered '.$tooltipped_class.'" style="width:'.$number_of_indicator_widths.'%;"  data-tooltip="'.$row->note.'" data-position="top">'.$row->title_kh.' '.$info_icon.'</th>';
                    }
                  }
                ?>
            </tr>
          </thead>
          <tbody>
            <?php
            $indicator_fields = array();
            $indicator_fields_only = array();
            $indicator_query = $conn->query("SELECT * FROM tbl_service_quality_checking_staff_key_indicator AS indicator ORDER BY indicator.order ASC");
            while($row_indicator = mysqli_fetch_object($indicator_query)){
              array_push($indicator_fields,"checking.key_indicator$row_indicator->id");
              array_push($indicator_fields_only,"key_indicator$row_indicator->id");
            }
            $indicator_fields_imploded = implode(",", $indicator_fields);
            $query = "SELECT
                      staff.id AS id,
                      staff.staff_id AS staff_id,
                      staff.branch_id as branch_id_main,
                      staff.staff_fullname_kh AS staff_fullname_kh,
                      $indicator_fields_imploded
                      ,pos.staff_position_kh AS staff_position,
                      checking.reported_by AS reported_by
                    FROM tbl_service_quality_checking_staff AS staff 
                    LEFT JOIN tbl_service_quality_checking AS checking ON staff.id = checking.staff_id
                    LEFT JOIN tbl_service_quality_checking_staff_position AS pos ON pos.id = staff.staff_position
                    WHERE staff.status = 1 $branch_access $position_search
                    ORDER BY staff.regional_id,staff.branch_id, staff.staff_position ASC";
                  // echo $query;
            $result = $conn->query($query);
            if(($result) && $result->num_rows>0){
              while($row = mysqli_fetch_object($result)) {
              ?>
              <tr class="row-items modal-trigger" data-target="<?php echo $branch_status_access_modal?'modal_'.$row->staff_id:'';?>">
                <td class="center view"><?php echo $row->staff_id;?></td>
                <td class="view"><?php echo $row->staff_fullname_kh;?></td>
                <td class="view"><?php echo $row->staff_position;?></td>
                <?php 
                $tooltipped = 'tooltipped';
                $status_class = '';
                $icon_status = '';
                $others = 'ផ្សេងៗ';
                $scoring = array(0=>'<span style="font-size: 12px;color: #c5c2c2;font-style: italic;">N/A</span>',1=>'Full',2=>'Partial',3=>'No');
                $i=1;
                foreach ($indicator_fields_only as $indicator_fields){
                  switch($row->$indicator_fields){
                    case '1'://Full
                      $status_class = 'done';
                      $icon_status  = 'fa fa-check-circle';
                      break;
                    case '2'://Partial
                      $status_class = 'pending';
                      $icon_status  = 'fa fa-exclamation-circle';
                      break;
                    case '3'://No
                      $status_class = 'fail';
                      $icon_status  = 'fa fa-times-circle';
                      break;
                  }
                  if($i==1){ 
                    $tooltipClass =  (!empty($row->$indicator_fields)) || ($row->$indicator_fields !=0)?' tooltipped':'';
                    $data_value = (!empty($row->$indicator_fields)) || ($row->$indicator_fields !=0)?$row->$indicator_fields:'';
                  ?>
                    <td class="view <?php echo $tooltipClass ;?>" data-position="top" data-tooltip="<?php echo $row->$indicator_fields !='0'?$row->$indicator_fields:'';?>" ><span class="one-row-text"><?php echo  $data_value;?></td>
                  <?php
                  }else{
                    $col_indicator = $row->$indicator_fields !=''?$row->$indicator_fields:0;
                    $cell_value = $scoring[$col_indicator];
                    ?>
                    <td class="view center"><span class="one-row-text <?php echo $status_class;?>"><i class="<?php echo $icon_status.' '.$status_class;?>"></i> <?php echo  $cell_value;?></td>
                    <?php
                  }
                  $i++;
                }
                ?>
              </tr>

              <!-- Modal Trigger -->
              <div class="modal fade modal-fixed-footer modal-service-checking" id="<?php echo "modal_$row->staff_id";?>">
                <form method="POST" action="#" class="service_checking_form">
                  <div class="modal-content">
                    <blockquote><?php echo $row->staff_fullname_kh."_".$row->staff_id;?></blockquote>
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row->staff_position;?></span>
                    <div class="row">
                      <div class="input-field col s8 offset-s2">
                        <p>សូមលោកគ្រូ អ្នកគ្រូមេតា្តបំពេញរបាយការណ៍ពីបុគ្គលិកលក្ខណៈរបស់បុគ្គលិកម្នាក់ៗសម្រាប់ថ្ងៃនេះដូចខាងក្រោម៖</p>
                        <ul class="list" style="padding:0;">
                          <?php 
                          $query = "SELECT * FROM tbl_service_quality_checking_staff_key_indicator AS indicator ORDER BY indicator.order DESC";
                          $result_indicator = $conn->query($query);
                          if(($result_indicator) && $result_indicator->num_rows>0){
                            $indicator_fields = '';
                            $i=7;
                            while($rows = mysqli_fetch_object($result_indicator)) {
                              $indicator_fields = 'key_indicator'.$i;
                              $row_value = $row->$indicator_fields;
                              $fields_name = "service_checking_".strtolower(trim($rows->title_en));
                              if($rows->id != 1){
                              ?>
                              <li class="item"><?php echo $rows->title_kh;?>
                                  <span>
                                    <label><input name="<?php echo $fields_name; ?>" value="1" type="radio" <?php echo $row_value==1?' checked="checked"':'';?>/> <span>Full</span></label>
                                    <label><input name="<?php echo $fields_name; ?>" value="2" type="radio" <?php echo $row_value==2?' checked="checked"':'';?>/> <span>Partial</span></label>
                                    <label><input name="<?php echo $fields_name; ?>" value="3" type="radio" <?php echo $row_value==3?' checked="checked"':'';?>/> <span>No</span></label>
                                  </span>
                              </li>
                              <?php
                                }
                                if($rows->id==1){
                                  $note_text = $row->$indicator_fields !='0'?$row->$indicator_fields:'';
                                  echo '<li>'.$rows->title_kh.' <i class="material-icons tooltipped" data-position="right" data-tooltip="'.$rows->note.'">info</i>
                                  <span>
                                    <textarea name="service_checking_note" cols="35" row="200" class="materialize-textarea validate service_checking_note" data-length="500" maxlength="500">'.$note_text.'</textarea>
                                  </span>
                                </li>';
                              }
                              $i--;
                            }
                          }
                          ?>
                        </ul>
                        <div class="input-field col s4" style="padding:0;">
                          <?php
                            $query_staff = "SELECT staff.id AS staff_id FROM tbl_service_quality_checking_staff AS staff WHERE staff.branch_id=$row->branch_id_main";
                            $result_indicator_staff = $conn->query($query_staff);
                            $staff_id_array = array();
                            while($row_staff =  mysqli_fetch_object($result_indicator_staff)){
                              array_push($staff_id_array,$row_staff->staff_id);
                            }


                            $staff_id_array = implode(",",$staff_id_array);
                            $query_s = "SELECT DISTINCT(checking.reported_by) AS reported_by FROM tbl_service_quality_checking AS checking WHERE checking.staff_id IN($staff_id_array) $date_access_search AND checking.reported_by !=''";
                            $result_reported_by = $conn->query($query_s);
                            $result_reported_by_list =  mysqli_fetch_object($result_reported_by);
                            $reported_by_id = $result_reported_by_list?$result_reported_by_list->reported_by:'';
                          ?>
                          <span>អត្តលេខអ្នកទទួលបន្ទុក</span>
                          <input name="service_checking_reported_staff_id" type="text" class="validate service_checking_reported_staff_id" <?php echo $reported_by_id !=''?' readonly="readonly"':'';?> placeholder="09072" minlength="5" maxlength="5" value="<?php echo $reported_by_id;?>">
                          <input type="hidden" name="service_checking_staff_id" class="service_checking_staff_id" value="<?php echo $row->id;?>" />
                        </div>
                        <div class="input-field col s8" id="staff_checking_status"></div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <a class="btn waves-effect waves-light left btn-primary btn-edit-staff" data-staff="<?php echo $row->staff_id;?>"><i class="fa fa-pencil-square-o"></i> កែប្រែព័ត៌មានបុគ្គលិក</a>
                    <button class="btn waves-effect waves-light btn_submit_update" data-action="update" form="service_checking"  type="submit" name="btn_submit_update"><i class="fa fa-floppy-o"></i> រក្សាទុក</button>
                    <a class="btn waves-effect waves-light red btn_delete_staff" data-action="delete" ><i class="fa fa-trash"></i> លុបបុគ្គលិក</a>
                    <button type="reset" class="modal-action btn orange darken-1"><i class="fa fa-minus"></i> សម្អាត</button>
                    <a href="#!" class="modal-action modal-close btn deep-orange darken-2"><i class="fa fa-times"></i> ចាកចេញ</a>
                  </div>
                </form>
              </div>
              
              <?php
              }
              ?>
              <tr class="total-row">
                <td rowspan="3" colspan="3" class="text-right">
                  សរុប៖
                </td>
                <td><span class="center wrapper done"><i class="fa fa-check-circle done"></i> Full</span></td>
                <?php
                  array_shift($indicator_fields_only);
                  foreach ($indicator_fields_only as $indicator_fields){
                    $query = "SELECT COUNT(*) AS sum_full FROM tbl_service_quality_checking AS checking INNER JOIN tbl_service_quality_checking_staff as staff ON staff.id=checking.staff_id WHERE $indicator_fields=1 $branch_access";
                    $result = $conn->query($query);
                    $sum_full = mysqli_fetch_object($result)->sum_full;
                    echo '<td class="center done">'.$sum_full.'</td>';
                  }
                ?>
              </tr>
              <tr class="total-row">
                <td><span class="center wrapper pending"><i class="fa fa-exclamation-circle pending"></i> Partial</span></td>
                <?php
                  foreach ($indicator_fields_only as $indicator_fields){
                    $query = "SELECT COUNT(*) AS sum_pending FROM tbl_service_quality_checking AS checking INNER JOIN tbl_service_quality_checking_staff as staff ON staff.id=checking.staff_id WHERE $indicator_fields=2 $branch_access";
                    $result = $conn->query($query);
                    $sum_pending= mysqli_fetch_object($result)->sum_pending;
                    echo '<td class="center pending">'.$sum_pending.'</td>';
                  }
                ?>
              </tr>
              <tr class="total-row">
                <td><span class="center wrapper fail"><i class="fa fa-times-circle fail"></i> Fail</span></td>
                <?php
                    foreach ($indicator_fields_only as $indicator_fields){
                      $query = "SELECT COUNT(*) AS sum_fail FROM tbl_service_quality_checking AS checking INNER JOIN tbl_service_quality_checking_staff as staff ON staff.id=checking.staff_id WHERE $indicator_fields=3 $branch_access";
                      $result_fail = $conn->query($query);
                      $sum_fail= mysqli_fetch_object($result_fail)->sum_fail;
                      echo '<td class="center fail">'.$sum_fail.'</td>';
                    }
                ?>
              </tr>
              <?php
            }else{
              echo '<tr class="pagination"><td colspan="10" class="centered"><p style="color:#ee6e73;paddin:0;margin:0;">ពុំ​ទាន់មានសាខាញ្ចូលរបាយការណ៍នៅឡើយ</p></td></tr>';
            }
            ?>
            
          </tbody>
        </table>

        <!-- Modal Trigger for Add new staff-->
        <div id="newstaff" class="modal fade modal-fixed-footer modal-add-new-staff">
          <form method="POST" action="#" class="service_checking_new_staff" id="service_checking_new_staff">
            <div class="modal-content">
              <blockquote>បញ្ចូលបុគ្គលិកថ្មី</blockquote>
              <div class="col s12">
                <p class="center">សូមគ្រូ អ្នកគ្រូធ្វើការបំពេញព័ត៌មានបុគ្គលិកថ្មីទៅតាមទម្រង់ដូចខាងក្រោម៖</p>
                <div class="row">
                  <div class="input-field col s4">
                    <label for="new_staff_id" class="active">អត្តលេខបុគ្គលិក</label>
                    <input id="new_staff_id" name="new_staff_id" type="text" class="validate">
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6">
                    <label for="new_staff_name_kh" class="active">ឈ្មោះបុគ្គលិកជាអក្សរខ្មែរ</label>
                    <input id="new_staff_name_kh" name="new_staff_name_kh" type="text" class="validate">
                  </div>
                  <div class="input-field col s6">
                    <label for="new_staff_name_en" class="active">ឈ្មោះបុគ្គលិកជាអក្សរឡាតាំង (Optional)</label>
                    <input id="new_staff_name_en" name="new_staff_name_en" type="text" class="validate">
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6">
                    <label class="active green-text">ជ្រើសរើសតួនាទី</label>
                    <select name="new_staff_position" id="new_staff_position">
                      <?php
                      $query_position = $conn->query("SELECT id,staff_position_kh FROM tbl_service_quality_checking_staff_position ORDER BY staff_position_kh ASC ");
                        while($row = mysqli_fetch_object($query_position)) {
                          echo '<option value="'.$row->id.'" >'.$row->staff_position_kh.'</option>';
                          $i++;
                        }
                      ?>
                    </select>
                  </div>
                  <div class="input-field col s6">
                    <label for="new_staff_branch_fc" class="active">លេខកូដសាខា</label>
                    <?php 
                    $branch_fc_number = '';
                    $branch_id_number = '';
                    $regional_id_number='';
                    $ready_notadmin = '';
                    if(isset($_SESSION['login_id']) && $_SESSION['login_id'] != 1){ 
                      $branch_fc_number = $_SESSION['login_user_fc'];
                      $branch_id_number = $_SESSION['login_branch_id'];
                      $regional_id_number = $_SESSION['login_regional_id'];
                      $ready_notadmin = ' readonly';
                    }
                    ?>
                    <input id="new_staff_branch_fc" name="new_staff_branch_fc" type="text"  <?php echo $ready_notadmin; ?> class="validate" value="<?php echo $branch_fc_number; ?>">
                    <input type="hidden" name="branch_id" id="branch_id" value="<?php echo $branch_id_number;?>"/>
                    <input type="hidden" name="regional_id" id="regional_id" value="<?php echo $regional_id_number;?>" />
                  </div>
                  <div class="input-field col s6 offset-s6">
                    <div id="branch_checking_status"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn waves-effect waves-light btn_add_new_staff" type="submit" name="btn_add_new_staff"><i class="fa fa-floppy-o"></i> រក្សាទុក</button>
              <button type="reset" class="modal-action btn orange darken-1"><i class="fa fa-minus"></i> សម្អាត</button>
              <a href="#!" class="modal-action modal-close btn deep-orange darken-2"><i class="fa fa-times"></i>  ចាកចេញ</a>
            </div>
          </form>
        </div>

        <!-- Modal Trigger for edit staff-->
        <div id="modal_edit_staff" class="modal fade modal-fixed-footer modal-add-new-staff">
          <form action="#" method="POST" class="service_checking_new_staff service_checking_edit_staff" id="service_chicking_edit_staff">
            <div class="modal-content">
              <blockquote>កែប្រែព័ត៌មានបុគ្គលិក</blockquote>
              <div class="col s12">
                <p class="center">សូមគ្រូ អ្នកគ្រូធ្វើការកែប្រែព័ត៌មានបុគ្គលិកទៅតាមទម្រង់ដូចខាងក្រោម៖</p>
                <div class="row">
                  <div class="input-field col s4">
                    <label for="new_staff_id_edit" class="active">អត្តលេខបុគ្គលិក</label>
                    <input id="new_staff_id_edit" name="new_staff_id" type="text" class="validate" value="" />
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6">
                    <label for="new_staff_name_kh_edit" class="active">ឈ្មោះបុគ្គលិកជាអក្សរខ្មែរ</label>
                    <input id="new_staff_name_kh_edit" name="new_staff_name_kh" type="text" class="validate" value="">
                  </div>
                  <div class="input-field col s6">
                    <label for="new_staff_name_en_edit" class="active">ឈ្មោះបុគ្គលិកជាអក្សរឡាតាំង (Optional)</label>
                    <input id="new_staff_name_en_edit" name="new_staff_name_en" type="text" class="validate" value="">
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6">
                    <label class="active green-text" for="new_staff_position_edit">ជ្រើសរើសតួនាទី</label>
                    <select name="new_staff_position_edit" id="new_staff_position_edit">
                      <?php
                      $query_position = $conn->query("SELECT id,staff_position_kh FROM tbl_service_quality_checking_staff_position ORDER BY staff_position_kh ASC ");
                        while($row = mysqli_fetch_object($query_position)) {
                          echo '<option value="'.$row->id.'">';
                          echo $row->staff_position_kh;
                          echo '</option>';
                          $i++;
                        }
                      ?>
                    </select>
                  </div>
                  <div class="input-field col s6">
                    <label for="new_staff_branch_fc_edit" class="active">លេខកូដសាខា</label>
                    <?php
                    $branch_id_number = '';
                    $regional_id_number='';
                    $ready_notadmin = '';
                    if(isset($_SESSION['login_id']) && $_SESSION['login_id'] != 1){ 
                      $branch_id_number = $_SESSION['login_branch_id'];
                      $regional_id_number = $_SESSION['login_regional_id'];
                      $ready_notadmin = ' readonly';
                    }
                    ?>
                    <input id="new_staff_branch_fc_edit" name="new_staff_branch_fc" type="text"  <?php echo $ready_notadmin; ?> class="validate" value="">
                    <input type="hidden" name="new_branch_id" id="branch_id_edit" value="<?php echo $branch_id_number;?>"/>
                    <input type="hidden" name="new_regional_id" id="regional_id_edit" value="<?php echo $regional_id_number;?>" />
                  </div>
                  <div class="input-field col s6 offset-s6">
                    <div id="branch_checking_edit_status"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn waves-effect waves-light btn_update_staff" type="submit" name="btn_update_staff"><i class="fa fa-floppy-o"></i> កែប្រែព័ត៌មាន</button>
              <button type="reset" class="modal-action btn orange darken-1"><i class="fa fa-minus"></i> សម្អាត</button>
              <a href="#!" class="modal-action modal-close btn deep-orange darken-2"><i class="fa fa-times"></i> ចាកចេញ</a>
            </div>
          </form>
        </div>


      <br />
      <style>
        table.service-quality-check-list tbody tr td{
          padding:5px;
        }
        table.service-quality-check-list tbody tr td:hover{
          text-decoration: underline solid;
          cursor:pointer;
        }
        textarea#service_checking_note{
          min-height:80px;
        }
        .pending, .pending:after, .result-status p span.pending, .result-status p span.pending:after{
          color:#f0ad4e;
        }
        .fail, .fail:after, .result-status p span.fail, .result-status p span.fail:after{
          color:#ec0808a6;
        }
      </style>