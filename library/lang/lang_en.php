<?php
define("_FEEDBACKREPORT", "Feedback Report");
define("_MYACCOUNT", "My Account");
define("_CHANGEPASSWORD", "Change Password");
define("_LOGOUT", "Logout");
define("_ADDNEWREPORT", "New Report");
define("_DELETEREPORT","Deleted Report");
define("_CUSTOMERFEEDBACKREPORTLIST","Customer Feedback Report List");
define("_CUSTOMERNAME","Customer Name");
define("_FROMDATE","From Date");
define("_TODATE","To Date");
define("_RESULT","Status");
define("_DOWNLOAD","Download");
define("_COPYRIGHT","All rights reserved Mamketing Department");
define("_ISSUEDATE","Issue Date");
define("_PHONENUMBER","Phone Number");
define("_FINANCIALSERVICE","Financial Service");
define("_ISSUEINFO","Issue/Info");
define("_AREA","Area");
define("_EDIT","Edit");
define("_DELETE","Delete");
define("_BACK","Previous Page");
define("_SEARCH","Search");
define("_GENDER","Gender");
define("_CATEGORY","Category");

//Status message
define("_DONE","Done");
define("_PENDING","Pending");
define("_FAIL","Fail");
define("_EDITREPORT","Edit Report");
define("_SOLUTION","Solution/Response");
define("_ISSUEPERSON","Commited By");
define("_ISSUEVIA","Via");
define("_STAFFID","Staff ID");
define("_SAVEREPORT","Submit Report");
define("_EXAMPLE","Example");

//Suggestion Report
define("_SUGGESTIONREPORT", "Suggestion Report");
define("_SUGGESTIONREPORTLIST","Suggeestion Report List");
define("_ADDNEWSUGGESTION","New Suggestion");

//Service Quality Checking
define("_SERVICEQUALITYCHECKING","Service Quality Checking Report");
define("_SPECIALACCOUNT","Special Account Number");

