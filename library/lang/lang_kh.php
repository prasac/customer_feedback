<?php
define("_FEEDBACKREPORT", "របាយការណ៍ព័ត៌មានត្រលប់");
define("_MYACCOUNT", "គណនីរបស់ខ្ញុំ");
define("_CHANGEPASSWORD", "ផ្លាស់ប្ដូរលេខសម្ងាត់");
define("_LOGOUT", "ចាកចេញ");
define("_FROMDATE","ចាប់ពីថ្ងៃ");
define("_TODATE","ដល់ថ្ងៃ");
define("_RESULT","លទ្ធផល");
define("_DOWNLOAD","ដោនឡូត");
define("_COPYRIGHT","រក្សាសិទ្ធិគ្រប់​យ៉ាងដោយ នាយកដ្ឋានទីផ្សារនិងទំនាក់ទំនង");
define("_ISSUEDATE","កាលបរិច្ឆេទបញ្ហា/ព័ត៌មាន");
define("_PHONENUMBER","លេខទូរស័ព្ទ");
define("_FINANCIALSERVICE","ប្រភេទសេវាហិរញ្ញវត្ថុ");
define("_ISSUEINFO","បញ្ហា/ព័ត៌មាន");
define("_AREA","ប្រភេទមតិ/ព័ត៌មាន");
define("_EDIT","កែប្រែ");
define("_DELETE","លុយ");
define("_BACK","ត្រលប់ក្រោយ");
define("_SEARCH","ស្វែងរក");
define("_GENDER","ភេទ");
define("_CATEGORY","ប្រភេទ");


//Status message
define("_DONE","Done");
define("_PENDING","Pending");
define("_FAIL","Fail");
define("_EDITREPORT","កែប្រែរបាយការណ៍");
define("_ADDNEWREPORT", "បញ្ចូលព័ត៌មានត្រលប់ថ្មី");
define("_DELETEREPORT","របាយការណ៍ដែលបានលុបចេញ");
define("_CUSTOMERFEEDBACKREPORTLIST","បញ្ជីរបាយការណ៍ត្រលប់ពីអតិថិជន");
define("_CUSTOMERNAME","ឈ្មោះអតិថិជន");
define("_SOLUTION","ការឆ្លើយតប/ដំណោះស្រាយ");
define("_ISSUEPERSON","ប្រព្រឹត្តដោយ");
define("_ISSUEVIA","តារយៈ");
define("_STAFFID","អត្តលេខបុគ្គលិក");
define("_SAVEREPORT","រក្សារទុករបាយការណ៍");
define("_EXAMPLE","ឧទាហរណ៍");

//Suggestion Report
define("_SUGGESTIONREPORT","របាយការណ៍ផ្ដល់មតិយោបល់");
define("_SUGGESTIONREPORTLIST","បញ្ជីរបាយការណ៍ផ្ដល់មតិយោបល់");
define("_ADDNEWSUGGESTION","បញ្ចូលមតិយោបល់ថ្មី");

//Service Quality Checking
define("_SERVICEQUALITYCHECKING","របាយការណ៍គុណភាពសេវាកម្ម");
define("_SPECIALACCOUNTREPORT","របាយការណ៍គណនីលេខពិសេស");


