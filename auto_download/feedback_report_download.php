<?php 
    include('session.php');
    $date = new DateTime();
    $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
    $today_time = $date->format("Y-M-d_h:i");

    $this_month = isset($_POST['month'])?$_POST['month']:$date->format("M");
    $months = array(1 => 'មករា', 2 => 'កុម្ភៈ', 3 => 'មីនា', 4 => 'មេសា', 5 => 'ឧសភា', 6 => 'មិថុនា', 7 => 'កក្កដា', 8 => 'សីហា', 9 => 'កញ្ញា', 10 => 'តុលា', 11 => 'វិច្ឆិកា', 12 => 'ធ្នូ');
    $date_access_search = ' WHERE DATE(id.created_date)=CURDATE() ';  
    $query_export = "SELECT 
                      id.id as feedback_id,
                      cd.id as client_id,
                      cd.client_name as client_name,
                      cd.client_gender as gender,
                      cd.client_phone as phone,
                      cd.client_category as category,
                      cat.title as category_name,
                      cd.client_type as clienttype,
                      id.id as issue_id,
                      id.staff_id as staff_id,
                      id.detail_submited as issue,
                      id.issue_date as issuedate,
                      id.is_read as is_read,
                      id.detail_responsed as solution,
                      id.area as area,
                      areas.title as feedback_area,
                      id.status as issue_status,
                      fs.title as status_en,
                      fs.title_kh as status_kh,
                      fis.title as financial_service,
                      us.display_name as agent_name,
                      ch.title as channel,
                      br.name as branch_name,
                      re.region as regional_code,
                      re.name as regional_name,
                      id.staff_position as related_position,
                      id.service_action as service_saction,
                      id.fc as branch_fc,
                      (SELECT title FROM tbl_financial_services WHERE id=id.service_action LIMIT 1) as service_action,
                      (SELECT title FROM tbl_agent WHERE id=id.staff_position LIMIT 1) as commited_by
                    FROM tbl_issue_detail as id 
                      INNER JOIN tbl_client_detail as cd ON cd.id=id.client_id
                      INNER JOIN tbl_financial_services as fis ON fis.id=cd.client_type
                      INNER JOIN tbl_user as us on id.user_id=us.id
                      INNER JOIN tbl_channel as ch on ch.id=id.client_channel
                      INNER JOIN branches as br on br.fc=id.fc
                      INNER JOIN tbl_regional as re on re.id=br.regional_id
                      INNER JOIN tbl_feedback_status as fs ON fs.id=id.status
                      INNER JOIN tbl_issue_category as cat ON cat.id=cd.client_category
                      INNER JOIN tbl_area as areas ON areas.id=id.area
                      $date_access_search 
                      ORDER BY id.created_date ASC";
    $query_export_result = $conn->query($query_export);
    include('./PHPExcel/Classes/PHPExcel.php');
    $objPHPExcel = new PHPExcel();
    //Testing Create multiple sheet_ one sheet per item
    $stylehead1 = array(
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font'  => array(
          'color' => array('rgb' => '117515'),
          'size'  =>9,
          'name'  => 'Khmer OS Muol Light',
      ),
      'borders' => array(
        'right' => array(
          'style' => PHPExcel_Style_Border::BORDER_HAIR,
          'color' => array('rgb' => '333333')
        ),
      ),
    );
    $stylehead2 = array(
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font'  => array(
          'color' => array('rgb' => 'b78311'),
          'size'  => 9,
          'name'  => 'Khmer OS Muol Light'
      ),
      'borders' => array(
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('rgb' => '333333')
        ),
        'right' => array(
          'style' => PHPExcel_Style_Border::BORDER_HAIR,
          'color' => array('rgb' => '333333')
        ),
      ),
    );

    $stylerowhead = array(
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font'  => array(
          // 'bold'  => true,
          'color' => array('rgb' => '333333'),
          'size'  => 8,
          'name'  => 'Khmer OS Muol Light'
      ),
      'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('rgb' => '333333')
        )
      ),
      'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'FFFFFF')
      )
    );

    $stylerowhead_en = array(
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font'  => array(
          'bold'  => true,
          'color' => array('rgb' => '333333'),
          'size'  => 9,
          'name'  => 'Times New Roman'
      ),
      'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('rgb' => '333333')
        )
      ),
      'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'FFFFFF')
      )
    );

    $stylecontent = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font'  => array(
          'bold'  => false,
          'size'  =>9,
          'name'  => 'Khmer OS Content'
      ),
      'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('rgb' => '333333')
        )
      )
    );
      
    $objPHPExcel->disconnectWorksheets();
    $objWorkSheet = $objPHPExcel->createSheet(0);
    $objWorkSheet->setTitle("Customer_Feedback_Report");
    $objWorkSheet->getRowDimension('1')->setRowHeight(30);
    $objWorkSheet->getRowDimension('2')->setRowHeight(28);
    $objWorkSheet->setCellValue('A1','គ្រឹះស្ថានមីក្រូហិរញ្ញវត្ថុ ប្រាសាក់ ម.ក | PRASAC MFI PLC.');
    $objWorkSheet->getStyle("A1:S1")->applyFromArray($stylehead1);
    $objWorkSheet->getRowDimension('1')->setRowHeight(20);

    $objWorkSheet->setCellValue('A2','របាយការណ៍ស្តីពីព័ត៌មានត្រលប់របស់អតិថិជន | CUSTOMER FEEDBACK REPORT');
    $objWorkSheet->getStyle("A2:S2")->applyFromArray($stylehead2);
    $objWorkSheet->getRowDimension('2')->setRowHeight(20);
    $objWorkSheet->getRowDimension('3')->setRowHeight(20);

    $objWorkSheet->getStyle("A3:S3")->applyFromArray($stylerowhead);
    $objWorkSheet->getStyle("A4:S4")->applyFromArray($stylerowhead_en);


    $rowCount_kh = 3;
    $rowCount_en = 4;
    $customTitle_kh = array ('លរ','កាលបរិច្ឆេទ','ម៉ោង','ឈ្មោះអតិថិជន','ភេទ','លេខទូរស័ព្ទ','ប្រភេទមតិ/ព័ត៌មាន','ប្រភេទសេវាហិរញ្ញវត្ថុ','បង្ខាំង/ដំណើរការ','ប្រភេទ','បញ្ហា/ព័ត៌មាន','ការឆ្លើយតប/ដំណោះស្រាយ','លទ្ធផល','បង្កដោយ','កូដសាខា','ឈ្មោះសាខា','ភូមិភាគ','តាមរយៈ','អត្តលេខបុគ្គលិក');
    $customTitle_en = array ('Nr.','Date','Time','Customer Name','Sex','Phone Number','Feedback Category','Financial Service','Block/Reset','Category','Issue','Response/Solution','Result','Committed By','Branch Code','Branch Name','Region','Channel','Staff ID');
    $colName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S');
    $i=0;
    $celVal = '';
    foreach ($colName as $col_list){
      $celVal_kh = $col_list.$rowCount_kh;
      $celVal_en = $col_list.$rowCount_en;
      $objWorkSheet->SetCellValue($celVal_kh, $customTitle_kh[$i]);
      $objWorkSheet->SetCellValue($celVal_en, $customTitle_en[$i]);
      $i++;
    }
    $objWorkSheet->getColumnDimension('A')->setWidth(5);
    $objWorkSheet->getColumnDimension('B')->setWidth(12);
    $objWorkSheet->getColumnDimension('C')->setWidth(10);
    $objWorkSheet->getColumnDimension('D')->setWidth(20);
    $objWorkSheet->getColumnDimension('E')->setWidth(6);
    $objWorkSheet->getColumnDimension('F')->setWidth(15);
    $objWorkSheet->getColumnDimension('G')->setWidth(15);
    $objWorkSheet->getColumnDimension('H')->setWidth(25);
    $objWorkSheet->getColumnDimension('I')->setWidth(25);
    $objWorkSheet->getColumnDimension('J')->setWidth(15);
    $objWorkSheet->getColumnDimension('K')->setWidth(60);
    $objWorkSheet->getColumnDimension('L')->setWidth(60);
    $objWorkSheet->getColumnDimension('M')->setWidth(25);
    $objWorkSheet->getColumnDimension('N')->setWidth(20);
    $objWorkSheet->getColumnDimension('O')->setWidth(10);
    $objWorkSheet->getColumnDimension('P')->setWidth(15);
    $objWorkSheet->getColumnDimension('Q')->setWidth(15);
    $objWorkSheet->getColumnDimension('R')->setWidth(30);
    $objWorkSheet->getColumnDimension('S')->setWidth(15);

    // Adding report into the cells
    $customer  = array();

    //Set cell function
    function cellColor($cells,$color,$fillcolor){
      global $objPHPExcel;
      $objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray(array(
        'font'  => array(
             'bold'  => false,
             'color' => array('rgb' => $color),
        ),
        'fill' => array(
          'type' => PHPExcel_Style_Fill::FILL_SOLID,
          'color' => array('argb' =>$fillcolor)
        )
      ));
    }

    if($query_export_result->num_rows){
      $recive_amount_total=0;
      $transfer_amount_total=0;
      $ending_amount_total = 0;
      $n=5;
      $number = 1;
      $gender_list = array(1=>'ប្រុស',2=>'ស្រី');
      while($row = mysqli_fetch_assoc($query_export_result)) {
        $issue_status_id = $row['issue_status'];
        $client_name = $row['client_name'];
        $gender =  $gender_list[$row['gender']];
        $phone = '855'.(int)$row['phone'];
        $category = $row['category_name'];
        $feedback_area = $row['feedback_area'];
        $issue = $row['issue'];
        $staff_id = $row['staff_id'];
        $solution = $row['solution'];
        $area = $row['area'];
        $branch_fc = $row['branch_fc'];
        $issue_status = $row['status_en'];
        $financial_service = $row['financial_service'];
        $service_action = isset($row['service_action'])?$row['service_action']:'N/A';
        $channel = $row['channel'];
        $branch_name = $row['branch_name'];
        $regional_code = $row['regional_code'];
        $regional_name = $row['regional_name'];
        $issuedate = new DateTime($row['issuedate']);
        $issuetime = new DateTime($row['issuedate']);
        $related_position = isset($row['commited_by'])?$row['commited_by']:'';

        $objWorkSheet->SetCellValue('A'.$n,$number);
        $objWorkSheet->SetCellValue('B'.$n,$issuedate->format('d-M-Y'));
        $objWorkSheet->SetCellValue('C'.$n,$issuetime->format('h:i A'));
        $objWorkSheet->SetCellValue('D'.$n,$client_name);
        $objWorkSheet->SetCellValue('E'.$n,$gender);
        $objWorkSheet->setCellValueExplicit('F'.$n,$phone, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue('G'.$n,$category);
        $objWorkSheet->SetCellValue('H'.$n,$financial_service);
        $objWorkSheet->SetCellValue('I'.$n,$service_action);
        $objWorkSheet->SetCellValue('J'.$n,$feedback_area);
        $objWorkSheet->SetCellValue('K'.$n,$issue);
        $objWorkSheet->SetCellValue('L'.$n,$solution);
        $objWorkSheet->SetCellValue('M'.$n,$issue_status);
        $celcolor = '';
        $fillcolor = '';
        switch($issue_status_id){
          case 1:
            $celcolor = '4db848';
            $fillcolor = 'CCFFCC';
            break;
          case 2:
            $celcolor = 'e98008';
            $fillcolor = 'FFFFCC';
            break;
          case 3:
            $celcolor = 'ec0808';
            $fillcolor = 'FFCCCC';
            break;
        }
        cellColor('M'.$n,$celcolor,$fillcolor);
        $objWorkSheet->SetCellValue('N'.$n,$related_position);
        $objWorkSheet->setCellValueExplicit('O'.$n,$branch_fc, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue('P'.$n,$branch_name);
        $objWorkSheet->setCellValueExplicit('Q'.$n,$regional_code, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue('R'.$n,$channel);
        $objWorkSheet->setCellValueExplicit('S'.$n,$staff_id, PHPExcel_Cell_DataType::TYPE_STRING);

        $objWorkSheet->getStyle("A".$n.":S".$n)->applyFromArray($stylecontent);
        // $objWorkSheet->getStyle('A'.$n.':'.'S'.$n)->getAlignment()->setWrapText(true);
        $objWorkSheet->getStyle("A".$n)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $objWorkSheet->getStyle("A".$n)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $objWorkSheet->getStyle("A".$n.":"."S".$n)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));

        $objWorkSheet->getStyle("B".$n)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $objWorkSheet->getStyle("B".$n)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));

        $objWorkSheet->getStyle("E".$n)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $objWorkSheet->getStyle("E".$n)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));

        $objWorkSheet->getStyle("F".$n)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $objWorkSheet->getStyle("F".$n)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));

        $objWorkSheet->getStyle("G".$n)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $objWorkSheet->getStyle("G".$n)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $objWorkSheet->getStyle("K".$n)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $objWorkSheet->getStyle("K".$n)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $objWorkSheet->getStyle("L".$n)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $objWorkSheet->getStyle("L".$n)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));

        $number++;
        $n++;
      }
    }

    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setShowGridlines(false);
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $filenamedate = $date->format("dmYhisa");
    $fullfilename = "Feedback_Report_$filenamedate";
    $filename = $fullfilename.'.xlsx';
    $filename_path = "feedback_report/$filename";
    $objWriter->save($filename_path);
    exit;
?>