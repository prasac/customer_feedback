<?php
  include('session.php');
  if(!isset($_SESSION['login_user'])){
    header("location:index.php");
    exit();
  }
  
  include('header.php');
  $header_text = 'ប្រព័ន្ធគ្រប់គ្រង់សម្ភារៈផ្សព្វផ្សាយ';
  include('head.php');

  if(isset($_POST['btn-pdf'])){
    include('pdf.php');
  }
  if(isset($_POST['btn-excel'])){
    include('excel.php');
  }
  if(isset($_POST['btn-unread']) || isset($_POST['btn-read'])){
    $status = isset($_POST['btn-unread'])?0:1;
    include('setting.php');
  }
  ?>
  <div class="row">
      <nav class="nav-main">
          <div class="nav-wrapper">
              <div class="col s12">
                  <a href="http://feedback.local/" class="breadcrumb">ទំព័រដើម</a>
                  <a href="#" class="breadcrumb">ការកំណត់សាខា</a>
              </div>
          </div>
      </nav>
  </div>
  <div class="row button-wrapper">
    <a class="btn waves-effect btn-success waves-left right" href="material-setting.php">ការកំណត់សម្ភារៈ
        <i class="material-icons left">settings_applications</i>
    </a>
    <a class="btn waves-effect btn-success waves-left right active" href="branch-setting.php" style="margin-left:10px;">ការកំណត់សាខា
        <i class="material-icons left">settings_applications</i>
    </a>
    <a class="btn waves-effect btn-success waves-left right" href="general-setting.php">ការកំណត់ទូរទៅ
        <i class="material-icons left ">settings_applications</i>
    </a>
  </div>
  <?php
    include('branch-list.php');
  ?>
  <?php
  include('footer.php');
  ?>