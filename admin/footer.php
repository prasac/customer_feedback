    <div class="row" style="padding:0;margin-bottom:0;">
        <div class="footer-wrapper">
            <footer class="page-footer">
                <div class="center" style="padding:10px;">
                    © <?php echo date("Y");?> <?= _COPYRIGHT;?>
                    <a class="grey-text text-lighten-4" href="https://www.prasac.com.kh" target="_blank"> &nbsp; www.prasac.com.kh</a>
                </div>
            </footer>
        </div>
    </div>
<style>  
    #loader {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        background: rgba(0, 0, 0, 0.75) url(http://feedback.local/images/Loading.gif) no-repeat center center;
        z-index: 10000;
    }
    /* PULSATING CARET */
    [data-typer]:after {
    content:"";
    display: inline-block;
    vertical-align: middle;
    width:1px;
    height:1em;
    background: #000;
            animation: caretPulsate 1s linear infinite; 
    -webkit-animation: caretPulsate 1s linear infinite; 
    }
    @keyframes caretPulsate {
    0%   {opacity:1;}
    50%  {opacity:1;}
    60%  {opacity:0;}
    100% {opacity:0;}
    }
    @-webkit-keyframes caretPulsate {
    0%   {opacity:1;}
    50%  {opacity:1;}
    60%  {opacity:0;}
    100% {opacity:0;}
    }
</style>
<div id="loader"></div>
        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="http://feedback.local/library/jquery-3.2.1.min.js"></script>
        <!--Import Materialize-Stepper JavaScript -->
        <script src="http://feedback.local/library/materialize-v1.0.0/js/materialize.min.js"></script>
        <script src="http://feedback.local/library/materialize-datetimepicker-master/js/materializedatetimepicker.js"></script>
        <script type="text/javascript" src="http://feedback.local/library/jquery-chained.min.js"></script>
        <script type="text/javascript" src="http://feedback.local/library/jquery-validation/dist/jquery.validate.min.js"></script>
        <script>
            $("#sub_service").chained("#main_service");
            var currYear = (new Date()).getFullYear();

            var DateTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Bangkok"});
            var date = new Date();
            var year = date.getFullYear();
            var month = date.getMonth();
            var day = date.getDate();
            var MyminDate = new Date(year, month-1, day);


            $(document).ready(function() {
                M.AutoInit();
                $('textarea#issue_detail, textarea#solution,#remark').characterCounter();
                $('input#issue_time').timepicker({
                    default: 'now',
                    autoClose:true,
                    twelvehour: false,
                    vibrate: true,
                });
                $(".issue_date").datepicker({
                    selectMonths: true,// Creates a dropdown to control month
                    selectYears: 1, // Creates a dropdown of 15 years to control year
                    format: "yyyy-mm-dd",
                    defaultDate:new Date(),
                    setDefaultDate: true,
                    autoClose : true,
                    minDate:MyminDate,
                    maxDate:new Date(),
                    yearRange: [currYear, currYear],
                });
                
                $("#issue_date_edit").datepicker({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 1, // Creates a dropdown of 15 years to control year
                    format: "yyyy-mm-dd",
                    autoClose : true,
                    minDate:MyminDate,
                    maxDate:new Date(),
                    yearRange: [currYear, currYear],
                });

                $("#search_date_from,#search_date_to").datepicker({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 1, // Creates a dropdown of 15 years to control year
                    format: "yyyy-mm-dd",
                    setDefaultDate: true,
                    minDate: new Date(currYear-3,01,01),
                    maxDate:new Date(),
                    autoClose : true,
                    showClearBtn: true,
                    yearRange: [currYear-2, currYear],
                });
                $("input#search_date").datepicker({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 1, // Creates a dropdown of 15 years to control year
                    format: "yyyy-mm-dd",
                    setDefaultDate: true,
                    minDate: new Date(currYear-3,01,01),
                    maxDate:new Date(),
                    autoClose : true,
                    showClearBtn: true,
                    yearRange: [currYear-2, currYear],
                });
                
                var spinner = $('#loader');
                $('.tooltipped').tooltip();
                $('.sidenav').sidenav();
                $('select').formSelect();
                $('input.select-dropdown').click(function() {
                    $(this).addClass(' active');
                });

                $("td.detail input").attr('readonly', true);
                $("td.detail").click(function(){
                    var $row_id =   $(this).attr('id');
                    $("td.detail#"+$row_id+" input").attr("readonly",false);
                });


                $("#regional").on('change',function(){
                    var $val = $(this).find('option:selected').val();
                    $("#branch option").hide();
                    $("#branch option."+$val).show();
                    $('#branch').formSelect();
                });

                //
                $("table.authentication-form tbody tr").each(function(){
                    var $re_data    =   $(this).find('td:eq(3)>span').text();
                    var $tran_data    =   $(this).find('td:eq(4)>span').text();
                    var $ending_data    =   $(this).find('td:eq(5)>span').text();
                    var $comment_data    =   $(this).find('td:eq(6)>span').text();
                    if(($re_data+$tran_data+$ending_data)>0 || $comment_data != ''){
                        $(this).css('opacity',1);
                    }
                });

                //Initial Spinner
                var spinner = jQuery('#loader');
                $(".btn-export-excel").click(function(e){
                    e.preventDefault();
                    spinner.show();
                    var data = $('form#update-form').serializeArray();
                    $.ajax({
                        type:"POST",
                        url:"../admin/excel.php",
                        cache:false,
                        data:data,
                        success: function(data)
                        {
                            spinner.hide();
                            showToast('<i class="material-icons left">check_circle</i>  របាយការណ៍របស់លោកអ្នកដោនឡូតបានយ៉ាងជោគជ័យ',3000);
                            var a = document.createElement('a');
                            a.href =data;
                            a.download = 'Consolidated_Report_Customer_Feedback_System.xlsx';
                            a.click();
                            a.remove();
                            console.log(data);
                        }
                    });
                });

                //Initial Spinner
                var spinner = jQuery('#loader');
                $("#btn-export-service-checking").click(function(e){
                    e.preventDefault();
                    spinner.show();
                    var data = $('form.search-report-form').serializeArray();
                    $.ajax({
                        type:"POST",
                        url:"../admin/service_quality_check_report.php",
                        cache:false,
                        data:data,
                        success: function(data)
                        {
                            var new_data = data.split(',');
                            console.log(data);
                            spinner.hide();
                            showToast('<i class="material-icons left">check_circle</i>  របាយការណ៍របស់លោកអ្នកដោនឡូតបានយ៉ាងជោគជ័យ',3000);
                            var a = document.createElement('a');
                            a.href = new_data[0];
                            a.download = new_data[1];
                            a.click();
                            a.remove();
                        }
                    });
                });


                // $('select#materials').formSelect();
                $('select.select_all').siblings('ul').prepend('<li id=sm_select_all><span>ជ្រើសរើសទាំងអស់</span></li>');
                $('li#sm_select_all').on('click', function () {
                    var jq_elem = $(this), 
                    jq_elem_span = jq_elem.find('span'),
                    select_all = jq_elem_span.text() == 'ជ្រើសរើសទាំងអស់',
                    set_text = select_all ? 'លុបជម្រើសទាំងអស់' : 'ជ្រើសរើសទាំងអស់';
                    jq_elem_span.text(set_text);
                    jq_elem.siblings('li').filter(function() {
                        return $(this).find('input').prop('checked') != select_all;
                    }).click();
                });

                $("table.list-authen tbody tr td.enable_click").click(function(e){
                    $(this).parent().css('opacity',1);
                    var $row_class = $(this).attr('id');
                    var $spanclass  =  $(this).find('span').attr('class');
                    $("table.list-authen tbody tr td input").hide();
                    $("table.list-authen tbody tr td span").show();
                    $(this).parent().find("span."+$spanclass ).hide();
                    $(this).parent().find("input."+$spanclass ).show();
                });
                // $('select#service_checking_branch,select#service_checking_regional').formSelect();
                // $("#btn_regional_branch_filter").on('click',function(){
                //     $("select#service_checking_regional").last().trigger('click');
                // });

                $("select#service_checking_regional").on('click',function(){
                    $('select#service_checking_branch').formSelect();
                });


                $.validator.addMethod("greaterThan", 
                function(value, element, params) {

                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) >= new Date($(params).val());
                    }

                    return isNaN(value) && isNaN($(params).val()) 
                        || (Number(value) >= Number($(params).val())); 
                },'Must be greater than {0}.');


                $.validator.setDefaults({
                    ignore: []
                });

                $("input#search_branch_fc").keypress(function(e){
                    var id_check_length =   jQuery(this).val().length;
                    var fc_site = jQuery(this).val();
                    if(id_check_length>0){
                        jQuery.ajax({
                            url: "filter-branch.php",
                            type: "POST",
                            data:{'fcsite':fc_site},
                            success: function(response) {
                                var datasource = JSON.parse(response);
                                var dataCountry = {};
                                for (var i = 0; i < datasource.length; i++) {
                                    dataCountry[datasource[i].name_fc] = null; //countryArray[i].flag or null
                                }
                                $('input#search_branch_fc').autocomplete({
                                    data:dataCountry,
                                    limit:5,
                                });
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log(errorThrown);
                            }
                        });
                    }
                });
            });
            /*
            **Showing Toast as HTML
            */
            function showToast(message, duration) {
                M.toast({html:message,duration:duration,class:'rounded'});
            }
        </script>                                                                                                                               
    </body>
</html>