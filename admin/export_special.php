<?php 
    $date = new DateTime();
    $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
    $today_time = $date->format("Y-M-d_h:i");
    $search_client_name = $_POST["search_client_name"]?$_POST["search_client_name"]:'';
    $search_client_phone = $_POST["search_client_phone"]?$_POST["search_client_phone"]:'';
    $search_date = $_POST["search_date"]?$_POST["search_date"]:'';
    $account_status = $_POST["account_status"]?$_POST["account_status"]:'';
    $search_staff_id = $_POST["search_staff_id"]?$_POST["search_staff_id"]:'';

    $json = file_get_contents("https://www.prasac.com.kh/en/special-account-api?action=load&client_name=$search_client_name&client_phone=$search_client_phone&booking_date=$search_date&account_status=$account_status&staff_id=$search_staff_id");
    $obj = json_decode($json);

    if(count($obj)>0){
      //=====Call PHPExcel Library===
      include('../library/PHPExcel/Classes/PHPExcel.php');
      $objPHPExcel = new PHPExcel();
      $objPHPExcel->disconnectWorksheets();
      $objWorkSheet = $objPHPExcel->createSheet(0);
      $objWorkSheet->setTitle("របាយការណ៍គណនីលេខពិសេស");
      $objPHPExcel->getProperties()->setCreator("ហម គឹមហ៊ីម")
                  ->setLastModifiedBy("ហម គឹមហ៊ីម")->setTitle("របាយការណ៍គណនីលេខពិសេស")
                  ->setSubject("របាយការណ៍គណនីលេខពិសេស")->setDescription("របាយការណ៍គណនីលេខពិសេស")
                  ->setKeywords("របាយការណ៍គណនីលេខពិសេស")->setCategory("របាយការណ៍គណនីលេខពិសេស");

      $stylehead1 = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ),
        'font'  => array(
            'color' => array('rgb' => '117515'),
            'size'  =>9,
            'name'  => 'Khmer OS Muol Light',
        ),
        'borders' => array(
          'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('rgb' => '333333')
          ),
        ),
      );
      $stylehead2 = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ),
        'font'  => array(
            'color' => array('rgb' => 'b78311'),
            'size'  => 9,
            'name'  => 'Khmer OS Muol Light'
        ),
        'borders' => array(
          'bottom' => array(
              'style' => PHPExcel_Style_Border::BORDER_HAIR,
              'color' => array('rgb' => '333333')
          ),
          'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('rgb' => '333333')
          ),
        ),
      );
  
      $stylerowhead = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ),
        'font'  => array(
            // 'bold'  => true,
            'color' => array('rgb' => '333333'),
            'size'  => 8,
            'name'  => 'Khmer OS Muol Light'
        ),
        'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_HAIR,
              'color' => array('rgb' => '333333')
          )
        ),
        'fill' => array(
          'type' => PHPExcel_Style_Fill::FILL_SOLID,
          'color' => array('rgb' => 'FFFFFF')
        )
      );
  
      $stylerowhead_en = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ),
        'font'  => array(
            'bold'  => true,
            'color' => array('rgb' => '333333'),
            'size'  => 9,
            'name'  => 'Times New Roman'
        ),
        'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_HAIR,
              'color' => array('rgb' => '333333')
          )
        ),
        'fill' => array(
          'type' => PHPExcel_Style_Fill::FILL_SOLID,
          'color' => array('rgb' => 'FFFFFF')
        )
      );
  
      $stylecontent = array(
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ),
        'font'  => array(
            'bold'  => false,
            'size'  =>9,
            'name'  => 'Khmer OS Content'
        ),
        'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_HAIR,
              'color' => array('rgb' => '333333')
          )
        )
      );
      $stylealignleft = array(
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ),
        'font'  => array(
            'bold'  => false,
            'size'  =>9,
            'name'  => 'Khmer OS Content'
        ),
        'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_HAIR,
              'color' => array('rgb' => '333333')
          )
        )
      );

      
      $n=5;
      $number = 1;
      
      $objWorkSheet->getColumnDimension('A')->setWidth(5);
      $objWorkSheet->getColumnDimension('B')->setWidth(12);
      $objWorkSheet->getColumnDimension('C')->setWidth(10);
      $objWorkSheet->getColumnDimension('D')->setWidth(20);
      $objWorkSheet->getColumnDimension('E')->setWidth(20);
      $objWorkSheet->getColumnDimension('F')->setWidth(25);
      $objWorkSheet->getColumnDimension('G')->setWidth(25);
      $objWorkSheet->getColumnDimension('H')->setWidth(15);
      $objWorkSheet->getColumnDimension('I')->setWidth(15);
      $objWorkSheet->getColumnDimension('J')->setWidth(50);
      $objWorkSheet->getColumnDimension('K')->setWidth(15);

      $objWorkSheet->getRowDimension('1')->setRowHeight(30);
      $objWorkSheet->getRowDimension('2')->setRowHeight(28);
      $objWorkSheet->setCellValue('A1','គ្រឹះស្ថានមីក្រូហិរញ្ញវត្ថុ ប្រាសាក់ ម.ក | PRASAC MFI PLC.');
      $objWorkSheet->getStyle("A1:K1")->applyFromArray($stylehead1);
      $objWorkSheet->getRowDimension('1')->setRowHeight(20);

      $objWorkSheet->setCellValue('A2','របាយការណ៍កក់លេខគណនីពិសេសតាមរយៈវេបសាយត៍ | SPECIAL ACCOUNT NUMBER BOOKING REPORT');
      $objWorkSheet->getStyle("A2:K2")->applyFromArray($stylehead2);
      $objWorkSheet->getRowDimension('2')->setRowHeight(20);
      $objWorkSheet->getRowDimension('3')->setRowHeight(20);

      $objWorkSheet->getStyle("A3:K3")->applyFromArray($stylerowhead);
      $objWorkSheet->getStyle("A4:K4")->applyFromArray($stylerowhead_en);

      $rowCount_kh = 3;
      $rowCount_en = 4;
      $customTitle_kh = array ('លរ','កាលបរិច្ឆេទ','ម៉ោង','ឈ្មោះអតិថិជន','លេខទូរស័ព្ទ','លេខគណនី','តម្លៃ(ដុល្លារ)','កូដសាខា','ស្ថានភាព','បរិយាយ','អត្តលេខបុគ្គលិក');
      $customTitle_en = array ('Nr.','Date','Time','Customer Name','Phone Number','Account Number','Account Price(USD)','Branch Code','Account Status','Remark','Staff ID');
      $colName = array('A','B','C','D','E','F','G','H','I','J','K');
      $i=0;
      $celVal = '';

      foreach ($colName as $col_list){
        $celVal_kh = $col_list.$rowCount_kh;
        $celVal_en = $col_list.$rowCount_en;
        $objWorkSheet->SetCellValue($celVal_kh, $customTitle_kh[$i]);
        $objWorkSheet->SetCellValue($celVal_en, $customTitle_en[$i]);
        $i++;
      }

      
      //Set cell function
      function cellColor($cells,$color,$fillcolor){
        global $objPHPExcel;
        $objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray(array(
          'font'  => array(
              'bold'  => false,
              'color' => array('rgb' => $color),
          ),
          'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' =>$fillcolor)
          )
        ));
      }


      foreach($obj as $item) {
        $status_class = 'done';
        $icon_status  = 'check_circle';
        $status_title = '';
        switch($item->status){
          case 1:
            $status_class = 'pending';
            $icon_status  = 'info';
            $status_title = 'មិនទាន់បើកគណនី';
            break;
          case 2:
            $status_class = 'done';
            $icon_status  = 'check_circle';
            $status_title = 'បានបើកគណនីរួច';
            break;
          case 3:
            $status_class = 'fail';
            $icon_status  = 'cancel';
            $status_title = 'មិនបើកគណនី';
            break;
        }

        // $status_title_icon = '<i style="float:left;margin-top:0px;" alt="" class="material-icons '.$status_class.'">'.$icon_status.'</i>&nbsp;';
        

        $client_name = $item->client_name;
        $client_phone = $item->client_phone;
        $account_number = $item->account_number;
        $account_price = $item->account_price;
        $branch_id = $item->branch_id;
        $staff_id  = $item->staff_id;
        $booking_date = new DateTime($item->created_date);
        $remark = $item->remark;

        $objWorkSheet->SetCellValue('A'.$n,$number);
        $objWorkSheet->SetCellValue('B'.$n,$booking_date->format('d-M-Y'));
        $objWorkSheet->SetCellValue('C'.$n,$booking_date->format('h:i A'));
        $objWorkSheet->SetCellValue('D'.$n,$client_name);
        $objWorkSheet->setCellValueExplicit('E'.$n,$client_phone, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->setCellValueExplicit('F'.$n,$account_number, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->setCellValueExplicit('G'.$n,$account_price, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->setCellValueExplicit('H'.$n,$branch_id, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->setCellValueExplicit('I'.$n,$status_title, PHPExcel_Cell_DataType::TYPE_STRING);
        $celcolor = '';
        $fillcolor = '';
        switch($item->status){
          case 2:
            $celcolor = '4db848';
            $fillcolor = 'CCFFCC';
            break;
          case 1:
            $celcolor = 'e98008';
            $fillcolor = 'FFFFCC';
            break;
          case 3:
            $celcolor = 'ec0808';
            $fillcolor = 'FFCCCC';
            break;
        }

        cellColor('I'.$n,$celcolor,$fillcolor);
        $objWorkSheet->setCellValueExplicit('J'.$n,$remark, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->setCellValueExplicit('K'.$n,$staff_id, PHPExcel_Cell_DataType::TYPE_STRING);
       
        
        $objWorkSheet->getStyle("A".$n.":K".$n)->applyFromArray($stylecontent);
        $objWorkSheet->getStyle("J".$n.":J".$n)->applyFromArray($stylealignleft);
        $number++;
        $n++;
      }
    }
    
    
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setShowGridlines(false);
    // $objPHPExcel->getActiveSheet()->getStyle('K1:K'.$objPHPExcel->getActiveSheet()->getHighestRow())
    // ->getAlignment()->setWrapText(true); 
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $filename= "SpecialAccountReport.xlsx";
    $filename_path = "../reports/$filename";
    $objWriter->save($filename_path);
    echo $filename_path;
    exit;
?>