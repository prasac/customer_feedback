<?php 
    $login_session = isset($_SESSION['login_user'])?$_SESSION['login_user']:'';
?>
<div class="container">
    <div class="row" style="padding: 0 -15px;">
        <nav class="nav" role="navigation">
            <div class="nav-wrapper">
                <a style="padding:0 10px;color:red;font-weight:bold;" href="http://feedback.prasac.localadmin/logout.php" class="right" alt="ចុច​ទីនេះ​ដើម្បី​ចាក​ចេញ​" title="ចុច​ទីនេះ​ដើម្បី​ចាក​ចេញ​" data-position="top" data-delay="50" data-tooltip="ចុច​ទីនេះ​ដើម្បី​ចាក​ចេញ"><i class="material-icons right" style="margin-left:5px;">exit_to_app</i>ចាកចេញ</a>
                <?php
                if(isset($_SESSION['login_user']) && ($_SESSION['login_id'] ==2)){
                ?>
                    <a href="#" class="right">|</a>
                    <a  style="padding:0 10px;" class='right profile-seperate' href='http://feedback.prasac.localauth/profile.php'>ព័ត៌មានគណនី</a>
                <?php 
                }
                ?>
            </div>
        </nav>
    </div>
    <div class="row">
        <a id="logo-container" href="http://feedback.prasac.local" class="brand-logo col s5 l5">
            <img src="http://feedback.local/images/logo-website.png" style="width:100px;text-align:left;margin:0 10px;"/>
            <?php echo '<h2>'.$header_text.'</h2>';?>
        </a>
        <ul class="hide-on-med-and-down welcome-message col s7 l7 text-right">
            <li>ស្វាគមន៍ :  <span style="font-size:1.5em;">&nbsp;<?php echo $login_session;?></span></li>
        </ul>
    </div>