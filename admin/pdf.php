<?php 
  // error_reporting(0);
  $items_checked = isset($_POST['checkboxitem'])?$_POST['checkboxitem']:'';
  if($items_checked){
    $items_checked = implode(',',$items_checked);
    $mpdf_path =  '../library/mpdf/mpdf.php';
    require_once($mpdf_path);
    $mpdf = new mPDF('s',    // mode - default ''
    'A4',    // format - A4, for example, default ''
    '',     // font size - default 0
    '',    // default font family
    8,    // 15 margin_left
    8,    // 15 margin right
    8,     // 16 margin top
    8,    // margin bottom
    8,     // 9 margin header
    8,     // 9 margin footer
    'P');
    // $mpdf->debug = true;
    $mpdf->allow_charset_conversion = true;
    $mpdf->useActiveForms = true;
    $mpdf->formUseZapD = false;
    $mpdf->form_border_color = '0.6 0.6 0.72';
    $mpdf->form_button_border_width = '2';
    $mpdf->form_button_border_style = 'S';
    $mpdf->form_radio_color = '0.0 0.0 0.4'; // radio and checkbox
    $mpdf->form_radio_background_color = '0.9 0.9 0.9';
    if($service_type=='deposit'){
      include('pdf-deposit.php');
    }else{
      include('pdf-loan.php');
    }
    ob_clean();
    $mpdf->useSubstitutions = false; 
    $mpdf->simpleTables = true;
    $mpdf->SetTitle('Customer Account Opening Application');
    $mpdf->Output('customer_account_opening.pdf', 'I');
    $message = '<span style="color:#4DB848;">Data have been successful exported.</span>';
  }else{
    $message = '<span style="color:#ee6e73;">No item selected to export.</span>';
  }
?>