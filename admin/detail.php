<?php
  include('session.php');
  if(!isset($_SESSION['login_user'])){
    header("location:index.php");
    exit();
  }
  include('header.php');
  $header_text = 'ប្រព័ន្ធគ្រប់គ្រងសំណើប្រាក់កម្ចី';
  $service_type = 'loan';
  if((isset($_POST['btn-select']) || isset($_POST['service_type'])) && $_POST['service_type']=='deposit'){
    $header_text = 'ប្រព័ន្ធគ្រប់គ្រងសំណើប្រាក់បញ្ញើសន្សំ';
    $service_type = 'deposit';
  }
  include('head.php');
  if(isset($_POST['btn-pdf'])){
    include('pdf.php');
  }
  $id = base64_decode($_GET['id']);
  $query = "SELECT 
    c.id,c.customer_gender,c.customer_dob,c.customer_personal_identify,c.customer_personal_number,c.payment_method,c.borrow_duration,
    c.customer_name,FORMAT(c.borrow_amount,2) as borrow_amount,c.brrow_rate_request_permonth,c.customer_address,c.customer_street,c.customer_group,v.name_kh as customer_village,
    c.customer_phone,c.customer_status,co.name_kh as customer_commune,d.name_kh as customer_district,pv.name_kh as customer_province,c.business_type,c.business_purpose,
    b.name_kh as branch_name,c.created_date
    FROM loan_customer AS c 
    left join loan_purpose AS p ON c.id=p.customer_id 
    left join villages as v on c.customer_village=v.id
    left join communes as co on c.customer_commune=co.id 
    left join districts as d on c.customer_district=d.id 
    left join provinces as pv on c.customer_province=pv.id
    left join branches as b on c.duty_station=b.id
    where c.id=$id
    ORDER BY c.id ASC limit 1";
    $result = $conn->query($query);
    if($result->num_rows){
        $conn->query("update loan_customer set is_view=1 where id=$id");
        $businessType = array('កសិកម្ម​','ពាណិជ្ជកម្ម/ជំនួញ','សេវាកម្ម​​','ដឹកជញ្ជួន','សាងសង់','ប្រើប្រាស់​ទូទៅក្នុងគ្រួសារ');
        $businessPurpose = array('​ឯកកម្ម​សិទ្ធិ','សហកម្មសិទ្ធិ','ពង្រីកមុខជំនួញ','ចាប់​ផ្ដើម​មុខ​ជំនួញ');
        $personal_identify = array(1=>'​សៀវភៅ​គ្រួសារ​​',2=>'អត្ត​សញ្ញាណ​ប័ណ្ណ​',3=>'លិខិត​ឆ្លង​ដែន​',4=>'សំបុត្រ​កំណើត​',5=>'ប័ណ្ណ​បើកបរ​');
        $payment_method = array(1=>'ប្រ​ចាំ​ខែ​',2=>'ប្រចាំ​ត្រី​មាស​',3=>'ប្រ​ចាំ​ឆមាស​',4=>'ប្រចាំ​ឆ្នាំ​');

        $row = mysqli_fetch_assoc($result);
     
        $id = $row['id'];
        $customer_name = $row['customer_name'];
        $customer_gender = ($row['customer_gender'] && $row['customer_gender']==1)?'ប្រុស':'ស្រី';
        $customer_dob = date('d-m-Y',strtotime($row['customer_dob']));
        $customer_personal_identify = $personal_identify[$row['customer_personal_identify']];
        $customer_personal_number = $row['customer_personal_number'];
        $customer_phone = $row['customer_phone'];
        $borrow_amount = $row['borrow_amount'];
        $customer_address = $row['customer_address'];
        $customer_street = $row['customer_street'];
        $customer_group = $row['customer_group'];
        $customer_village = $row['customer_village'];
        $customer_commune = $row['customer_commune'];
        $customer_district = $row['customer_district'];
        $customer_province = $row['customer_province'];
        $business_purpose = $row['business_purpose'];
        $branch_name = $row['branch_name'];
        $payment_request_method = $payment_method[$row['payment_method']];
        $brrow_rate_request_permonth = $row['brrow_rate_request_permonth'];
        $borrow_duration = $row['borrow_duration'];
        $customer_full_addresss = 'ផ្ទះ​លេខ​ '.$customer_address.'ផ្លូវលេខ​ '.$customer_street.' ក្រុម​ទី'.$customer_group.' ភូមិ​ '.$customer_village.' ​ឃុំ/សង្កាត់ '.$customer_commune.' ស្រុក/ខណ្ឌ​ '.$customer_district.' ខេត្ត​/ក្រុង '.$customer_province;
    ?> 
    <div class="row">
      <div class="col s12 m8 offset-m2 l8 offset-l2">
        <div class="card blue-grey darken-1">
          <div class="card-content white-text">
            <div class="card-title centered">ព័ត៌មាន​លម្អិត​របស់​អតិថិជន</div>
            <table>
              <tbody>
                <tr>
                  <td class="label">ឈ្មោះអតិថិជន</td>
                  <td class="data"><?php echo $customer_name;?></td>
                  <td class="label">ភេទ</td>
                  <td class="data"><?php echo $customer_gender;?></td>
                </tr>
                <tr>
                  <td class="label">លេខ​ទូរស័ព្ទ</td>
                  <td class="data"><?php echo $customer_phone;?></td>
                  <td class="label">ទំហំ​ប្រាក់​កម្ចី</td>
                  <td class="data"><?php echo $borrow_amount;?>&nbsp;ដុល្លាអាមេរិក</td>
                </tr>
                <tr>
                  <td class="label">ថ្ងៃ​ខែ​ស្នើសុំ</td>
                  <td class="data"><?php echo $row['created_date'];?></td>
                  <td class="label">របៀបសង​</td>
                  <td class="data"> ​<?php echo $payment_request_method;?></td>
                </tr>
                <tr>
                  <td class="label">អត្រាការប្រាក់​ស្នើសុំ</td>
                  <td class="data"><?php echo $brrow_rate_request_permonth;?> % ក្នុង​មួយ​ឆ្នាំ​</td>
                  <td class="label">រយៈពេល​ខ្ចី​</td>
                  <td class="data"> ​<?php echo $borrow_duration;?> ខែ​</td>
                </tr>
                
                <tr>
                  <td class="label">ឯកសារ​សំគាល់​អត្ត​សញ្ញាណ</td>
                  <td class="data"><?php echo $customer_personal_identify;?></td>
                  <td class="label">ឯកសារ​សំគាល់​អត្ត​សញ្ញាណលេខ​</td>
                  <td class="data"> ​<?php echo $customer_personal_number;?> ខែ​</td>
                </tr>

                <tr>
                  <td class="label">អសយដ្ឋាន</td>
                  <td class="data" colspan="3"><?php echo $customer_full_addresss;?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-action">
            <a href="list.php" class="left"><i class="fa fa-arrow-left fa-1x" aria-hidden="true"></i>&nbsp;ត្រលប់​ក្រោយ</a>
            <span class="left"><a>ទាញយក​ទិន្នន័យជា៖</a></span>
            <form action="" method="POST">
              <input type="hidden" value="<?php echo $id?>" name="checkboxitem[]" />
              <button type="submit" name="btn-pdf" class="left tooltipped" style="padding:0 8px;background:transparent;border:none;" data-delay="30" data-tooltip="ទាញយក​ទិន្នន័យជា​ PDF"><i class="fa fa-file-pdf-o fa-2x btn-pdf" aria-hidden="true"></i></button> 
            </form>
            <br />
          </div>
        </div>
      </div>
      </div>
    </div>
    <?php
    }
    include('footer.php');
  ?>