<?php
  include('session.php');
  if(!isset($_SESSION['login_user'])){
    header("location:http://feedback.prasac.local");
  }else if(isset($_SESSION['login_user']) && ($_SESSION['login_id'] ==2)){ // if user already logged in with admin user
    header("location:http://feedback.local/auth/index.php");//redirect to admin list page
  }else{
    include('header.php');
    $header_text = 'ប្រព័ន្ធគ្រប់គ្រង់សម្ភារៈផ្សព្វផ្សាយ';
    include('head.php');
    ?>
  <div class="row">
    <div class="col s12 l12 form-wrapper">
      <form class="form search" action="" method="POST" id="admin_posm_form">
        <br />
        <div class="col s2 l2 left"></div>
        <div class="col s2 l2 left">
          <select name="regional" id="regionals" required >
            <?php
            $query_branch = $conn->query("select id,name from tbl_regional order by id ");
            $regional_posted = isset($_POST['regional'])?$_POST['regional']:false;
              $i=0;
              $branch_id  = $_SESSION['login_branch_id'];
              echo '<option value="0" selected >ទាំងអស់</option>';
              while($row = mysqli_fetch_object($query_branch)) {
                if($regional_posted && $regional_posted==$row->id){
                  echo '<option value="'.$row->id.'" selected>'.$row->name.'</option>';
                }else{
                  echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                }
                $i++;
              }
            ?>
          </select>
          <label><b>សូមជ្រើសរើសភូមិភាគ</b></label>
        </div>
        <div class="col s4 l4 left">
          <select name="materials[]" id="materials" multiple class="select_all">
            <?php
            $query_material = $conn->query("select id,title_kh from materials where status=1 order by id ");
            $material_posted = isset($_POST['materials'])?$_POST['materials']:false;
              $i=0;
              // echo '<option value="0" disabled>ទាំងអស់</option>';
              while($row = mysqli_fetch_object($query_material)) {
                if($material_posted && in_array($row->id, $material_posted)){
                  echo '<option value="'.$row->id.'" selected>'.$row->title_kh.'</option>';
                }else{
                  echo '<option value="'.$row->id.'">'.$row->title_kh.'</option>';
                }
                $i++;
              }
            ?>
          </select>
          <label><b>សូមជ្រើសរើសសម្ភារៈ</b></label>
        </div>
        <div class="col s6 l1 left">
          <select name="month" id="month" >
              <?php 
                $months = array(1 => 'មករា', 2 => 'កុម្ភៈ', 3 => 'មីនា', 4 => 'មេសា', 5 => 'ឧសភា', 6 => 'មិថុនា', 7 => 'កក្កដា', 8 => 'សីហា', 9 => 'កញ្ញា', 10 => 'តុលា', 11 => 'វិច្ឆិកា', 12 => 'ធ្នូ');
                $transposed = array_slice($months, date('n'), 12, true) + array_slice($months, 0, date('n'), true);
                $last8 = array_reverse(array_slice($transposed, -8, 12, true), true);
                foreach ($months as $num => $name) {
                  if($num <= (int)date('m')){
                    if((isset($_POST['month']) && $_POST['month']== $num) || (!isset($_POST['month']) && $num == (int)date('m'))){
                      echo "<option value='".$num."' selected>$name</option>";
                    }else{
                      echo "<option value='".$num."'>$name</option>";
                    }
                  }else{
                    echo "<option value='".$num."' disabled>$name</option>";
                  }
                }
              ?>
          </select>
          <label><b>សូមជ្រើសរើសខែ</b></label>
        </div>
        <div class="col s6 l1 left">
          <select name="year" id="year" required="" aria-required="true">
          <?php 
            $date_end =date("Y")+2;
            $date_start= date("Y") - 2;
            $year = 0;
            for($date_start;$date_start<=$date_end;$date_start++){
                $finalyear= $date_start-$year;
                if((int)date('Y')==$finalyear){
                  echo "<option value='".$finalyear."' selected>$finalyear</option>";
                }else{
                  echo "<option value='".$finalyear."' disabled>$finalyear</option>";
                }
            }
          ?>
          </select>
          <label><b>សូមជ្រើសរើសឆ្នាំ</b></label>
        </div>
        <div class="col s6 l2 left">
            <button class="btn btn-success waves-effect waves-light " id="btn_sumbit_click_admin" type="submit" name="btn-submit"><i class="material-icons left">add_circle_outline</i>បង្ហាញ​ទិន្នន័យ</button>
            <label></label>
        </div>
      </div>
      <?php 
      if(isset($_POST['btn-submit'])){?>
        <div class="col s12 m12 l12" style="background-color:transparent;box-shadow:none;text-align:center;">
          <div class="card-content green-text">
            <?php
            $regional_list = array(1=>'ការិយាល័យកណ្ដាល',2=>'នាយកដ្ឋានរដ្ឋបាលការិយាល័យកណ្ដាល',3=>'ភូមិភាគ១',4=>'ភូមិភាគ២',5=>'ភូមិភាគ៣',6=>'ភូមិភាគ៤',7=>'ភូមិភាគ៥',8=>'ភូមិភាគ៦', 9=>'ភូមិភាគ៧',10=>'ភូមិភាគ៨',11=>'ភូមិភាគ9',12=>'ភូមិភាគ10',13=>'ភូមិភាគ11',14=>'ភូមិភាគ12',15=>'ភូមិភាគ13');
            $posted_regional_id= $_POST['regional'];
            $selected_month = (int)date('m');
            $selected_year = (int)date('Y');
            $posted_materials  = isset($_POST['materials'])?implode(',',$_POST['materials']):'0';
            $selected_month = $months[$_POST['month']];
            
            $query  = "select title_kh from materials";
            if($posted_materials != 0){ // if select one regional and all branch
              $query  = "select title_kh from materials where id in($posted_materials)";
            }
            $query_material = $conn->query($query);
            $result_material = []; //this is where a new array is created
            while($postsData = mysqli_fetch_array($query_material)) {
                $result_material[] = $postsData['title_kh'];
            }
            $branch_post_list = implode(' ',$result_material);
            ?>
            <p class="info">
              <strong style="font-weight: bold;">លទ្ធផលរបាយការណ៍សម្រាប់៖</strong>
              <?php echo $posted_regional_id>0?$regional_list[$posted_regional_id]:'គ្រប់ភូមិភាគ';?>
              <?php echo $posted_materials?'<b>សម្ភារៈ</b> '.$branch_post_list:'គ្រប់សម្ភារៈទាំងអស់';?>
              <?php echo '<b>ខែ</b> '.$selected_month; ?>
              <?php echo '<b>ឆ្នាំ</b> '.$selected_year; ?>
            </p>
          </div>
        </div>
      <?php 
      }
      echo isset($message)?'<p>'.$message.'</p>':'';
      include('posm-list.php');
    ?>
    </form>
  </div>
  <?php
  include('footer.php');
  }
  ?>