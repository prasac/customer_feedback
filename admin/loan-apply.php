<?php
include("header.php");
?>
<!-- 
Materializecss Stepper v1.1 - Igor Marcossi
https://github.com/Kinark/Materialize-stepper 
-->
<div class="section grey lighten-5">
   <div class="container">
      <div class="row">
         <div class="col l6 m10 s12 offset-l3 offset-m1">
            <h3 class="light center-align blue-text">Sign up form</h3>
            <div class="card">
               <div class="card-content">

                  <ul data-method="GET" class="stepper horizontal">
                     <li class="step active">
                        <div class="step-title waves-effect waves-dark">E-mail</div>
                        <div class="step-content">
                           <div class="row">
                              <div class="input-field col s6">
                                 <input id="email" name="email" type="email" class="validate" required>
                                 <label for="first_name">Your e-mail</label>
                              </div>
                           </div>
                           <div class="step-actions">
                              <button class="waves-effect waves-dark btn next-step" data-feedback="anyThing">Continue</button>
                           </div>
                        </div>
                     </li>
                     <li class="step">
                        <div class="step-title waves-effect waves-dark">Step 2</div>
                        <div class="step-content">
                           <div class="row">
                              <div class="input-field col s6">
                                 <input id="password" name="password" type="password" class="validate" required>
                                 <label for="password">Your password</label>
                              </div>
                           </div>
                           <div class="step-actions">
                              <button class="waves-effect waves-dark btn next-step">CONTINUE</button>
                              <button class="waves-effect waves-dark btn-flat previous-step">BACK</button>
                           </div>
                        </div>
                     </li>
                     <li class="step">
                        <div class="step-title waves-effect waves-dark">Callback</div>
                        <div class="step-content">
                          Unfortunately callback loading screen is not working in here =( I'll make a github page soon!
                           <div class="step-actions">
                              <button class="waves-effect waves-dark btn next-step" data-feedback="anyThing">ENDLESS CALLBACK!</button>
                           </div>
                        </div>
                     </li>
                  </ul>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>