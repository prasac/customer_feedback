<?php 
$query = "SELECT 
c.id,c.customer_gender,c.customer_dob,c.customer_personal_identify,c.customer_personal_number,c.borrow_amount_character,c.borrow_duration,c.payment_method,
c.customer_name,FORMAT(c.borrow_amount,2) as borrow_amount,c.brrow_rate_request_permonth,c.customer_address,c.customer_street,c.customer_group,v.name_kh as customer_village,
c.customer_phone,c.customer_status,co.name_kh as customer_commune,d.name_kh as customer_district,pv.name_kh as customer_province,c.business_type,c.business_purpose,
b.name_kh as branch_name,c.business_location as business_location
 FROM loan_customer AS c 
 left join villages as v on c.customer_village=v.id
 left join communes as co on c.customer_commune=co.id 
 left join districts as d on c.customer_district=d.id 
 left join provinces as pv on c.customer_province=pv.id
 left join branches as b on c.duty_station=b.id
 where c.id in($items_checked)
 ORDER BY c.id ASC ";
$result = $conn->query($query);
$customer  = array();

$businessType = array('កសិកម្ម​','ពាណិជ្ជកម្ម/ជំនួញ','សេវាកម្ម​​','ដឹកជញ្ជួន','សាងសង់','ប្រើប្រាស់​ទូទៅក្នុងគ្រួសារ');
$businessPurpose = array('​ឯកកម្ម​សិទ្ធិ','សហកម្មសិទ្ធិ','ពង្រីកមុខជំនួញ','ចាប់​ផ្ដើម​មុខ​ជំនួញ');
$personal_identify = array(1=>'​សៀវភៅ​គ្រួសារ​​',2=>'អត្ត​សញ្ញាណ​ប័ណ្ណ​',3=>'លិខិត​ឆ្លង​ដែន​',4=>'សំបុត្រ​កំណើត​',5=>'ប័ណ្ណ​បើកបរ​');
$relationship = array(1=>'ប្ដី',2=>'ប្រពន្ធ',3=>'បង',4=>'ប្អូន',5=>'ម្ដាយ',6=>'ឪពុក',7=>'ផ្សេងៗ');
$payment_method = array(1=>'ប្រ​ចាំ​ខែ​',2=>'ប្រចាំ​ត្រី​មាស​',3=>'ប្រ​ចាំ​ឆមាស​',4=>'ប្រចាំ​ឆ្នាំ​');

while($row = mysqli_fetch_assoc($result)) {
 $id = $row['id'];
 $customer_name = $row['customer_name'];
 $customer_gender = ($row['customer_gender'] && $row['customer_gender']==1)?'ប្រុស':'ស្រី';
 $customer_dob = date('d-m-Y',strtotime($row['customer_dob']));
 $customer_personal_identify = $personal_identify[$row['customer_personal_identify']];
 $customer_personal_number = $row['customer_personal_number'];
 $customer_phone = $row['customer_phone'];
 $borrow_amount = $row['borrow_amount'];
 $customer_address = $row['customer_address'];
 $customer_street = $row['customer_street'];
 $customer_group = $row['customer_group'];
 $customer_village = $row['customer_village'];
 $customer_commune = $row['customer_commune'];
 $customer_district = $row['customer_district'];
 $customer_province = $row['customer_province'];
 $business_purpose = $row['business_purpose'];
 $business_location = $row['business_location'];
 $business_type = $row['business_type'];
 $borrow_amount = $row['borrow_amount'];
 $borrow_amount_character = $row['borrow_amount_character'];
 $borrow_duration = $row['borrow_duration'];
 $brrow_rate_request_permonth = $row['brrow_rate_request_permonth'];
 $branch_name = $row['branch_name'];
 $payment_request_method = $payment_method[$row['payment_method']];

 $mpdf->AddPage('P', // L - landscape, P - portrait 
 '', '', '',
 8, // margin_left
 8, // margin right
 8, // margin top
 8, // margin bottom
 8, // margin header
 8); // margin footer 
 //echo $items_checked[$i];
 $mpdf->SetHTMLFooter('<div class="pdf-footer"><div class="footer-right">ពាក្យ​សុំ​ខ្ចី​ចងការប្រាក់​</div><div class="footer-left">Form: LAF-4-V8</div></div>');
 $html = '
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <body>
     <head>
     </head>
     <body>
     <style>
     html,body{
       font-family:hanuman,Verdana;
       font-size:13px;
     }
     h1.khmer{
       font-family:"khmerokmoul";
       font-weight:lighter;
       font-size:1.1em;
       line-height:1.8em;
     }
     strong.khmer{
       font-family:"khmerokmoul";
       font-weight:lighter;
       font-size:1.1em;
       line-height:1.8em;
     }
     h1.khmer.center{
       font-family:"khmerokmoul";
       font-weight:lighter;
       font-size:1.1em;
       text-align:center;
       line-height:1.8em;
     }

     h1.underline{
       text-decoration:underline;
     }
     div.right{
       text-align:right;
       float:right;
       margin-top:-2.5em;
     }
     .right{
       text-align:right;
       float:right;

     }
     .center,strong.center,center.center{
       text-align:center;
     }
     p{
       line-height:1.8em;
       font-size:12px;
     }
     p.khmer,span.khmer{
       font-family:"hanuman";
       font-size:13px;
       text-lign:justify;
     }
     table{
       border-collapse:collapse;
     }
     table thead tr th{
         valign:middle;
         padding:5px;
         font-weight:bold;
     }
     table tbody tr td{
       valign:middle;
       padding:5px;
     }
     table tbody tr td.center{
       text-align:center;
     }
     .pdf-footer{
       font-size:10px;
     }
     .footer-left{
       float:left;
       text-align:left;
       width:40%;
     }
     .footer-right{
       float:right;
       text-align:right;
       font-family:"hanuman";
       width:40%;
     }
     p.khmer.content{
       line-height:2;
       text-align:justify !important;
       width:100%;
     }

     </style>
         <h1 class="khmer center">
           ព្រះ​រាជាណាចក្រ​កម្ពុជា <br />
           ជាតិ សាសនា ព្រះមហាក្សត្រ
           <br /><br />
           ពាក្យ​សុំ​ខ្ចី​ចងការ​ប្រាក់​
         </h1>
         <form>
         <div class="khmer right">លេខ​សំណើ​កម្ចីៈ..............................</div>
         <p class="khmer content">
         អ្នក​សុំ​ខ្ចី​ប្រាក់​ឈ្មោះ​&nbsp;<b>'.$customer_name.'</b>&nbsp;ភេទ&nbsp;<b>'.$customer_gender.'</b>&nbsp;ថ្ងៃ-ខែ-ឆ្នាំកំណើត&nbsp;<b>'.$customer_dob.'</b>&nbsp;សញ្ជាតិ​ខ្មែរ​ ឯកសារ​សម្គាល់​អត្តសញ្ញាណៈ​
         &nbsp;<b>'.$customer_personal_identify.'</b>&nbsp;លេខៈ <b>'.$customer_personal_number.'</b>&nbsp;';
         
         $query_referent= "select * from loan_referent where customer_id=$id";
         $query_re_result = $conn->query($query_referent);
         if($query_re_result->num_rows){
           $i = 1;
           while($row = mysqli_fetch_object($query_re_result)) {
               $re_name = $row->reference_name;
               $re_gender = $row->reference_gender==1?'ប្រុស​':'ស្រី​';
               $re_dob = $row->reference_dob;
               $re_nation = $row->reference_nation;
               $re_identify = $row->reference_personal_identify;
               $re_identify_number = $row->reference_number;
               $re_relationship = $row->reference_relationship;
               $html .=$i==3?'ព្រម​​ទាំង':'និង';
               $html .= '​ឈ្មោះ&nbsp;<b>'.$re_name.'</b>&nbsp;ភេទ​&nbsp;<b>'.$re_gender.'</b>&nbsp;ថ្ងៃ-ខែ-ឆ្នាំកំណើត​&nbsp;<b>'.$re_dob.'</b>&nbsp;សញ្ជាតិ​<b>'.$re_nation.'</b>
               ឯកសារ​​សម្គាល់​​អត្តសញ្ញាណ&nbsp;<b>'.$personal_identify[$re_identify].'</b>&nbsp;លេខ&nbsp;<b>'.$re_identify_number.'</b>&nbsp;ដែល​​ត្រូវ​​ជា&nbsp;<b>'. $relationship[$re_relationship].'</b>&nbsp;';
            $i++;
           }
         }

         $html  .='មាន​អាសយដ្ឋាន​​ផ្ទះ​​លេខ​​&nbsp;<b>'.$customer_address.'</b>&nbsp;ផ្លូវ​លេខ​&nbsp;<b>'.$customer_street.'</b>&nbsp;ក្រុម​ទី​&nbsp;<b>'.$customer_group.'</b>&nbsp;ភូមិ​&nbsp;<b>'.$customer_village.'</b>&nbsp;ឃុំ​/សង្កាត់​&nbsp;<b>'.$customer_commune.'</b>&nbsp;
         ស្រុក/ខណ្ឌ/ក្រុង&nbsp;<b>'.$customer_district.'</b>&nbsp;ខេត្ត/រាជធានី&nbsp;<b>'.$customer_province.'</b>&nbsp;។
           <h1 class="khmer underline center">សូមគោរពចូលមក​</h1>
           <b>
             <center class="center"><b>លោក​-​លោកស្រី​ នាយកសាខាប្រាសាក់​ ស្នាក់​ការ&nbsp;<b>'.$branch_name.'</b>&nbsp;ជាគ្រឺះ​​ស្ថាន​​ឯក​ជន</b></center>
           </b>
         <b>កម្មវត្ថុៈ</b> សំណើ​សុំ​ខ្ចី​ចង​ការ​ប្រាក់​ចំនួន​&nbsp;<b>'.$borrow_amount.'&nbsp;ដុល្លា​</b>&nbsp;ជាអក្សរ&nbsp;<b>'.$borrow_amount_character.'</b>&nbsp;រយៈ​ពេល​&nbsp;<b>'.$borrow_duration.'</b>ខែ អត្រាការប្រាក់​ស្នើសុំ​&nbsp;<b>'.$brrow_rate_request_permonth.'</b>%&nbsp;ក្នុង​១ខែ​ ។
         </p>
         <table style="width:100%;" border="1">
           <thead>
             <tr>
               <th class="center">ល.រ</th>
               <th class="center">គោលបំណងខ្ចី</th>
               <th class="center">ឯកតា</th>
               <th class="center">តម្លៃក្នុង១ឯកតា</th>
               <th class="center">ចំនួនទឹកប្រាក់​ចំណាយ​</th>
             </tr>
           </thead>
           <tbody>
             ';
             $query_purpose= "select * from loan_purpose where customer_id=$id";
             $query_result = $conn->query($query_purpose);
             if($query_result->num_rows){
               $i = 1;
               while($row = mysqli_fetch_object($query_result)) {
                   $borrow_purpose = $row->borrow_purpose;
                   $borrow_unit = $row->borrow_unit;
                   $borrow_unit_price = $row->borrow_unit_price;
                   $expence_amount = $row->expence_amount;
                   $html .= '<tr>
                               <td class="center">'.$i.'</td>
                               <td >'.$borrow_purpose.'</td>
                               <td class="center">'. $borrow_unit.'</td>
                               <td class="right">'.$borrow_unit_price.'</td>
                               <td class="right">'.$expence_amount.'</td>
                             </tr>';
                   $i++;
               }
             }else{
               $html .= '<tr>
                     <td style="padding:12px;"></td>
                     <td style="padding:12px;"></td>
                     <td style="padding:12px;"></td>
                     <td style="padding:12px;"></td>
                     <td style="padding:12px;"></td>
                   </tr>';
             }
             $html .='
           </tbody>
         </table>
         <p class="khmer">
           <b>ប្រភេទ​មុខ​ជំនួញ៖</b>
           ';
           $business_type_explode = explode(',',$business_type);
           if(count($business_type_explode)){
             for($i=0;$i<count($business_type_explode);$i++){
                 // $html .='<label><input type="checkbox" checked="chekced" selected name="service" value="service">  '.$businessType[$business_type_explode[$i]].'</label>&nbsp;&nbsp;';
                 $html .= $businessType[$business_type_explode[$i]];
                 $html .= ($i+1<count($business_type_explode))?', ':'';
             }
           }else{
                 $html .=' <label>ផ្សេងៗ...........................</label>&nbsp;&nbsp;';
           }
           $html .='
            <label>លេខ​កូដ................</label><br />
           <b>ទ្រង់​ទ្រាយ​មុខ​ជំនួយ៖</b>';
             $business_purpose_explode = explode(',',$business_purpose);
             if(count($business_purpose_explode)){
               for($i=0;$i<count($business_purpose_explode);$i++){
                   $html .= ' '.$businessPurpose[$business_purpose_explode[$i]];
                   $html .= ($i+1<count($business_purpose_explode))?', ':'';
               }
             }
             $html .='
             &nbsp;&nbsp;ទីតាំង​មុខ​ជំនួញៈ&nbsp;<b>'.$business_location.'</b>&nbsp;<br />
             ដោយ​ស្នើសុំ​សងៈ&nbsp;<b>'.$payment_request_method.'&nbsp;</b><br />
             បន្ថែម​លើ​កម្ចី​ដែល​ខ្ញុំ​បាទ/នាង​ខ្ញុំ​បាន​ខ្ចី​ &nbsp;
             <label><input type="checkbox" name="prasac">​ប្រាសាក់​</label>&nbsp;&nbsp;<label><input type="checkbox" name="group">​ស្ថាប័នផ្សេង....................</label>
             <label>ចំនួន​...............&nbsp;&nbsp;កាលពី​ថ្ងៃ​ទី​.......ខែ.......ឆ្នំា​..........</label><br />
             <label>ប្រាក់​ដើម​នៅ​ជំពាក់​សរុប​........................&nbsp;គោល​បំណង​ខ្ចី​.......................&nbsp;&nbsp;ការ​ប្រើ​ប្រាស់​ជាក់​ស្ដែង​.......................</label><br />
         
             <strong class="khmer">សិទ្ធិ​របស់​ប្រាសាក់ៈ</strong><br />
             &nbsp;&nbsp;&nbsp;&nbsp;ប្រាសាក់​នឹង​ប្រមូល​ព័ត៌មាន​របស់​អតិថិជន​ យក​ទៅ​បញ្ចូល​នៅ​ក្នុង​ប្រព័ន្ធ​ចែករំលែក​ព័ត៌មាន​ឥណទាន​។ ព័ត៌មាន​​ដែល​ប្រមូល​បាន​នឹង​ត្រូវ​បាន​ប្រើ​ប្រាស់​ដើម្បី​វាយ​តម្លៃ​ការ​ផ្ដល់​ឥណទាន ហើយ​ព័ត៌មាន​នេះនឹង​ត្រូវ​បាន​ចែក​រំលែក​ដល់​ឥណទាន​ដទៃ​ទៀត​ ដែល
             រួម​ក្នុង​ប្រព័ន្ធ​ចែក​រំលែក​ព័ត៌មាន​ឥណទាន​ និង​ ក្រមប្រតិបត្តិ​។ ប្រសិន​អតិថិជន​ចង់​បាន​ ឬចង់​កែតម្រូវ​ព័ត៌មាន​ ឬលុប​ចោល​ទិន្នន័យ​របស់​ខ្លួន​ ដោយ​មាន​ហេតុ​ផល​គ្រប់​គ្រាន់​ស្រប​តាម​នីតិ​វិធី​ ដូច​មាន​ចែង​នៅ​ក្នុង​ប្រកាស​ស្ដី​ពី​ការ​ចែក​រំលែក
             ព័ត៌មាន​ឥណទាន​ អតិថិជន​អាចស្នើ​សុំ​ជា​លាយលក្ខណ៍​អក្សរ​ពី​អ្នក​ផ្ដល់​សេវា​ប្រព័ន្ធ​ចេក​រំលែក​ព័ត៌មាន​ឥណទាន​ ដោយ​ភ្ជាប់​ជាមួយ​ ឯកសារ​សម្គាល់​អត្តសញ្ញាណ​ ហើយ​អ្នក​ផ្ដល់​សេវា​ប្រព័ន្ធ​ចែក​រំលែក​ព័ត៌មាន​​នឹង​ឆ្លើយ​តប​ជួន​អតិថិជន​វិញ​។
           
             <br /><strong class="khmer">ការ​យល់​ព្រម​ និង​ការ​អះ​អាងៈ</strong><br />
             &nbsp;&nbsp;&nbsp;&nbsp;យើង​ខ្ញុំ​សូម​អះអាង​ថា​រាល់​ព័ត៌មាន​ដែល​បាន​បង្ហាញ​ខាង​លើ​ពិត​ជា​ត្រឹមត្រូវ​ បើ​រកឃើញ​ថា​មាន​ព័តឥមាន​មិន​ពិត​ សូម​គណៈកម្ម​ការ​ឥណទាន​បដិសេធ​កម្ចី​របស់​យើង​ខ្ញុំ​ចុះ​ និង​ យើង​ខ្ញុំ​យល់​ព្រម​អោយ​ប្រាសាក់​ប្រមូល​ព័ត៌មាន​ដែល​មាន​ក្នុង​ពាក្យ​សុំ​ខ្ចី​ប្រាក់​ និង​ព័ត៌មាន​ដទៃទៀតដែល​ពាក់​ព័ន្ធ​
             នឹង​ពាក្យ​ខ្ចី​ប្រាក់​នេះ​ ព្រម​ទាំង​បើក​ចំហ​ព័ត៌មាន​ទាំង​នេះ​ ទៅ​ទអោយ​តតីយជនសម្រាប់​គោល​បំណង​ដូចមាន​ចែង​ក្នុង​​​ប្រកាស​ស្ដីពី​ការ​ចែក​រំលែក​ព័ត៌មាន​ឥណទាន​។ 
             អ្នក​ផ្ដល់​សេវា​ចែករំលែក​ព័ត៌មាន​ឥណទាន​ និង​ប្រសាក់​ ជា​អ្នក​ទទួល​ខុសត្រូវ​លើ​ការ​ប្រ​មូល​ ការ​ដាក់​អោយ​ដំណើរ​ការ​ និង​ផ្សព្វ​ផ្សាយ​ព័ត៌មាន​ឥណទានរបស់​អតិថិជន។<br />
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;តាម​កម្មវត្ថុខាងលើសូម​លោក-លោកស្រី​ មេត្តាពិនិត្យ​ និង​ សម្រច​ផ្ដល់​ប្រាក់​កម្ចី​អោយ​យើង​ខ្ញុំ​ ដោយ​ក្ដី​អនុគ្រោះ​។<br />
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;សូម​លោក-លោកស្រី​ ទទួល​នូវ​ការ​គោរពដ៍ខ្ពង់ខ្ពស់​អំពី​យើង​ខ្ញុំ។
             <div class="footer-wrapper">
                 <b>ថ្ងៃ​ទី............ខែ...............ឆ្នាំ​២០.............</b><br />
                 ស្នាម​មេដៃ​អ្នក​ស្នើ​សុំខ្ចី​ចងការ​ប្រាក់​<br />
                 <div class="box"><div class="footer-write"></div></div>
                 <div class="box"><div class="footer-write"></div></div>
                 <div class="box"><div class="footer-write"></div></div>
                 <div class="box"><div class="footer-write"></div></div>
                 <div class="box"><div class="footer-write"></div></div>
                 <div class="box"><div class="footer-write"></div></div>
             </div>
           </p>
         </form>
         <style>
           div.footer-wrapper{
             text-align:center;
             overflow:hidden;
             margin-top:-1.6em !important;
             width:80%
             display:block;
             float:right;
             padding-bottom:1em;
           }
           div.footer-wrapper div.box{
             width:75px;
             height:90px;
             float:right;
             padding-bottom:1.5em;
             border-bottom:1px dotted black;
             margin:0 1.4em;
           }
           div.box div.footer-write{
               width:75px;
               height:90px;
               border:1px solid black;
           }
         </style>
     </body>';
   // echo $html;
 $mpdf->WriteHTML($html);
}
?>