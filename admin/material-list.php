  <?php

    $items_checked = isset($_POST['checkboxitem'])?$_POST['checkboxitem']:'';
    if($items_checked){
      $items_checked = implode(',',$items_checked);
    }

    if(isset($_POST['btn-enable'])){
      $query = "UPDATE materials as m set m.status=1 WHERE m.id IN($items_checked)";
    }elseif(isset($_POST['btn-disable'])){
      $query = "UPDATE materials as m set m.status=0 WHERE m.id IN($items_checked)";
    }else{
      $status= isset($_GET['status'])?$_GET['status']:0;
      $id = isset($_GET['id'])?$_GET['id']:0;
      $query = "UPDATE materials as m set m.status=$status WHERE m.id=$id limit 1";
    }
    $result = $conn->query($query);
    
    if(isset($_GET['status']) && isset($_GET['id'])){
      $status= $_GET['status'];
      $id = $_GET['id'];
      $query = "UPDATE materials as m set status=$status WHERE id=$id limit 1";
      $result = $conn->query($query);
    }
  ?>
    <div class="col s12 l12">
      <blockquote>
          <h1>ការកំណត់សម្ភារៈ</h1>
      </blockquote>
      <form class="form" action="" method="POST">
        <button class="left" name="btn-disable"  style="cursor:pointer;padding:0 5px;color:red;background:none;border:none;">បិទការប្រើប្រាស់
            <i class="material-icons left" style="color:red;margin-right:5px;">close</i>
        </button>
        <button class="left" name="btn-enable" style="cursor:pointer; padding:0 5px;color:#4DB848;background:none;border:none;">បើកការប្រើប្រាស់
          <i class="material-icons left" style="color:#4DB848;margin-right:5px;">check</i>
        </button>
        <table class="highlight bordered admin-table responsive-table">
          <thead>
            <tr>
                <th class="centered" style="width:5%;">
                    <label for="check-all">
                      <input type="checkbox" id="check-all"/>
                      <span></span>
                    </label>
                </th>
                <th class="centered" style="width:30%;">ឈ្មោះសម្ភារៈជាខេមរភាសា</th>
                <th class="centered" style="width:30%;">ឈ្មោះសម្ភារៈជាភាសាអង់គ្លេស</th>
                <th class="centered" style="width:20%;">ប្រភេទ</th>
                <th class="centered" style="width:5%;">ស្ថានភាព</th>
            </tr>
          </thead>
          <tbody>
        
          <?php
              
            $limit = 50;
            $offset = isset($_GET['page'])?$_GET['page']:0;
            if (isset($_GET["page"])) { 
              $page  = $_GET["page"]; 
              } else { 
                $page=1; 
              };  
              
            $start_from = ($page-1) * $limit;

            $condition = '';
            $pagination = " LIMIT $start_from, $limit";
            if(isset($_POST['btn-search'])){
              $month = $_POST['month'];
              $year = $_POST['year'];
              $province =  isset($_POST['province'])?$_POST['province']:'';
              $branch = isset($_POST['branch'])?$_POST['branch']:'';

              if($province){
                  $condition .= " c.customer_province=$province";
                }
                if($branch){
                  $condition .= " and  c.duty_station in($branch)";
              }

                
              if($start_date && !$end_date){
                  $condition .= " c.created_date >= '$start_date'";
              }
              if($end_date && !$start_date){
                  $condition .= " c.created_date >= '$end_date'";
              }
              if($end_date && $start_date){
                  $condition .= " c.created_date between '$start_date' and '$end_date'";
              }

            
              if($start_date || $end_date || $branch || $province){
                $condition = " where $condition";
              }
              $pagination = "";

            }
        
            $query = "SELECT * FROM materials as m 
                  ORDER BY m.id ASC";
            $result = $conn->query($query);
            $customer  = array();
            if(($result) && $result->num_rows>0){
              while($row = mysqli_fetch_object($result)) {
                ?>
                  <tr class="list-row">
                    <td class="centered">
                        <label for="checkbox-item<?php echo $row->id ?>">
                          <input type="checkbox" id="checkbox-item<?php echo $row->id ?>" name="checkboxitem[]" value="<?php echo $row->id ?>"/>
                          <span></span>
                        </label>
                    </td>
                    <td class="detail modal-trigger"><?php echo $row->title_kh ?></td>
                    <td class="detail"><?php echo $row->title_en; ?></td>
                    <td class="detail"><?php echo $row->material_type ?></td>
                    <td class="detail centered">
                      <?php 

                        if($row->status):
                          echo '<a href="?id='.$row->id.'&status=0"><i class="material-icons active">check</i></a>';
                        else:
                          echo '<a href="?id='.$row->id.'&status=1"><i class="material-icons disactive">close</i></a>';
                        endif;
                      ?>
                    </td>
                  </tr>
              <?php
              }
            }else{
              echo '<tr><td class="centered" colspan="8" style="color:#ee6e73;text-align:center;">ពុំ​មាន​ទិិន្នន័យ​ដែល​លោក​អ្នក​ស្វែង​រក​​នោះ​ទេ.</td></tr>';
            }
            ?>
            </tbody>
        </table>
      </form>
    </div>