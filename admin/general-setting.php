<?php
  include('session.php');
  if(!isset($_SESSION['login_user'])){
    header("location:/");
  }else if(isset($_SESSION['login_user']) && ($_SESSION['login_id'] ==2)){ // if user already logged in with admin user
    header("location:http://feedback.local/auth/index.php");//redirect to admin list page
  }else{
    include('header.php');
    $header_text = 'ប្រព័ន្ធគ្រប់គ្រង់សម្ភារៈផ្សព្វផ្សាយ';
    include('head.php');

    if(isset($_POST['btn-save-data'])){
        $date_from  =   $_POST['date-from'];
        $date_to  =   $_POST['date-to'];
        $query = 'UPDATE tbl_general_setting SET setting_value_1="'.$date_from.'", setting_value_2="'.$date_to.'" where setting_name="data_entry_schedule" LIMIT 1';
        $result = $conn->query($query);
        // var_dump($query);
    }
    ?>
    <div class="row">
        <nav class="nav-main">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="http://feedback.prasac.local" class="breadcrumb">ទំព័រដើម</a>
                    <a href="#" class="breadcrumb">ការកំណត់ទូរទៅ</a>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="col s4 m4 l4">
            <?php echo isset($message)?$message:''; ?>
        </div>
        <div class="col s8 m8 l8 left button-wrapper">
            <a class="btn waves-effect btn-success waves-left right" href="material-setting.php">ការកំណត់សម្ភារៈ
                <i class="material-icons left">settings_applications</i>
            </a>
            <a class="btn waves-effect btn-success waves-left right" href="branch-setting.php" style="margin-left:10px;">ការកំណត់សាខា
                <i class="material-icons left">settings_applications</i>
            </a>
            <a class="btn waves-effect btn-success waves-left right active" href="general-setting.php">ការកំណត់ទូរទៅ
                <i class="material-icons left ">settings_applications</i>
            </a>
        </div>
    </div>
    <div class="row">
            <div class="col s12 m12 l12">
                <blockquote>
                    <h1>ការកំណត់ទូរទៅ</h1>
                </blockquote>
                <div class="row">
                    <div class="input-field col s12 m2 l3">
                        <blockquote>
                            <strong>កាលវិភាគបញ្ចូលរបាយការណ៍</strong>
                        </blockquote>
                    </div>
                    <div class="input-field col s12 m10 l9">
                        <form action="" method="POST">
                        <?php
                            $query_s = "select * from tbl_general_setting order by id ASC";
                            $result_s = $conn->query($query_s);
                            $row_schedule = mysqli_fetch_object($result_s);
                        ?>
                            <div class="input-field col s4 m4 l4">
                                <input type="text" class="datepickerfrom" id="datefrom" name="date-from" value="<?php echo $row_schedule->setting_value_1 ;?>">
                                <label for="datefrom">ពីថ្ងៃ និងម៉ោង</label>
                            </div>
                            <div class="input-field col s4 m4 l4">
                                <input type="text" class="datepickerto" id="dateto" name="date-to" value="<?php echo $row_schedule->setting_value_2 ;?>">
                                <label for="dateto">ដល់ថ្ងៃ និងម៉ោង</label>
                            </div>

                            <div class="col s4 m4 l4">
                                <button class="btn waves-effect btn-success waves-light right" type="submit" name="btn-save-data">រក្សារការកំណត់
                                    <i class="material-icons left">save</i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>
  <?php
  }
  include('footer.php');
  ?>