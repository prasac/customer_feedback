
  <div class="col s12 l12">
    <div class="material-table">
      <div class="table-header">
        <div class="actions">
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table id="mydatatable" class="bordered list-authen responsive-table">
        <thead>
          <tr>
              <th class="centered">
                  <label for="check-all">
                    <input type="checkbox" id="check-all"/>
                    <span></span>
                  </label>
              </th>
              <th class="centered">ឈ្មោះភូមភាគ</th>
              <th class="centered">ឈ្មោះសាខា</th>
              <th class="centered">លេខ FC</th>
              <th class="centered">ចំនួនទទួល</th>
              <th class="centered">ចំនួនផ្ទេរ</th>
              <th class="centered">សមតុល្យចុងខែ</th>
              <th class="centered" colspan="2">មតិ</th>
          </tr>
        </thead>
        <tbody>
        <?php
            
          //$limit = 100;
          $offset = isset($_GET['page'])?$_GET['page']:0;
          if (isset($_GET["page"])) { 
            $page  = $_GET["page"]; 
            } else { 
              $page=1; 
            };  
            
          // $start_from = ($page-1) * $limit;

          $condition = '';
          $pagination = "";

          $months = array(1 => 'មករា', 2 => 'កុម្ភៈ', 3 => 'មីនា', 4 => 'មេសា', 5 => 'ឧសភា', 6 => 'មិថុនា', 7 => 'កក្កដា', 8 => 'សីហា', 9 => 'កញ្ញា', 10 => 'តុលា', 11 => 'វិច្ឆិការ', 12 => 'ធ្នូរ');
          $transposed = array_slice($months, date('n'), 12, true) + array_slice($months, 0, date('n'), true);
          $last8 = array_reverse(array_slice($transposed, -8, 12, true), true);

          if(isset($_POST['btn-submit'])){
            $posted_regional= $_POST['regional'];
            $posted_branch  = isset($_POST['branch'])?implode(',',$_POST['branch']):'0';
            $selected_month = isset($_POST['month'])?$_POST['month']:'"'.date("m").'"';
            $selected_year = isset($_POST['year'])?$_POST['year']:date('Y');
            $condition = '';
            if($posted_regional && $posted_regional !='0'){
                $condition .= " and b.regional_id='$posted_regional'";
            }

            if($posted_branch){
                $condition .= " and  b.id in($posted_branch)";
            }

            if($selected_month){
                $condition .= " and month = '$selected_month'";
            }

            if($selected_year){
                $condition .= " and year = '$selected_year'";
            }
          }


            $query = "SELECT
                    b.id,
                    b.fc as fc_number,
                    b.name_kh as branch_title,
                    (select name from tbl_regional where id=b.regional_id) as regional_name,
                    (select SUM(recieved_amount) from posm_report as r where r.branch_id=b.id $condition limit 1) as recive_amount,
                    (select SUM(transfer_amount) from posm_report as r  where r.branch_id=b.id $condition limit 1) as transfer_amount,
                    (select SUM(ending_amount) from posm_report as r  where r.branch_id=b.id $condition limit 1) as ending_amount,
                    (select GROUP_CONCAT(DISTINCT comment ORDER BY comment ASC SEPARATOR '<br />') from posm_report as r WHERE b.id=r.branch_id $condition limit 1) AS comment
                    FROM branches as b ORDER BY b.regional_id ASC $pagination";
            // echo $query;
          $result = $conn->query($query);
          $customer  = array();
          if(($result) && $result->num_rows>0){
            $total_recived=0;
            $total_transfer=0;
            $total_ending=0;
            while($row = mysqli_fetch_object($result)) {
              ?>
                <tr class="list-row">
                  <td class="centered">
                      <label>
                        <input type="checkbox" id="checkbox-item<?php echo $row->id ?>" name="checkboxitem[]" value="<?php echo $row->id ?>"/>
                        <span></span>
                      </label>
                  </td>
                  <td class="detail"  id="<?php echo ($row->id);?>"><?php echo $row->regional_name ?></td>
                  <td class="detail"  id="<?php echo ($row->id);?>"><?php echo $row->branch_title ?></td>
                  <td class="detail centered"  id="<?php echo ($row->id);?>"><?php echo $row->fc_number ?></td>
                  <td class="detail centered"  id="<?php echo ($row->id);?>"><?php echo $row->recive_amount?$row->recive_amount:0; ?></td>
                  <td class="detail centered"  id="<?php echo ($row->id);?>"><?php echo $row->transfer_amount?$row->transfer_amount:0; ?></td>
                  <td class="detail centered"  id="<?php echo ($row->id);?>"><?php echo $row->ending_amount?$row->ending_amount:0 ?></td>
                  <td class="detail" colspan="2" id="<?php echo ($row->id);?>"><?php echo $row->comment?$row->comment:'No Comment'?></td>
                </tr>
            <?php
              $total_recived +=$row->recive_amount;
              $total_transfer +=$row->transfer_amount;
              $total_ending +=$row->ending_amount;
            }
            // Total Row
            echo '<tr class="list-row">';
            echo '<td colspan="4" style="text-align:right;font-weight: bold;border: 1px dotted #4db848;">Total: </td>';
            echo '<td class="centered total_recieve" style="font-weight: bold;"><span>'.$total_recived.'</span></td>';
            echo '<td class="centered total_transfer" style="font-weight: bold;"><span>'.$total_transfer.'</span></td>';
            echo '<td class="centered total_ending" style="font-weight: bold;"><span>'.$total_ending.'</span></td>';
            echo '<td style="text-align:right;font-weight: bold;border-right: 1px solid #4db848;"></th>';
            echo '</tr>';
          }else{
            echo '<tr><td class="centered" colspan="8" style="color:#ee6e73;text-align:center;">ពុំ​មាន​ទិន្នន័យ​ដែល​លោក​អ្នក​ស្វែង​រក​​នោះ​ទេ.</td></tr>';
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>