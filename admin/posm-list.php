
  <div class="col s12 l12">
    <div class="row">
        <div class="col s2 l2 left">
          <button type="submit" name="btn-excel" class="btn-export-excel btn-success left btn waves-effect waves-left tooltipped" data-delay="30" data-tooltip="ទាញ​យក​ទិន្នន័យ​ជា EXCEL"> ទាញយក​៖ <i class="fa fa-file-excel-o fa-2x btn-excel" aria-hidden="true"></i></button> 
        </div>
        <div class="col s2 l8 setting-button">
          <a class="left btn btn-success waves-effect waves-left" href="general-setting.php">ការកំណត់ទូរទៅ
            <i class="material-icons left">settings_applications</i>
          </a>
          <a class="left btn-success btn waves-effect waves-left" href="branch-setting.php" style="margin-left:10px;">ការកំណត់សាខា
            <i class="material-icons left">settings_applications</i>
          </a>
          <a class="left btn btn-success waves-effect waves-left" href="material-setting.php">ការកំណត់សម្ភារៈ
            <i class="material-icons left">settings_applications</i>
          </a>
        </div>
        </div>
          <table class="bordered list-authen responsive-table">
            <thead>
              <tr>
                  <th class="centered">
                      <label for="check-all">
                        <input type="checkbox" id="check-all"/>
                        <span></span>
                      </label>
                  </th>
                  <th class="centered">ភូមិភាគ</th>
                  <th class="centered">លេខ FC</th>
                  <th class="centered">ឈ្មោះសាខា</th>
                  <th class="centered">ចំនួនទទួល</th>
                  <th class="centered">ចំនួនផ្ទេរ</th>
                  <th class="centered">សមតុល្យចុងខែ</th>
                  <th class="centered" colspan="2">មតិ</th>
              </tr>
            </thead>
            <tbody>
            <?php
                
              $limit = isset($_POST['num_row'])?$_POST['num_row']:200;
              $offset = isset($_GET['page'])?$_GET['page']:0;
              if (isset($_GET["page"])) { 
                $page  = $_GET["page"]; 
                } else { 
                  $page=1; 
                };  
                
              $start_from = ($page-1) * $limit;

              $condition = '';
              $pagination = " LIMIT $start_from, $limit";

              $months = array(1 => 'មករា', 2 => 'កុម្ភៈ', 3 => 'មីនា', 4 => 'មេសា', 5 => 'ឧសភា', 6 => 'មិថុនា', 7 => 'កក្កដា', 8 => 'សីហា', 9 => 'កញ្ញា', 10 => 'តុលា', 11 => 'វិច្ឆិការ', 12 => 'ធ្នូរ');
              $transposed = array_slice($months, date('n'), 12, true) + array_slice($months, 0, date('n'), true);
              $last8 = array_reverse(array_slice($transposed, -8, 12, true), true);
              $condition .= " and month =".date('m');
              $condition .= " and year =".date('Y');
              $regional_selected='';

              if(isset($_POST['btn-submit'])){
                // var_dump($_POST);
                $posted_regional= $_POST['regional'];
                $posted_branch  = isset($_POST['branch'])?implode(',',$_POST['branch']):'0';
                $selected_month = isset($_POST['month'])?$_POST['month']:0;
                $selected_year = isset($_POST['year'])?$_POST['year']:0;
                $condition = '';
                if($posted_regional && $posted_regional>0){
                  $condition .= " and b.regional_id=$posted_regional";
                  $regional_selected = "where b.regional_id=$posted_regional";
                }
          
                if($posted_materials){
                    $condition .= " and r.material_id in($posted_materials)";
                }
          
                if($selected_month){
                    $condition .= " and month = $selected_month";
                }
          
                if($selected_year){
                    $condition .= " and year = $selected_year";
                }
              }


                $query = "SELECT
                        b.id,
                        b.fc as fc_number,
                        b.name_kh as branch_title,
                        (select region from tbl_regional where id=b.regional_id) as regional_id,
                        (select SUM(recieved_amount) from posm_report as r where r.branch_id=b.id $condition limit 1) as recive_amount,
                        (select SUM(transfer_amount) from posm_report as r  where r.branch_id=b.id $condition limit 1) as transfer_amount,
                        (select SUM(ending_amount) from posm_report as r  where r.branch_id=b.id $condition limit 1) as ending_amount,
                        (select GROUP_CONCAT(DISTINCT comment ORDER BY comment ASC SEPARATOR '<br />') from posm_report as r WHERE b.id=r.branch_id $condition limit 1) AS comment
                        FROM branches as b $regional_selected ORDER BY b.regional_id ASC $pagination";
              // echo $query;
              $result = $conn->query($query);
              $customer  = array();
              if(($result) && $result->num_rows>0){
                $total_recived=0;
                $total_transfer=0;
                $total_ending=0;
                while($row = mysqli_fetch_object($result)) {
                  ?>
                    <tr class="list-row">
                      <td class="centered">
                          <label>
                            <input type="checkbox" id="checkbox-item<?php echo $row->id ?>" name="checkboxitem[]" value="<?php echo $row->id ?>"/>
                            <span></span>
                          </label>
                      </td>
                      <td class="detail centered"  id="<?php echo ($row->id);?>"><?php echo $row->regional_id ?></td>
                      <td class="detail centered"  id="<?php echo ($row->id);?>"><?php echo $row->fc_number ?></td>
                      <td class="detail"  id="<?php echo ($row->id);?>"><?php echo $row->branch_title ?></td>
                      <td class="detail centered"  id="<?php echo ($row->id);?>"><?php echo $row->recive_amount?$row->recive_amount:0; ?></td>
                      <td class="detail centered"  id="<?php echo ($row->id);?>"><?php echo $row->transfer_amount?$row->transfer_amount:0; ?></td>
                      <td class="detail centered"  id="<?php echo ($row->id);?>"><?php echo $row->ending_amount?$row->ending_amount:0 ?></td>
                      <td class="detail" colspan="2" id="<?php echo ($row->id);?>"><?php echo $row->comment?$row->comment:'No Comment'?></td>
                    </tr>
                <?php
                }
                
              }else{
                echo '<tr><td class="centered" colspan="8" style="color:#ee6e73;text-align:center;">ពុំ​មាន​ទិន្នន័យ​ដែល​លោក​អ្នក​ស្វែង​រក​​នោះ​ទេ.</td></tr>';
              }
              ?>
              </tbody>
              <?php 
              $query_pagin  = mysqli_query($conn,"select id from branches");
              $query_pagin_count = mysqli_num_rows($query_pagin);
              $total_pages = ceil($query_pagin_count / $limit); 
              //if no page var is given, default to 1.
              $prev = $page - 1;							//previous page is page - 1
              $next = $page + 1;							//next page is page + 1
              $lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
              $lpm1 = $lastpage - 1;						//last page minus 1
              $activepre = $prev==0?'disabled':'waves-effect';
              $prevlink = $prev?"?page=$prev":'#';

              $activenext = $next>$total_pages?'disabled':'waves-effect';
              $nextlink = $next<=$total_pages?"?page=$next":'#';
              if($query_pagin_count>$limit){?>
                <tfoot>
                  <tr>
                    <td colspan="9" class="centered">
                      <ul class="pagination">
                        <li class="<?php echo $activepre;?>"><a href="<?php echo $prevlink;?>"><i class="material-icons">chevron_left</i></a></li>
                        <?php
                          for ($i=1; $i<=$total_pages; $i++) { 
                          $active = (isset($_GET['page']) && $_GET['page']==$i) || (!isset($_GET['page']) && $i==1)?'active':'';
                          ?>
                          <li  class="<?php echo $active;?> waves-effect"><a href="?page=<?php echo $i;?>"><?php echo $i;?></a></li>
                          <?php 
                          }
                        ?>
                        <li class="<?php echo $activenext;?>"><a href="<?php echo $nextlink;?>"><i class="material-icons">chevron_right</i></a></li>
                      </ul>
                    </td>
                  </tr>
                </tfoot>
              <?php 
              }
              $conn->close();
              ?>
          </table>
    </div>