<?php
   $query = "SELECT
    d.id,p.name client_name,p.dob dob,p.nationality nationality,p.type_of_id type_of_id,p.id_number id_number,
    p.id_issured_date,p.id_expiry_date,p.phone_number,p.email_address,p.address,p.created_date,b.name_kh branch_name,d.is_view,d.created_date
    FROM deposit_customer_personal_info AS p
    left join deposit_customer_info as d on p.customer_id=d.id
    left join branches as b on d.duty_station=b.id
    where d.id in($items_checked)
    ORDER BY d.account_name_kh ASC $pagination";
    $result = $conn->query($query);

    $idtype = array(1=>'National ID Card',2=>'Passport',3=>'Driver License',4=>'Others');
    while($row = mysqli_fetch_assoc($result)) {
      $id = $row['id'];
      $mpdf->SetHTMLFooter('<div class="pdf-footer"><div class="footer-right">SA 001.V3​</div><div class="footer-left">Form : Personal Account Opening Application | Private Company </div></div>');
      $html = '
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <body>
          <head>
          </head>
          <body>
          <style>
          html,body{
            font-family:hanuman,Verdana;
            font-size:14px;
          }
          h1.khmer{
            font-family:"khmerosmoul",Arail;
            font-weight:lighter;
            font-size:1.1em;
            margin:0;
            padding:0;
          }
          strong.khmer{
            font-family:"khmerosmoul";
            font-weight:lighter;
            font-size:1.1em;
            line-height:1.8em;
          }
          h1.khmer.center{
            font-family:"khmerosmoul";
            font-weight:lighter;
            font-size:1.1em;
            text-align:center;
            line-height:1.8em;
          }

          h1.underline{
            text-decoration:underline;
          }
          div.right{
            text-align:right;
            float:right;
            margin-top:-2.5em;
          }
          .right{
            text-align:right;
            float:right;

          }
          .center,strong.center,center.center{
            text-align:center;
          }
          p{
            line-height:1.8em;
            font-size:12px;
          }
          p.khmer,span.khmer{
            font-family:"hanuman";
            font-size:13px;
            text-lign:justify;
          }
          table{
            border-collapse:collapse;
          }
          table thead tr th{
              valign:middle;
              padding:5px;
              font-weight:bold;
          }
          table tbody tr td{
            valign:middle;
            padding:5px;
          }
          table tbody tr td.center{
            text-align:center;
          }
          .pdf-footer{
            font-size:10px;
          }
          .footer-left{
            float:left;
            text-align:left;
            width:40%;
          }
          .footer-right{
            float:right;
            text-align:right;
            font-family:"hanuman";
            width:40%;
          }
          p.khmer.content{
            line-height:2;
            text-align:justify !important;
            width:100%;
          }
          input.khmer{
            font-family:"khmeros";
          }

          div.head-wraper{
            width:100%;
            margin:0 auto;
            overflow:hidden;
          }
          div.content{
            width:100%;
            overflow:hidden;
          }
          div.body-pdf{
            width:100%;
            margin:0 auto;
          }
          div.content div.label{
            width:20%;
            float:left;
          }
          div.content div.data{
            width:80%;
            float:right;
          }
          div.sub-label{
            width:20%;
            text-align:left;
            float:left;
            font-size:11px;
            line-height:0;
            margin-top:-5px;
            border:0px solid black;
          }
          div.sub-label-right{
            width:77%;
            text-align:left;
            float:right;
            font-size:11px;
            margin-top:-5px;
            border:0px solid red;
          }
          div.sub-label-right span.item{
            display:block; 
            border:0px solid blue;
            margin:0 20px;
          }
          div.content-second{
            border:1px solid #000000;
            padding:2px 5px;
            margin:8px auto;
          }
          </style>
            <form>
              <div class="head-wraper">
                <div  style="border:0px solid red;float:left;width:50%;padding:0;">
                    <img src="../images/logo-website.png" alt="" title="" class="img-responsive" width="90">
                </div>
                <div style="border:0px solid red;float:right;width:50%;padding:0;">
                  <h1 class="khmer right">ពាក្យស្នើសុំបើកគណនី <br /> ACCOUNT OPENING APPLICATION</label></h1>
                </div>
              </div>
              <div class="body-pdf">
                <h1 class="khmer">I.ព័ត័មានគណនី ​​/ ACCOUNT INFORMATION</h1>
                <div class="content">
                  <div class="label">ឈ្មោះគណនីជាខ្មែរ</div>
                  <div class="data">: .......................................</div>
                  <div class="sub-label">Account Name in Khmer</div>
                </div>
                <div class="content">
                  <div class="label">ឈ្មោះគណនីជាអក្សរឡាតាំង </div>
                  <div class="data">: ........................................</div>
                  <div class="sub-label">Account Name in Latin</div>
                </div>
                <div class="content">
                  <div class="label">លេខគណនី</div>
                  <div class="data">: ........................................</div>
                  <div class="sub-label">Account Number</div>
                </div>
                <div class="content">
                  <div class="label">ប្រភេទគណនី</div>
                  <div class="data">: 
                    <input type="checkbox" checked="checked" name="saving" value="saving"/> គណនីសន្សំ 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="unfixed" value="unfixed"/> គណនីចម្រើនគ្មានកាលកំណត់ 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="retired" value="retired"/> គណនីសោធននិវត្តន៌
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="other" value="other"/> ផ្សេងៗ................
                  </div>
                  <div class="sub-label">Accout Type</div>
                  <div class="sub-label-right">
                    &nbsp;
                    Savings Account &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    Unfixed Deposit Account &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    Retirement Account &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Others
                  </div>
                </div>
                <div class="content">
                  <div class="label">ប្រភេទរូបិយប័ណ្ឌ</div>
                  <div class="data">: 
                    <input type="checkbox"  name="khr" value="khr"/> ប្រាក់រៀល 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" checked="checked" name="usd" value="usd"/> ប្រាក់ដុល្លាអាមេរិក 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="thb" value="thb"/> ប្រាក់បាត
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="other" value="other" /> ផ្សេងៗ...............
                  </div>
                  <div class="sub-label">Currency Type</div>
                  <div class="sub-label-right">
                    &nbsp;
                    KHR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                    USD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    THB &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Others
                  </div>
                </div>
                <div class="content">
                  <div class="label">សេវាកម្ម</div>
                  <div class="data">: 
                    <input type="checkbox" checked="checked"  name="atm" value="atm"/> ប័ណ្ណអេធីអឹម 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" checked="checked" name="mib" value="mib"/> សេវាធនាគារចល័ត
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="other" value="other" /> ផ្សេងៗ..................
                  </div>
                  <div class="sub-label">Services</div>
                  <div class="sub-label-right">
                    &nbsp;
                    ATM  Card &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                    Mobile and Internet Banking &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    Others
                  </div>
                </div>
                <div class="content">
                  <div class="label">និវាសនដ្ឋាន</div>
                  <div class="data">: 
                    <input type="checkbox" checked="checked"  name="resident" value="resident"/> និវាសនជន
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" name="non-resident" value="non-resident"/> អនិវាសនជន
                  </div>
                  <div class="sub-label">Resident Status</div>
                  <div class="sub-label-right">
                    &nbsp;
                    Resident &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                    Non-Resident
                  </div>
                </div>

                <h1 class="khmer">II.ព័ត័មានផ្ទាល់ខ្លួន ​​/ PERSONAL INFORMATION</h1>
                <div class="content-second">
                  <div class="label">១.លេខ CIF / CIF Nº</div>
                  <div class="data">: </div>

                  <div class="label">១.លេខ CIF / CIF Nº</div>
                  <div class="data">: </div>
                  <div class="sub-label">Resident Status</div>
                  <div class="sub-label-right">
                    &nbsp;
                    Resident &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                    Non-Resident
                  </div>
                </div>
                <div class="content-second">
                  2
                </div>
                <div class="content-second">
                  3
                </div>
              </div>
            </form>
            <style>
              div.footer-wrapper{
                text-align:center;
                overflow:hidden;
                margin-top:-1.6em !important;
                width:80%
                display:block;
                float:right;
                padding-bottom:1em;
              }
              div.footer-wrapper div.box{
                width:75px;
                height:90px;
                float:right;
                padding-bottom:1.5em;
                border-bottom:1px dotted black;
                margin:0 1.4em;
              }
              div.box div.footer-write{
                  width:75px;
                  height:90px;
                  border:1px solid black;
              }
            </style>
          </body>';
      $mpdf->WriteHTML($html);
    }
?>