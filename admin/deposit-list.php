
  <div class="col s12 l12">
    <form class="form" action="" method="POST">
    <input type="hidden" value="deposit" name="service_type" />
      <table class="highlight bordered">
        <thead>
          <tr>
              <th colspan="3">
                  <span class="left">ទាញយក​ទិន្នន័យជា៖</span>
                  <button type="submit" name="btn-pdf" class="left tooltipped" style="padding:0 8px;background:transparent;border:none;" data-delay="30" data-tooltip="ទាញយក​ទិន្នន័យជា​ PDF"><i class="fa fa-file-pdf-o fa-2x btn-pdf" aria-hidden="true"></i></button> 
                  <button type="submit" name="btn-excel" class="left tooltipped" style="padding:0 8px;background:transparent;border:none;" data-delay="30" data-tooltip="ទាញ​យក​ទិន្នន័យ​ជា EXCEL"><i class="fa fa-file-excel-o fa-2x btn-excel" aria-hidden="true"></i></button> 
              </th>
              <th colspan="6" style="text-align:left;padding:0 5px;">
                  <?php echo isset($message)?$message:''; ?>
              </th>
              <td>
                <button type="button" name="btn-approved" class="right approved dropdown-button" style="padding:0 8px;background:transparent;border:none;" data-beloworigin="true" data-activates='dropdown1'><i class="small material-icons prefix">settings</i></button>    
                <ul id='dropdown1' class='dropdown-content'>
                  <li><button type="submit" name="btn-unread" style="padding:4px;background:transparent;border:none;"> មិនទាន់មើល</button></li>
                  <li><button type="submit" name="btn-read" style="padding:4px;background:transparent;border:none;"> មើលរួច</button></li>
                </ul>
              </td>
          </tr>
          <tr>
              <th class="centered" style="width:1%">
                <input type="checkbox" id="check-all" name="check_all"/>
                <label for="check-all"></label>
              </th>
              <th class="centered" style="width:11%">ឈ្មោះ​អតិថិជន</th>
              <th class="centered" style="width:9%">ថ្ងៃខែឆ្នាំ កំណេីត</th>
              <th class="centered" style="width:11%">សញ្ជាតិ</th>
              <th class="centered" style="width:11%">ឯ.បញ្ជាក់អត្តសញ្ញណ</th>
              <th class="centered" style="width:13%">ថេរវេលាមានប្រសិទ្ធភាព</th>
              <th class="centered" style="width:11%">លេខ</th>
              <th class="centered" style="width:11%">ឈ្មោះ​សាខាស្នើសុំ</th>
              <th class="centered" style="width:11%">អុីម៉ែល</th>
              <th class="centered" style="width:11%">ថ្ងៃស្នើសុំ</th>
          </tr>
        </thead>
        <tbody>
      
        <?php
            
            $limit = 50;
            $offset = isset($_GET['page'])?$_GET['page']:0;
            if (isset($_GET["page"])) { 
              $page  = $_GET["page"]; 
            } else { 
                $page=1; 
            };  
            
            $start_from = ($page-1) * $limit; 
            $businessType = array('កសិកម្ម​','ពាណិជ្ជកម្ម/ជំនួញ','សេវាកម្ម​​','ដឹកជញ្ជួន','សាងសង់','ប្រើប្រាស់​ទូទៅក្នុងគ្រួសារ');
            $businessPurpose = array('​ឯកកម្ម​សិទ្ធិ','សហកម្មសិទ្ធិ','ពង្រីកមុខជំនួញ','ចាប់​ផ្ដើម​មុខ​ជំនួញ');

            $condition = '';
            $pagination = " LIMIT $start_from, $limit";
            if(isset($_POST['btn-search'])){
              $service_type = isset($_POST['service_type'])?$_POST['service_type']:'';
              $start_date = $_POST['start_date']?date("Y-m-d",strtotime($_POST['start_date'])):'';
              $end_date = $_POST['end_date']?date("Y-m-d",strtotime($_POST['end_date'])):'';
              $branch = isset($_POST['branch'])?$_POST['branch']:'';
              $province =  isset($_POST['province'])?$_POST['province']:'';
              
              
              
              if($start_date && !$end_date){
                $condition .= " d.created_date >= '$start_date'";
              }
              if($end_date && !$start_date){
                $condition .= " d.created_date >= '$end_date'";
              }
              if($end_date && $start_date){
                $condition .= " d.created_date between '$start_date' and '$end_date'";
              }

              if($branch && (!$start_date && !$end_date)){
                $condition .= "d.duty_station in($branch)";
              }

              if($branch && ($start_date || $end_date)){
                $condition .= " and d.duty_station in($branch)";
              }

              if($start_date || $end_date || $branch){
              $condition = " where $condition and p.relationship IS NULL";
              }
              $pagination = "";

            }

            $query = "SELECT
                d.id,p.name client_name,p.dob dob,p.nationality nationality,p.type_of_id type_of_id,p.id_number id_number,
                p.id_issured_date,p.id_expiry_date,p.phone_number,p.email_address,p.address,p.created_date,b.name_kh branch_name,d.is_view,d.created_date
                FROM deposit_customer_personal_info AS p
                left join deposit_customer_info as d on p.customer_id=d.id
                left join branches as b on d.duty_station=b.id
                $condition
                ORDER BY d.account_name_kh ASC $pagination";
                // echo $query;
            $result = $conn->query($query);
            $customer  = array();
            if(($result) && $result->num_rows>0){
            while($row = mysqli_fetch_object($result)) {
              $is_view = $row->is_view?'ready':'not-ready';
              $idtype = array(1=>'National ID Card',2=>'Passport',3=>'Driver License',4=>'Others');
              ?>
                <tr class="<?php echo $is_view;?>">
                  <td class="right">
                      <input type="checkbox" id="checkbox-item<?php echo $row->id ?>" name="checkboxitem[]" value="<?php echo $row->id ?>"/>
                      <label for="checkbox-item<?php echo $row->id ?>"></label>
                  </td>
                  <td class="detail"  id="<?php echo base64_encode($row->id);?>"><?php echo $row->client_name; ?></td>
                  <td class="detail centered"  id="<?php echo base64_encode($row->id);?>"><?php echo $row->dob; ?></td>
                  <td class="detail centered"  id="<?php echo base64_encode($row->id);?>"><?php echo $row->nationality; ?></td>
                  <td class="detail centered"  id="<?php echo base64_encode($row->id);?>"><?php echo $idtype[$row->type_of_id] ?></td>
                  <td class="detail centered" style="font-size:12px;" id="<?php echo base64_encode($row->id);?>"><?php echo $row->id_issured_date.' ដល់ '.$row->id_expiry_date?></td>
                  <td class="centered"><?php echo $row->id_number; ?></td>
                  <td class="detail centered"  id="<?php echo base64_encode($row->id);?>"><?php echo $row->branch_name; ?></td>
                  <td class="centered"><a href="mailto:<?php echo $row->email_address;?>"><?php echo $row->email_address; ?></a></td>
                  <td class="centered" style="font-size:12px;"><?php echo $row->created_date; ?></td>
                </tr>
            <?php
            }
          }else{
            echo '<tr><td class="centered" colspan="8" style="color:#ee6e73;text-align:center;">ពុំ​មាន​ទិិន្នន័យ​ដែល​លោក​អ្នក​ស្វែង​រក​​នោះ​ទេ។</td></tr>';
          }
          ?>
          </tbody>
          <?php 
          $pagequery = "SELECT count(*) FROM loan_customer AS cu INNER JOIN loan_purpose AS pu ON cu.id=pu.customer_id";
          $pageresult = $conn->query($pagequery);
          $row = mysqli_fetch_row($pageresult);  
          $total_records = $row[0];  
          $total_pages = ceil($total_records / $limit); 
          //if no page var is given, default to 1.
          $prev = $page - 1;							//previous page is page - 1
          $next = $page + 1;							//next page is page + 1
          $lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
          $lpm1 = $lastpage - 1;						//last page minus 1
          $activepre = $prev==0?'disabled':'waves-effect';
          $prevlink = $prev?"?page=$prev":'#';

          $activenext = $next>$total_pages?'disabled':'waves-effect';
          $nextlink = $next<=$total_pages?"?page=$next":'#';

            if( $total_records>$limit){?>
            <tfoot>
                <tr>
                  <td colspan="9" class="centered">
                    <ul class="pagination">
                      <li class="<?php echo $activepre;?>"><a href="<?php echo $prevlink;?>"><i class="material-icons">chevron_left</i></a></li>
                      <?php
                        for ($i=1; $i<=$total_pages; $i++) { 
                        $active = (isset($_GET['page']) && $_GET['page']==$i) || (!isset($_GET['page']) && $i==1)?'active':'';
                        ?>
                        <li  class="<?php echo $active;?> waves-effect"><a href="?page=<?php echo $i;?>"><?php echo $i;?></a></li>
                        <?php 
                        }
                      ?>
                      <li class="<?php echo $activenext;?>"><a href="<?php echo $nextlink;?>"><i class="material-icons">chevron_right</i></a></li>
                    </ul>
                  </td>
                </tr>
            </tfoot>
            <?php 
            }
            $conn->close();
          ?>
      </table>
    </form>
  </div>