    <div class="row" style="padding:0;margin-bottom:0;">
        <div class="footer-wrapper">
            <footer class="page-footer">
                <div class="center" style="padding:10px;">
                    © <?php echo date("Y");?> រក្សាសិទ្ធិគ្រប់​យ៉ាងដោយ គ្រឹះស្ថានមីក្រូហិរញ្ញវត្ថុ ប្រាសាក់ ម.ក
                    <a class="grey-text text-lighten-4" href="https://www.prasac.com.kh" target="_blank"> &nbsp; www.prasac.com.kh</a>
                </div>
            </footer>
        </div>
    </div>
<style>  
    #loader {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        background: rgba(0, 0, 0, 0.75) url(http://feedback.local//images/Loading.gif) no-repeat center center;
        z-index: 10000;
    }
    /* PULSATING CARET */
    [data-typer]:after {
    content:"";
    display: inline-block;
    vertical-align: middle;
    width:1px;
    height:1em;
    background: #000;
            animation: caretPulsate 1s linear infinite; 
    -webkit-animation: caretPulsate 1s linear infinite; 
    }
    @keyframes caretPulsate {
    0%   {opacity:1;}
    50%  {opacity:1;}
    60%  {opacity:0;}
    100% {opacity:0;}
    }
    @-webkit-keyframes caretPulsate {
    0%   {opacity:1;}
    50%  {opacity:1;}
    60%  {opacity:0;}
    100% {opacity:0;}
    }
</style>
<div id="loader"></div>
        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="http://feedback.local/library/jquery.min.js"></script>
        <!--Import Materialize-Stepper JavaScript -->
        <script src="http://feedback.local/library/materialize-v1.0.0/js/materialize.min.js"></script>
        <script src="http://feedback.local/library/materialize-datetimepicker-master/js/materializedatetimepicker.js"></script>
        <script type="text/javascript" src="http://feedback.local/library/jquery-chained.min.js"></script>
        <script type="text/javascript" src="http://feedback.local/library/jquery-validation/dist/jquery.validate.min.js"></script>
        <script>
            $("#sub_service").chained("#main_service");


            var currYear = (new Date()).getFullYear();

            var DateTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Bangkok"});
            var date = new Date();
            var year = date.getFullYear();
            var month = date.getMonth();
            var day = date.getDate();
            var MyminDate = new Date(year, month-1, day);


            $(document).ready(function() {
                M.AutoInit();
                $('textarea#issue_detail, textarea#solution').characterCounter();
                // M.textareaAutoResize($('#issue_detail'));
                // MaterialDateTimePicker.create($('input.issue_date'));
                // MaterialTimePicker.create($('input#issue_time'));
                $('input#issue_time').timepicker({
                    default: 'now',
                    autoClose:true,
                    twelvehour: false,
                    vibrate: true,
                });
                $(".issue_date").datepicker({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 1, // Creates a dropdown of 15 years to control year
                    format: "yyyy-mm-dd",
                    defaultDate:new Date(),
                    setDefaultDate: true,
                    autoClose : true,
                    minDate:MyminDate,
                    maxDate:new Date(),
                    yearRange: [currYear-1, currYear],
                });
                
                $("#issue_date_edit").datepicker({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 1, // Creates a dropdown of 15 years to control year
                    format: "yyyy-mm-dd",
                    autoClose : true,
                    minDate:MyminDate,
                    maxDate:new Date(),
                    yearRange: [currYear-1, currYear],
                });

                $("#search_date_from,#search_date_to").datepicker({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears:1,
                    format: "yyyy-mm-dd",
                    setDefaultDate: true,
                    minDate: new Date(2019,11,32),
                    autoClose : true,
                    showClearBtn: true,
                    yearRange: [currYear-1, currYear],
                });
                
                var spinner = $('#loader');
                $('.tooltipped').tooltip();
                $('.sidenav').sidenav();
                $('select').formSelect();
                $('input.select-dropdown').click(function() {
                    $(this).addClass(' active');
                });

                $("td.detail input").attr('readonly', true);
                $("td.detail").click(function(){
                    var $row_id =   $(this).attr('id');
                    $("td.detail#"+$row_id+" input").attr("readonly",false);
                });


                $("#regional").on('change',function(){
                    var $val = $(this).find('option:selected').val();
                    $("#branch option").hide();
                    $("#branch option."+$val).show();
                    $('#branch').formSelect();
                });

                //
                $("table.authentication-form tbody tr").each(function(){
                    var $re_data    =   $(this).find('td:eq(3)>span').text();
                    var $tran_data    =   $(this).find('td:eq(4)>span').text();
                    var $ending_data    =   $(this).find('td:eq(5)>span').text();
                    var $comment_data    =   $(this).find('td:eq(6)>span').text();
                    if(($re_data+$tran_data+$ending_data)>0 || $comment_data != ''){
                        $(this).css('opacity',1);
                    }
                });

                //Initial Spinner
                var spinner = jQuery('#loader');
                $(".btn-export-excel").click(function(e){
                    e.preventDefault();
                    spinner.show();
                    //if($("table.report-list tbody tr td input:checkbox:checked").length > 0){
                    var data = $('form#update-form').serializeArray();
                    $.ajax({
                        type:"POST",
                        url:"../admin/excel.php",
                        cache:false,
                        data:data,
                        success: function(data)
                        {
                            spinner.hide();
                            showToast('<i class="material-icons left">check_circle</i>  របាយការណ៍របស់លោកអ្នកដោនឡូតបានយ៉ាងជោគជ័យ',3000);
                            var a = document.createElement('a');
                            a.href =data;
                            a.download = 'Consolidated_Report_Customer_Feedback_System.xlsx';
                            a.click();
                            a.remove();
                            console.log(data);
                        }
                    });
                    // }else{
                    //     spinner.hide();
                    //     showToast('<span style="color:#ffab00;"><i class="material-icons left">info_outline</i> លោកអ្នកមិនទាន់ជ្រើសរើសទិន្នន័យសម្រាប់​ដោនឡូតនៅឡើយ</span>', 3000);
                    // }
                });
                $('select#materials').formSelect();
                $('select.select_all').siblings('ul').prepend('<li id=sm_select_all><span>ជ្រើសរើសទាំងអស់</span></li>');
                $('li#sm_select_all').on('click', function () {
                    var jq_elem = $(this), 
                    jq_elem_span = jq_elem.find('span'),
                    select_all = jq_elem_span.text() == 'ជ្រើសរើសទាំងអស់',
                    set_text = select_all ? 'លុបជម្រើសទាំងអស់' : 'ជ្រើសរើសទាំងអស់';
                    jq_elem_span.text(set_text);
                    jq_elem.siblings('li').filter(function() {
                        return $(this).find('input').prop('checked') != select_all;
                    }).click();
                });

                $("table.list-authen tbody tr td.enable_click").click(function(e){
                    $(this).parent().css('opacity',1);
                    var $row_class = $(this).attr('id');
                    var $spanclass  =  $(this).find('span').attr('class');
                    $("table.list-authen tbody tr td input").hide();
                    $("table.list-authen tbody tr td span").show();
                    $(this).parent().find("span."+$spanclass ).hide();
                    $(this).parent().find("input."+$spanclass ).show();
                });

                $.validator.addMethod("greaterThan", 
                function(value, element, params) {

                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) >= new Date($(params).val());
                    }

                    return isNaN(value) && isNaN($(params).val()) 
                        || (Number(value) >= Number($(params).val())); 
                },'Must be greater than {0}.');


                $.validator.setDefaults({
                    ignore: []
                });

                $("input#search_branch_fc").keypress(function(e){
                    var id_check_length =   jQuery(this).val().length;
                    var fc_site = jQuery(this).val();
                    if(id_check_length>0){
                        jQuery.ajax({
                            url: "filter-branch.php",
                            type: "POST",
                            data:{'fcsite':fc_site},
                            success: function(response) {
                                var datasource = JSON.parse(response);
                                var dataCountry = {};
                                for (var i = 0; i < datasource.length; i++) {
                                    dataCountry[datasource[i].name_fc] = null; //countryArray[i].flag or null
                                }
                                $('input#search_branch_fc').autocomplete({
                                    data:dataCountry,
                                    limit:5,
                                });
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log(errorThrown);
                            }
                        });
                    }
                });
            });



            // $("table tbody tr.row-item td.view").click(function(e){
            //     window.location.href= $(this).parent().data('detailurl');
            // });

            $("table.authentication-form tbody tr td input.keypress").keypress(function(e) {
                var $entered_value  =   $(this).val();
                var $data_type  =   $(this).data('name');
                if (e.keyCode === 13 || e.keyCode === 9) {
                    if($data_type=='comment' || isNormalInteger($entered_value)){
                        var values = {
                            'data':$(this).data('name'),
                            'report_id': $(this).data('id'),
                            'branch_id':$(this).data('branch'),
                            'in_month': $(this).data('month'),
                            'in_year': $(this).data('year'),
                            'report_value': $entered_value
                        };
                        jQuery.ajax({
                            url: "../admin/saveReport.php",
                            type: "POST",
                            data:values,
                            success: function(data, textStatus, jqXHR) {
                                console.log(data);
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log(errorThrown);
                            }
                        });
                        $(this).hide();
                        var $row_class_input =    $(this).data('id');
                        $(this).parent().find('span').text($entered_value).show();
                        
                        
                        if($entered_value>0){
                            jQuery(this).parent().parent().css('opacity',1);
                        }

                    }else{
                        showToast('<span style="color:#ffab00;"><i class="material-icons left">info_outline</i> អនុញ្ញាតិអោយបញ្ចូលតែលេខឡាតាំង ហើយជាចំនួនគត់វិជ្ជមាន​ប៉ុន្នោះ</span>', 3000);
                        $(this).val(0);
                        spinner.hide();
                        return false;
                    }
                }
            });

            function isNormalInteger(str) {
                return /^\+?(0|[1-9]\d*)$/.test(str);
            }

            $('.dropdown-button').dropdown({
                inDuration: 300,
                outDuration: 225,
                constrainWidth: false, // Does not change width of dropdown to that of the activator
                hover: true, // Activate on hover
                gutter: 0, // Spacing from edge
                belowOrigin: false, // Displays dropdown below the button
                alignment: 'left', // Displays dropdown with edge aligned to the left of button
                stopPropagation: false // Stops event propagation
            });

            // $('#check-all').on('change', function() {
            //     $('table.report-list tbody tr td:nth-child(1) input').prop('checked', this.checked);
            //     var $checked_item = $("tbody tr td input[name='checkboxitem[]']:checked").length;

            //     if($checked_item){
            //         $("form.update-form .status-row button, form.update-form .status-row select>option, form.update-form .status-row i, form.update-form .status-row .select-wrapper, form.update-form .status-row input").removeAttr('disabled');
            //         $("form.update-form .status-row .select-wrapper select#select_status").prop('disabled',false);
            //         $("tbody tr td input[name='checkboxitem[]']:checked").parent().parent().parent().css('background','rgba(97,187,70,.2)');
            //     }else{
            //         $("form.update-form .status-row button, form.update-form .status-row select>option, form.update-form .status-row i, form.update-form .status-row .select-wrapper, form.update-form .status-row input").attr('disabled',true);
            //         $("tbody tr td input[name='checkboxitem[]']").parent().parent().parent().css('background','transparent');
            //     }
            // });

            // $('table.report-list tbody tr td input').on('change', function() {
            //     $('#check-all').prop('checked', this.checked);
            //     var $checked_item = $("tbody tr td input[name='checkboxitem[]']:checked").length;
            //     if($checked_item){
            //         $("form.update-form .status-row button, form.update-form .status-row select>option, form.update-form .status-row i, form.update-form .status-row .select-wrapper, form.update-form .status-row input").removeAttr('disabled');
            //         $("form.update-form .status-row .select-wrapper select#select_status").prop('disabled',false);
            //         $("tbody tr td input[name='checkboxitem[]']:checked").parent().parent().parent().css('background','rgba(97,187,70,.2)');
            //         $(this).parent().parent().parent().css('background','rgba(97,187,70,.2)');
            //     }else{
            //         $("form.update-form .status-row button, form.update-form .status-row select>option, form.update-form .status-row i, form.update-form .status-row .select-wrapper, form.update-form .status-row input").attr('disabled',true);
            //         $(this).parent().parent().parent().css('background','transparent');
            //     }
            // });

            /*
            **Showing Toast as HTML
            */
            function showToast(message, duration) {
                M.toast({html:message,duration:duration,class:'rounded'});
            }
        </script>                                                                                                                               
    </body>
</html>