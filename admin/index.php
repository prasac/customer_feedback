<?php
include('http://feedback.local/session.php');
if(!isset($_SESSION['login_user'])){
  header("location:http://feedback.local/");
}else if(isset($_SESSION['login_user']) && ($_SESSION['login_id'] ==2)){ // if user already logged in with admin user
  header("location:http://feedback.local/auth/index.php");//redirect to admin list page
}else{
    include('http://feedback.local/login.php'); // Includes Login Script
    include('http://feedback.local/header.php');
    ?>
    <style>
    body{
      background: rgba(97, 187, 70, 0.2);
      padding:0;
      margin:0;
    }
    </style>
      <main>
        <center>
          <div class="container-fluid">
            <img class="responsive-img" src="http://feedback.local/images/logo-website.png" width="120"/>
            <div class="section"></div>
            <h5 class="indigo-text">ប្រព័ន្ធគ្រប់គ្រងមតិអតិថិជនq</h5>
            <div class="section"></div>
            <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

              <form action="" method="POST">
                <div class='row'>
                  <div class='input-field col s12'>
                    <input type='text' name='username' id='username' class='validate' required/>
                    <label for='username'>ឈ្មោះ​</label>
                  </div>
                </div>

                <div class='row'>
                  <div class='input-field col s12'>
                    <input class='validate' type='password' name='password' id='password' required/>
                    <label for='password'>លេខ​សម្ងាត់​</label>
                  </div>
                </div>

                <br />
                <center>
                  <div class='row'>
                    <button type='submit' name='submit' class='col s12 btn btn-large waves-effect indigo'>ចូល​</button>
                  </div>
                </center>
              </form>
              <div class='row'>
                <div class='input-field col s12 pink-text'>
                  <?php echo  $error;?>
                </div>
              </div>
            </div>
          </div>
        </center>
      </main>
  <?php 
  }
?>
