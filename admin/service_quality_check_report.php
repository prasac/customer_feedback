<?php 
  include('session.php');
  $date = new DateTime();
  $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
  $current_datetime = $date->format("d-M-Y h:i:s A");

  $this_month = isset($_POST['month'])?$_POST['month']:$date->format("M");
  $months = array(1 => 'មករា', 2 => 'កុម្ភៈ', 3 => 'មីនា', 4 => 'មេសា', 5 => 'ឧសភា', 6 => 'មិថុនា', 7 => 'កក្កដា', 8 => 'សីហា', 9 => 'កញ្ញា', 10 => 'តុលា', 11 => 'វិច្ឆិកា', 12 => 'ធ្នូ');
  $files = glob('../reports/Service_Checking_Report/*'); // get all file names
  foreach($files as $file){ // iterate files
    if(is_file($file)) {
      unlink($file); // delete file
    }
  }
  include('../library/PHPExcel/Classes/PHPExcel.php');
    $objPHPExcel = new PHPExcel();
    //Testing Create multiple sheet_ one sheet per item
    $stylehead1 = array(
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font'  => array(
          'color' => array('rgb' => '117515'),
          'size'  =>12,
          'name'  => 'Khmer OS Muol Light',
      ),
      'borders' => array(
        'right' => array(
          'style' => PHPExcel_Style_Border::BORDER_HAIR,
          'color' => array('rgb' => '333333')
        ),
      ),
    );
    $stylehead2 = array(
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font'  => array(
          'color' => array('rgb' => 'b78311'),
          'size'  =>12,
          'name'  => 'Khmer OS Muol Light'
      ),
      'borders' => array(
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('rgb' => '333333')
        ),
        'right' => array(
          'style' => PHPExcel_Style_Border::BORDER_HAIR,
          'color' => array('rgb' => '333333')
        ),
      ),
    );

    $stylerowhead = array(
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font'  => array(
          // 'bold'  => true,
          'color' => array('rgb' => '333333'),
          'size'  =>12,
          'name'  => 'Khmer OS Muol Light'
      ),
      'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('rgb' => '333333')
        )
      ),
      'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'FFFFFF')
      )
    );

    $stylerowhead_en = array(
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font'  => array(
          'bold'  => true,
          'color' => array('rgb' => '333333'),
          'size'  =>12,
          'name'  => 'Times New Roman'
      ),
      'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('rgb' => '333333')
        )
      ),
      'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'FFFFFF')
      )
    );

    $stylecontenthead = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font'  => array(
          'bold'  => true,
          'size'  =>10,
          'name'  => 'Khmer OS Content'
      ),
      'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('rgb' => '333333')
        )
      )
    );

    $stylecontent = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font'  => array(
          'bold'  => false,
          'size'  =>10,
          'name'  => 'Khmer OS Content'
      ),
      'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('rgb' => '333333')
        )
      )
    );

    $date_search_file_name = isset($_POST['search_date'])?date("Ymd",strtotime($_POST['search_date'])):$date->format("Ymd");
    $date_report = isset($_POST['search_date'])?date("d-M-Y",strtotime($_POST['search_date'])):$date->format("d-M-Y");

    $objPHPExcel->disconnectWorksheets();
    $objWorkSheet = $objPHPExcel->createSheet(0);
    $objWorkSheet->setTitle("Service_Quality_Checking_Report");

    $rowCount = 4;
    $customTitle = array ('កូដសាខា','សាខា','ភូមិភាគ','អត្តលេខបុគ្គលិក','ឈ្មោះបុគ្គលិក','តួនាទី','អត្តលេខអ្នកទទួលបន្ទុក');
    $colName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
   
    //Adding indicator
    $query = "SELECT * FROM tbl_service_quality_checking_staff_key_indicator AS indicator ORDER BY indicator.order ASC";
    $result_indicator = $conn->query($query);
    if(($result_indicator) && $result_indicator->num_rows>0){
      while($row = mysqli_fetch_object($result_indicator)) {
        array_push($customTitle,$row->title_kh);
      }
    }
      
    $i=0;
    $celVal = '';
    foreach ($customTitle as $col_list){
      $celCol= $colName[$i].$rowCount;
      $objWorkSheet->SetCellValue($celCol,$col_list);
      $i++;
    }

    $i--;
    $objWorkSheet->getStyle("A".$rowCount.":".$colName[$i].$rowCount)->applyFromArray($stylecontenthead);
    $objWorkSheet->mergeCells("$colName[0]1:$colName[$i]1");
    $objWorkSheet->mergeCells("$colName[0]2:$colName[$i]2");
    $objWorkSheet->mergeCells("$colName[0]3:$colName[$i]3");

    $objWorkSheet->setCellValue('A1','គ្រឹះស្ថានមីក្រូហិរញ្ញវត្ថុ ប្រាសាក់ ម.ក | PRASAC MFI PLC.');
    $objWorkSheet->getStyle("A1:$colName[$i]1")->applyFromArray($stylehead1);
  
    $objWorkSheet->setCellValue('A2','របាយការណ៍ត្រួតពិនិត្យគុណភាពសេវាកម្ម | Service Quality Checking System Report');
    $objWorkSheet->getStyle("A2:$colName[$i]2")->applyFromArray($stylehead2);

    $objWorkSheet->getRowDimension('1')->setRowHeight(30);
    $objWorkSheet->getRowDimension('2')->setRowHeight(30);
    $objWorkSheet->getRowDimension('3')->setRowHeight(20);


    $objWorkSheet->setCellValue('A3','កាលបរិច្ឆេទរបាយការណ៍​៖ '. $date_report);
    $objWorkSheet->getStyle("A3:$colName[$i]3")->applyFromArray($stylecontent);



    $objWorkSheet->getColumnDimension('A')->setWidth(10);
    $objWorkSheet->getColumnDimension('B')->setWidth(25);
    $objWorkSheet->getColumnDimension('C')->setWidth(10);
    $objWorkSheet->getColumnDimension('D')->setWidth(15);
    $objWorkSheet->getColumnDimension('E')->setWidth(20);
    $objWorkSheet->getColumnDimension('F')->setWidth(25);
    $objWorkSheet->getColumnDimension('G')->setWidth(20);
    $objWorkSheet->getColumnDimension('H')->setWidth(30);
    $objWorkSheet->getColumnDimension('I')->setWidth(15);
    $objWorkSheet->getColumnDimension('J')->setWidth(15);
    $objWorkSheet->getColumnDimension('K')->setWidth(18);
    $objWorkSheet->getColumnDimension('L')->setWidth(15);
    $objWorkSheet->getColumnDimension('M')->setWidth(15);
    $objWorkSheet->getColumnDimension('N')->setWidth(15);

    //Adding report into the cells
    $customer  = array();
    //Set cell function
    function cellColor($cells,$color,$fillcolor){
      global $objPHPExcel;
      $objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray(array(
        'font'  => array(
             'bold'  => false,
             'color' => array('rgb' => $color),
        ),
        'fill' => array(
          'type' => PHPExcel_Style_Fill::FILL_SOLID,
          'color' => array('argb' =>$fillcolor)
        )
      ));
    }

    $search_date = isset($_POST['search_date'])?$_POST['search_date']:0;
    $position_search  = isset($_POST['position_search'])?implode(",", $_POST['position_search']):0;
    $service_checking_regional = isset($_POST['service_checking_regional'])?$_POST['service_checking_regional']:0;
    $service_checking_branch = isset($_POST['service_checking_branch'])?implode(",",$_POST['service_checking_branch']):0;
    $branch_access = '';
    $date_access_search = ' AND DATE(checking.created_date)=CURDATE() ';
    if($search_date){
      $date_access_search = " AND DATE(checking.created_date) = '$search_date%'";
    }

    if($position_search){
      $position_search = " AND staff.staff_position IN($position_search)";
    }


    if($search_date && (!$service_checking_regional && !$service_checking_branch)){
      $branch_access = "AND DATE(checking.created_date) = '$search_date%'";
    }

    if($service_checking_regional && !$service_checking_branch){
      $branch_access = "AND staff.regional_id = $service_checking_regional";
    }else if($service_checking_regional && $service_checking_branch){
      $branch_access = "AND staff.regional_id = $service_checking_regional AND staff.branch_id IN($service_checking_branch)";
    }else{
      $branch_access = "";
    }

    $indicator_fields = array();
    $indicator_query = $conn->query("SELECT * FROM tbl_service_quality_checking_staff_key_indicator AS indicator ORDER BY indicator.order ASC");
    while($row_indicator = mysqli_fetch_object($indicator_query)){
      array_push($indicator_fields,"key_indicator$row_indicator->id");
    }
    $indicator_fields_imploded = implode(",", $indicator_fields);
    
    $query = "SELECT
              staff.id AS id,
              staff.staff_id AS staff_id,
              staff.branch_id AS branch_id_main,
              staff.staff_fullname_kh AS staff_fullname_kh,
              $indicator_fields_imploded
              ,pos.staff_position_kh AS staff_position,
              branch.fc AS branch_fc,
              branch.name_kh AS branch_name,
              regional.region AS regional,
              checking.reported_by AS reported
            FROM tbl_service_quality_checking AS checking
            RIGHT JOIN tbl_service_quality_checking_staff AS staff  ON staff.id = checking.staff_id
            LEFT JOIN tbl_service_quality_checking_staff_position AS pos ON pos.id = staff.staff_position
            INNER JOIN branches AS branch ON branch.id=staff.branch_id
            INNER JOIN tbl_regional AS regional ON regional.id=staff.regional_id
            WHERE staff.status = 1 $branch_access
            ORDER BY staff.regional_id,staff.branch_id,staff.staff_position ASC";
    $query_export_result = $conn->query($query);
    if($query_export_result->num_rows){
      $scoring = array(0=>'N/A',1=>'Full',2=>'Partial',3=>'No');
      $content_num = 5;
      $col = 0;
      while($row = mysqli_fetch_object($query_export_result)) {
        $fcsite = $row->branch_fc;
        $branch_name = $row->branch_name;
        $regional = $row->regional;
        $staff_id = $row->staff_id;
        $staff_fullname_kh = $row->staff_fullname_kh;
        $staff_position = $row->staff_position;
        $reported = $row->reported?$row->reported:'';
        $objWorkSheet->getStyle("A".$content_num.":N".$content_num)->applyFromArray($stylecontent);
        $objWorkSheet->getStyle('A'.$content_num.':'.'F'.$content_num)->getAlignment()->setWrapText(true);
        $objWorkSheet->getStyle('I'.$content_num.':'.'M'.$content_num)->getAlignment()->setWrapText(true);
        $objWorkSheet->setCellValueExplicit('A'.$content_num,$fcsite, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue('B'.$content_num,$branch_name);
        $objWorkSheet->setCellValueExplicit('C'.$content_num,$regional, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->setCellValueExplicit('D'.$content_num,$staff_id, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue('E'.$content_num,$staff_fullname_kh);
        $objWorkSheet->SetCellValue('F'.$content_num,$staff_position);
        $objWorkSheet->setCellValueExplicit('G'.$content_num,$reported, PHPExcel_Cell_DataType::TYPE_STRING);
        $celcolor = '000000';
        $fillcolor = 'FFFFFF';
        $col = 7;
        $cell_value = 'N/A';
        foreach ($indicator_fields as $indicator_fields_list){
          switch($row->$indicator_fields_list){
            case '1'://Full
              $celcolor = '4db848';
              $fillcolor = 'CCFFCC';
              break;
            case '2'://Partial
              $celcolor = 'e98008';
              $fillcolor = 'FFFFCC'; 
              break;
            case '3'://No
              $celcolor = 'ec0808';
              $fillcolor = 'FFCCCC';
              break;
          }
          
          if($col==7){
            $cell_value = $row->$indicator_fields_list;
            if($cell_value  == '0' || $cell_value  == ''){
              $cell_value = '';
            }
          }else{
            $col_indicator = $row->$indicator_fields_list !=''?$row->$indicator_fields_list:0;
            $cell_value = $scoring[$col_indicator];
          }

          $objWorkSheet->SetCellValue($colName[$col].$content_num,$cell_value);
          cellColor($colName[$col].$content_num,$celcolor,$fillcolor);
          $col++;
        }
        // exit;
        $objWorkSheet->getStyle("I".$content_num)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $objWorkSheet->getStyle("I".$content_num)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));

        $objWorkSheet->getStyle("J".$content_num)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $objWorkSheet->getStyle("J".$content_num)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));

        $objWorkSheet->getStyle("K".$content_num)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $objWorkSheet->getStyle("K".$content_num)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));

        $objWorkSheet->getStyle("L".$content_num)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $objWorkSheet->getStyle("L".$content_num)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));

        $objWorkSheet->getStyle("M".$content_num)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $objWorkSheet->getStyle("M".$content_num)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));

        $objWorkSheet->getStyle("N".$content_num)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $objWorkSheet->getStyle("N".$content_num)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $content_num++;
      }
    }
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setShowGridlines(false);
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $fullfilename = "$date_search_file_name"."_"."Service_Quality_Checking_Report";
    $filename = $fullfilename.'.xlsx';
    $filename_path = "../reports/Service_Checking_Report/$filename";
    $objWriter->save($filename_path);
    echo $filename_path.','.$filename;
    exit;
?>