<!DOCTYPE html>
<html>
  <head>
    <title><?php echo isset($title)?$title:'ប្រព័ន្ធគ្រប់គ្រងព័ត៌មានត្រលប់ពីអតិថិជន';?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--Import materialize.css-->
    <link href="http://feedback.local/css/fonts.min.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="http://feedback.local/library/materialize-v1.0.0/css/materialize.min.css"/>
    <link type="text/css" rel="stylesheet" href="http://feedback.local/library/font-awesome/css/font-awesome.min.css"/>
    <link type="text/css" rel="stylesheet" href="http://feedback.local/css/style.min.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="http://feedback.local/css/style-front.min.css" media="screen" />
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="http://feedback.local/images/favoicon.ico">
  </head>
  <body>